import React from 'react'
import { RetailPackage } from '../../Components'
import { LayoutNoAuth } from '../../Layout'
import { retailPackage1 } from '../../utils'

export default function index() {
  return (
    <LayoutNoAuth Title={"Retail Package"}>
        <RetailPackage retailPackage1={retailPackage1}/>
    </LayoutNoAuth>
  )
}
