export const getAuthToken = () => {
    return localStorage.getItem("user:token");
};

export const setAuthToken = (token) =>
    localStorage.setItem("user:token", token);

export const setLocationHistory = (location) =>
    sessionStorage.setItem("user:redirect:location", JSON.stringify(location));

export const getLocationHistory = () => {
    return JSON.parse(sessionStorage.getItem("user:redirect:location"));
};


export const GeneralUrl ='https://api.pocketlawyers.io/'