/* eslint-disable react/no-unescaped-entities */
import { useState } from "react";
import { LayoutNoAuth } from "../../Layout";
import VerficationIcon from "../../assest/Image/mailverify.svg";
import Image from "next/image";
import { InputComp2 } from "../../Components";
import Button from "../../Components/Ui/Button";
import { useAuth } from "../../hooks/useContext";

export default function Index() {
  const initialState = {
    email: "",
    code: "",
  };
  const { VerifyAccountFunc,RequestPassordReset,state:{isLoading} } = useAuth();
  const [ userData, setUserData ] = useState(initialState);

  const handleOnchange = (e) => {
    const { name, value } = e.target;
    //console.log(value)
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = () => {
   // console.log(userData)
   VerifyAccountFunc(userData?.email,userData?.code)
  }
  const handleReset = () => {
   // console.log(userData)
   RequestPassordReset({email:userData?.email})
  }

  return (
    <LayoutNoAuth footer>
      <section className="pb-8">
        <div className="container d-flex justify-content-center mb-5">
          <div className="col-10 col-lg-8 col-xl-4">
            <div className="text-center mb-3">
              <Image src={VerficationIcon} alt="" />
            </div>
            <div className="text-center mb-3">
              <h5 className="mb-3">VERIFY YOUR EMAIL ADDRESS</h5>
              <h6 className="text-type2 mb-5">
                We've sent a code your inbox, Check and enter the code below
              </h6>
            </div>
            <div className="">
              <div className="mb-3">
                <InputComp2
                  label={"Email"}
                  labelSize="17px"
                  bordercolor="#C4C4C4"
                  className={"py-3"}
                  name="email"
                  onChange={handleOnchange}
                  value={userData?.email}
                />
              </div>
              <div className="mb-5">
                <InputComp2
                  label={"Verification Code "}
                  labelSize="17px"
                  bordercolor="#C4C4C4"
                  className={"py-3"}
                  name="code"
                  onChange={handleOnchange}
                  value={userData?.code}
                />
              </div>
              <div className="mt-5 mb-3">
                <Button
                  bntText={isLoading?"Loading...":"Verify"}
                  className="bg-type1 text-white h4 fw-bold py-3  w-100 px-3 rounded"
                  onClick={handleSubmit}
                  // loading={isLoading}
                />
              </div>
              <div className=" text-center">
                <Button
                  bntText={"Resend Code"}
                  className="bg-7  fw-bold    rounded"
                  onClick={handleReset}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </LayoutNoAuth>
  );
}
