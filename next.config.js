/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['api.pocketlawyers.io'],
  },
}

module.exports = nextConfig
