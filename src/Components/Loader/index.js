/* eslint-disable @next/next/no-img-element */
import styled from "styled-components";
import { Spinner } from "react-bootstrap";
import Image from "next/image";

const Styled = styled.div`
  height: ${(props) => (props.Height ? props.Height : "100vh")};
  display: flex;
  justify-content: center;
  align-items: center;
  .spinner-border{
    color: ${props=>props.Color?props.Color:'black'}
  }
`;
export const Loader =({ height,color })=> {
  return (
    <Styled Height={height} Color={color}>
      <Spinner animation="border" role="status">
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    </Styled>
  );
}

export const Loader2 =({ height,color })=> {
  return (
    <Styled Height={height} Color={color}>
      <div className=" d-flex justify-content-center  w-100 text-center animate__animated animate__infinite	infinite animate__bounce">
       <div style={{width:'300px'}}>
       <img src={'/Logo.svg'} className="w-100"  alt={""} />
       </div>
      </div>
    </Styled>
  );
}
