import styled from "styled-components";
import Logo from "../../assest/Image/loginImage.svg";
import Image from "next/image";
import { InputComp } from "../Ui";
import Button from "../Ui/Button";
import Link from "next/link";
import { useState } from "react";

const Style = styled.section`
  padding: 3rem 0 15rem 0;
  .form-width {
    width: 35%;
  }
  input,
input::placeholder {
    font-size: 15px ;
}
  @media (max-width: 992px) {
    .form-width {
      width: 60% !important;
    }
  }
  @media (max-width: 786px) {
    .form-width {
      width: 80% !important;
    }
  }
`;
export const LoginForm = ({ Login, loading }) => {
  const initialState = {
    email: "",
    password: "",
  };
  const [userData, setUserData] = useState(initialState);

  const handleOnChnage = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };
  const { email, password } = userData;

  const handleSubmit = (e) => {
    e.preventDefault();
    const value = { email, password };
    Login(value);
  };

  return (
    <Style>
      <div className="container  text-black">
        <div className="py-5 py-lg-3"></div>
        {/* <h6 className="fw-bold mb-4 text-center">WELCOME TO</h6>
        <div className="text-center mb-5 px-5 px-md-0">
          <Image src={Logo} alt="" />
        </div> */}
        <div className="d-flex justify-content-center">
       <div className="col-12">
       <div className="row justify-content-center g-0 overflow-hidden" style={{borderRadius:'20px'}}>
        <div className="col-md-8 col-xl-6 bg-6">
       <div className="d-flex justify-content-center">
       <div className="p-4 col-lg-8">
       <div className="mb-5 mt-5">
            <h3 className=" fw-1 font-4 text-type1 mb-0">Sign In</h3>
            <h5 className="h3 font-2 fw-0  text-capitalize">to access your dashboard</h5>
            </div>
       <form className="w-100">
            <div>
              <InputComp
                label={<span className="h11 font-5">Email Address</span>}
                className="border-3 py-2"
                placeholder="input email address"
                name="email"
                type={"email"}
                value={email}
                onChange={handleOnChnage}
              />
            </div>
            <div className="mb-5">
              <InputComp
                label={<span className="h11 font-5">Password</span>}
                icon
                smallText={"Forget Password"}
                placeholder="Enter Password"
                typeComp={"password"}
                name="password"
                value={password}
                onChange={handleOnChnage}
              />
            </div>
            <div className="text-center">
              <Button
                bntText={"LOG IN"}
                className="w-100 py-2 h51 fw-bolder bg-type1 text-white mb-3"
                onClick={handleSubmit}
                loading={loading}
                disabled={loading}
              />
              <div className="fw-0 h11">
                Dont have an account yet,{" "}
                <Link href="/signup">
                  <a className="text-type1 fw-1">Sign Up</a>
                </Link>
              </div>
            </div>
          </form>
       </div>
       </div>
        </div>
        <div className=" d-none d-xl-block col-xl-6">
          <div className="" style={{height:"650px"}}>
          <picture>
        <source srcSet='/login.svg' type="image/svg" />
            <img src={'/login.svg'} alt='' className="w-100 h-100"  layout='responsive' />
            </picture>
          </div>
        </div>
        <div></div>
        </div>
       </div>
        </div>
      </div>
    </Style>
  );
};
