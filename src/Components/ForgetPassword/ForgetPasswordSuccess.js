
import VerficationIcon from '../../assest/Image/forgetpasswordsuccess.svg'
import Image from 'next/image'
import { InputComp2 } from '../../Components'
import Button from '../../Components/Ui/Button'
import { useRouter } from 'next/router'
export const ForgetpasswordSuccess =()=> {
  const router =useRouter()
  return (
    <div>
        <div className='container d-flex justify-content-center mb-5'>
    <div className='col-10 col-lg-8 col-xl-4'>
<div className='text-center mb-3'>
    <Image src={VerficationIcon} alt=''/>
</div>
<div className='text-center mb-3'>
    <h5 className='mb-3'>FORGET PASSWORD</h5>
    <div className='text-type2 mb-5 h10'>We have sent a password recovery instruction
to you email.
password.</div>
</div>
<div className=''>
  <div className='mt-5 mb-3'><Button bntText={'Open Email'} onClick={()=>router.push('/create-new-password')} className='bg-type1 text-white h5 fw-bold py-3  w-100 px-3 rounded'/></div>
 
 
</div>
</div>
</div>
    </div>
  )
}
