import React, { useEffect } from 'react'
import { LayoutAuthLawyer } from '../../../Layout'
import BackgroundImage from '../../../assest/Image/dasboardBack.png'
import LawyerShedularIcon from "../../../assest/Image/lawyerscheduler.svg";
import LawyerReciptIcon from "../../../assest/Image/lawyerrecipt.svg";
import Empty from "../../../assest/Image/empty.svg";
import Link from "next/link";
import { MdOutlineArrowBackIosNew } from "react-icons/md";
import Image from 'next/image'
import { LawyerReceiptComp } from '../../../Components';
import Button from '../../../Components/Ui/Button';
import { ReceiptData } from '../../../utils';
import { useAuth } from '../../../hooks/useContext';

export default function Receipts() {
  const {LawyerPaymentFunc,state} =useAuth()
  useEffect(() => {
    LawyerPaymentFunc()
  }, [LawyerPaymentFunc])
  

   //console.log(state,'State')
  return (
    <LayoutAuthLawyer Title={"Receipts"}>
      <div className='container-fluid'>
        <h5>Receipts</h5>

        <div>
        <div className="position-relative">
            <div className="back"><Image src={BackgroundImage} alt=''/></div>
                  {/* <hr /> */}
                  <div className="bg-white py-4 px-md-4 ">
                    <div className="mb-5">
                      <Link href={"/dashboard/lawyer"} >
                      <a>
                        <MdOutlineArrowBackIosNew
                          size={35}
                          className="text-type1 pointer"
                        />
                        </a>
                      </Link>
                      <div className="w-100 text-center d-flex justify-content-center align-items-center">
                        <span className="me-3">
                          <Image src={LawyerReciptIcon} alt="" />
                        </span>
                 <h5 className="text-type2 fw-2">Receipts</h5>
                      </div>
                    </div>
                    <div
                      className="px-2 px-md-4 "
                      style={{ minHeight: "80vh" }}
                    >
                      <div className=" d-flex justify-content-center">
                        <div className="col-md-8">
                        {state?.lawyerPayment?.length >0  &&    <div className="fw-0 mb-3 h5">Recent Receipts</div>}

                          {state?.lawyerPayment?.length ===0 &&<div className="noConversation2 py-5">
                  <div className="noConversation2 text-center">
              <div>
                <Image src={Empty} alt='' layout='' />
                <h5>Nothings here</h5>
              </div>
                {/* No Recent  Yet */}
              </div>
              </div>}

                          {state?.lawyerPayment?.map((item, index) => (
                            <div key={index}>
                              <LawyerReceiptComp data={item} />
                            </div>
                          ))}

                          {/* <div className="mt-5">
                            {" "}
                            <Button
                              bntText={"Proceed"}
                              className="bg-type1 text-white py-3 px-5 w-100 h4"
                            />
                          </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>
    </LayoutAuthLawyer>
  )
}
