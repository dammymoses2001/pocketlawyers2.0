/* eslint-disable react/no-unescaped-entities */
import { useState } from "react";
import {  ForgetpasswordSuccess } from "../../Components";
import { useAuth } from "../../hooks/useContext";
import { LayoutNoAuth } from "../../Layout";

export default function Index() {
  const [toggle, setToogle] = useState(0);
  const {} =useAuth()
  const [userData,setUserData]=useState()

  const handleOnchange = (e) =>{
    const {name,value}= e.target
    setUserData({...userData,[name]:value})
  }

  const handleSubmitEmail = ()=>{
console.log()
  }

  const handleSubmitRestPassword = ()=>{

  }
  return (
    <LayoutNoAuth Title={"Forget Password"} footer>
      <div className="pb-8 pt-5">
       
          <ForgetpasswordSuccess setToogle={setToogle} handleOnchange={handleOnchange}/>
      
      </div>
    </LayoutNoAuth>
  );
}
