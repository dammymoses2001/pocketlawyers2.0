import styledComponents from "styled-components";
import { useRouter } from "next/router";
import Logo from "../../assest/Image/Logo3.svg";

import Logout from "../../assest/Image/logouticon.svg";
import Link from "next/link";
import Image from "next/image";
import Button from "../Ui/Button";
import { useState } from "react";
import { useAuth } from "../../hooks/useContext";
import { sideBarAuthClient, sideBarLawyerAuthClient } from "../../utils";
import { Nav } from "react-bootstrap";
import { RiCloseLine } from "react-icons/ri";

const Style = styledComponents.div`
height:100vh;

.bg1{
    background:red;
    color:white;
}
.bg2{
    background:green;
    color:white;
}

`;
export const SideBar = ({setToogleSideBar}) => {
  const router = useRouter();
  const [toggle,setToggle] =useState(1)
  const {logout}=useAuth();
  const routpath = router.pathname;
  //console.log(toggle,routpath,'toggle')

  const handleToggle = (route,id) => {
    
    setToggle(id)
    router.push(route)
  }
  return (
    <Style>
      <div className="d-flex flex-column w-100 justify-content-between h-100">
        <div>
          <div className='text-end d-md-none mt-2'>
            <span onClick={()=>setToogleSideBar(false)}><RiCloseLine size={25}/></span>
          </div>
          <div className="px-4 mt-3 mb-8">
            <Image src={Logo} alt="" />
          </div>
          <div className="mb-8">
            <div>
              
              <div className="  px-3 ">
             
              {sideBarAuthClient.map((item,index)=>
               <div  key={index} onClick={()=>setToggle(index+1)}>
               <Nav.Link className='p-0' as={Link } href={item?.path}>
   
               <a  className={`d-flex align-items-center ${routpath ===(item?.path)? 'bg-type4 text-white ':'text-muted bg-white'}  px-4 py-2 rounded mb-4`}>
          
               <Image src={routpath=== (item?.path)? item?.icon:item?.icon1} alt="" />{" "}
               <span className="ms-4 mb-0 fw-2 h6">{item?.name}</span>
               </a>
               </Nav.Link>
             </div>
          
              )}  
     
              </div>
            </div>
          </div>
        </div>
        <div className="text-center mb-5 d-flex justify-content-center align-items-center pointer">
      <Button bntText={  <span className=" d-flex justify-content-center align-items-center"><Image src={Logout} alt='' className=""/> Log Out </span>} className="btn fw-2 h5 text-muted d-flex justify-content-center align-items-center"  onClick={logout}/>
        </div>
      </div>
    </Style>
  );
};



export const LawyerSideBar = ({setToogleSideBar}) => {
  const router = useRouter();
  const [toggle,setToggle] =useState(1)
  const {logout}=useAuth();
  const routpath = router.pathname;
  //console.log(toggle,routpath,'toggle')

  const handleToggle = (route,id) => {
    
    setToggle(id)
    router.push(route)
  }
  return (
    <Style>
      <div className="d-flex flex-column w-100 justify-content-between h-100">
        <div>
        <div className='text-end d-md-none mt-2'>
            <span onClick={()=>setToogleSideBar(false)}><RiCloseLine size={25}/></span>
          </div>
          <div className="px-4 mt-3 mb-8">
            <Image src={Logo} alt="" />
          </div>
          <div className="mb-8">
            <div>
              
              <div className="  px-3 ">
             
              {sideBarLawyerAuthClient.map((item,index)=>
               <div  key={index} onClick={()=>setToggle(index+1)}>
                <Nav.Link className='p-0' as={Link } href={item?.path}>
               <a  className={`d-flex align-items-center ${routpath=== (item?.path)? 'bg-type2 text-white ':'text-muted bg-white'}  px-4 py-2 rounded mb-4`}>
               <Image src={routpath=== (item?.path)? item?.icon:item?.icon1} alt="" />{" "}
               <span className="ms-4 mb-0 fw-2 h6">{item?.name}</span>
               </a>
               </Nav.Link>
             </div>
          
              )}  
     
              </div>
            </div>
          </div>
        </div>
        <div className="text-center mb-5 d-flex justify-content-center align-items-center pointer">
      <Button bntText={  <span className=" d-flex justify-content-center align-items-center"><Image src={Logout} alt='' className=""/> Log Out </span>} className="btn fw-2 h5 text-muted d-flex justify-content-center align-items-center"  onClick={logout}/>
        </div>
      </div>
    </Style>
  );
};
