/* eslint-disable @next/next/no-img-element */
import styled from 'styled-components'
import { Swiper, SwiperSlide } from "swiper/react";
// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import Image from 'next/image';
import { FaQuoteLeft } from 'react-icons/fa';



export const Carousel = ({arrayData})=> {
    
  return (
    <SwiperStyle>
      <Swiper
        slidesPerView={1}
        spaceBetween={10}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          968: {
            slidesPerView: 3,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
        modules={[Pagination,Autoplay]}
        className="mySwiper"
      >
        {arrayData.map((item,index)=>  
        <SwiperSlide key={index}>
         <div className='p-3' data-aos="fade-left"  data-aos-delay="100">
         <img src={item} alt='' width={"150px"} height={"105px"} layout={''}/>
         </div>
        </SwiperSlide>)}
      
        
      </Swiper>
    </SwiperStyle>
  )
}


export const Carousel2 = ({arrayData})=> {
    
  return (
    <SwiperStyle2>
      <Swiper
        slidesPerView={1}
        spaceBetween={10}
        navigation={true}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          968: {
            slidesPerView: 3,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 4,
            spaceBetween: 50,
          },
        }}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper"
      >
        {arrayData.map((item,index)=>  
        <SwiperSlide key={index} >
         <div className='rounded p-3 bg-white h-100 rounded-4 ' data-aos="fade-left"  data-aos-delay="100">
       <div style={{width:'48px', height:"48px"}} className='mb-3'>
       <Image src={item?.icon} alt='' blurDataURL='/white.png' placeholder='blur' width={"48px"} height={"48px"} layout={''}/>
       </div>
       <h6 className='text-start text-7  font-5'>{item?.title}</h6>
       <p className='h7 text-justify'>{item?.desc}</p>
         </div>
        </SwiperSlide>)}
      
        
      </Swiper>
    </SwiperStyle2>
  )
}

export const Carousel3 = ({arrayData})=> {
    
  return (
    <SwiperStyle3>
      <Swiper
        slidesPerView={1}
        spaceBetween={5}
        pagination={{
          clickable: true,
        }}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          968: {
            slidesPerView: 3,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 50,
          },
        }}
        modules={[Pagination,Autoplay]}
        className="mySwiper"
      >
        {arrayData.map((item,index)=>  
        <SwiperSlide key={index} style={{background:item?.background}} >
            <div className="h-100 " key={index}  >
                 <div className={`${item?.background} rounded-4 p-3`}>
            <div className="row align-items-center">
              <div className="col-md-4 mb-4" >
                <div><Image src={item?.pic} alt="" width={"100px"} height={"100px"}/></div>
              </div>
              <div className="col-md-7 font-3">
                <div className="fw-bold h51 text-md-start">{item?.name}
</div>
<div className="h7 text-md-start mb-3 mb-md-0">SMEs</div>
              </div>
            </div>
            <div className="d-flex">
             <QuoteStyle Background={item?.iconbackground} className='me-3'> <FaQuoteLeft size={20} color='#fff'/></QuoteStyle>
              <div className="h7 text-justify pe-md-4">{item?.desc} </div>
            </div>
          </div>
              </div>
        </SwiperSlide>)}
      
        
      </Swiper>
    </SwiperStyle3>
  )
}

const QuoteStyle =styled.div`
height: 40px;
width: 40px;
background: ${props=>props?.Background};
border-radius: 50%;
padding: 10px;
display: flex;
justify-content: center;
align-items: center;
`

const SwiperStyle = styled.div`

.swiper {
    width: 100%;
    height: 100%;
  }
  
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
  
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }
  
  .swiper-slide img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .swiper-wrapper{
    height:200px;
  }
  .swiper-button-next{
    right:0 !important;
    color: #218698 !important;
  }
  .swiper-button-prev{
    left:0 !important;
    color: #218698 !important;
  }
  
`

const SwiperStyle2 = styled.div`

.swiper {
    width: 100%;
    height: 100%;
  }
  
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #EAF8FA;
    border-radius:30px !important;
  
    /* Center slide text vertically */
    // display: -webkit-box;
    // display: -ms-flexbox;
    // display: -webkit-flex;
    // display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }
  
  .swiper-slide img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .swiper-wrapper{
   height:250px;

  }
  .swiper-button-next{
    right:0 !important;
    color: #218698 !important;
  }
  .swiper-button-prev{
    left:0 !important;
    color: #218698 !important;
  }
  
`

const SwiperStyle3 = styled.div`

.swiper {
    width: 100%;
    height: 100%;
  }
  
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    // background: ;
    border-radius:30px !important;
  
    /* Center slide text vertically */
    // display: -webkit-box;
    // display: -ms-flexbox;
    // display: -webkit-flex;
    // display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }
  
  .swiper-slide img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .swiper-wrapper{
   height:350px;

  }
  @media (max-width: 540px) {
    @media (max-width: 540px) {
      .swiper-wrapper{
        height:400px;
     
       }
    }
  }
  .swiper-button-next{
    right:0 !important;
    color: #218698 !important;
  }
  .swiper-button-prev{
    left:0 !important;
    color: #218698 !important;
  }
  
`