import React from "react";

export default function Button({
  bntText,
  icon,
  loading,
  className,
  onClick,
  ...props
}) {
  return (
    <div>
      <button onClick={onClick} className={`rounded ${className}`} {...props}>
        {loading ? "Loading..." : bntText}{" "}
        {icon && <i className="ms-3">{icon}</i>}{" "}
      </button>
    </div>
  );
}
