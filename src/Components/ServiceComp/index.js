import React from 'react'
import { TwoSide } from '../TwoSide'
import VisionImage from '../../assest/Image/Pic11.png'
import ServiceImage from '../../assest/Image/serviceicon.svg'
import Image from 'next/image'
import Link from 'next/link'
import styledComponents from 'styled-components'

const Style =styledComponents.div`

`

export const ServicesComp=()=> {
  return (
    <Style className='py-5'>
         <div className='mb-5'>
         <TwoSide
         blockBg
      sideOne={
          <div>
              <h4 className='fw-bold mb-4'>Services</h4>
              <div className='row px-3'>
                <div className='col-lg-2'>
<div className='Wrapper'><Image src={ServiceImage} alt=''/></div>
                </div>
                <div className='col-lg-10'>
                  <h5 className='font-3 fw-1 text-type2'>PocketLawyers  is a virtual law firm platform for individuals and small business that allows them to request for legal services, chat with an attorney and even receive a follow up report. With PocketLawyers you can settle your legal issues from the comfort of your home or office. By democratizing access to affordable legal services, we are helping SMEs and startups manage their legal risks and grow their businesses.</h5>
                </div>
              </div>
             
          </div>
      }
      displayImage={VisionImage}
      switchSide
      />
         </div>
       <div className="text-center mb-5">
                <h5 className="fw-2 mb-5">Reach Out to us to Discuss your Legal solutions</h5>
                <div>
                    <Link href='/contact'>
                    <a  className='h4 fw-bold bg-type2 py-3 px-8 text-white rounded'>Get in touch with us </a>
                    </Link>
                </div>
            </div>
    </Style>
  )
}
