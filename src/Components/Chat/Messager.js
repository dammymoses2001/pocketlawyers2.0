/* eslint-disable @next/next/no-img-element */
import  { useEffect,useState } from 'react'
import {AiOutlineFileZip} from 'react-icons/ai'
import { GeneralUrl } from '../../utils'

export const Messager =({item,userProfile,message})=> {

    const [messageData,setMessageData] =useState()

    useEffect(() => {
     
    }, [])

    const filter = (item)=>{
      // console.log(item,'chcking', item?.includes("docx"))
      if(item?.includes("docx") || item?.includes("pdf")){
        return item
      }
      //item?.media|| item?.mediaPath  && (GeneralUrl+item?.mediaPath)
      return GeneralUrl+item
    }
    
  return (
    <div>
         
         <div>
                      {item?.sender=== userProfile?.user?.id && (
                        <div className="text-end mb-3">
                          <div>
                          <div className="d-flex align-items-center justify-content-end">
                            <span className="me-3 px-3 py-1 rounded bg-6 text-7 h9 maxwidth">
                           { item?.mediaPath && 
                             <div className=" me-3">
                             {/* {console.log(item,'looking')}  */}
                             <a
                             target={"_blank"}
                               href={
                              filter(item?.media|| item?.mediaPath)
                               }
                               download
                               alt="" rel="noreferrer"
                             >
                               <AiOutlineFileZip size={30}/>
                             </a>
                           </div>
                           } 
                           { item?.media  && 
                             <div className=" me-3">
                             <a
                             target={"_blank"}
                               href={
                                filter(item?.media|| item?.mediaPath)
                               }
                               download
                               alt="" rel="noreferrer"
                             >
                               <AiOutlineFileZip size={30}/>
                             </a>
                           </div>
                           } 
                         
                              {item?.text}
                            </span>
                            <div className="imageWrapperMessage me-3">
                              <img
                                src={
                                  userProfile?.user?.pic_name ? (GeneralUrl+userProfile?.user?.pic_name): "https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg"
                                }
                                alt=""
                              />
                            </div>
                          </div>
                        </div>
                        </div>
                      )}

                      {item?.sender !==userProfile?.user?.id && (
                        <div className="mb-3">
                          <div className="d-flex align-items-center justify-content-start">
                            <div className="imageWrapperMessage me-3">
                              <img
                                src={
                                  message?.user?.pic_name?  (GeneralUrl+message?.user?.pic_name):'https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg'
                                }
                                alt=""
                              />
                            </div>
                            <span className="me-3 h9 px-3 py-1 rounded bg-type2 text-white maxwidth rounded-bottom">
                            { item?.mediaPath && 
                             <div className=" me-3">
                             <a
                             target={"_blank"}
                               href={
                                 item?.mediaPath && (GeneralUrl+item?.mediaPath)
                               }
                               download
                               alt="" rel="noreferrer"
                             >
                               <AiOutlineFileZip size={30}/>
                             </a>
                           </div>
                           } 
                              {item?.text}
                            </span>
                          </div>
                        </div>
                      )}
                    </div>
    </div>
  )
}
