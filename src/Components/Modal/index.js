import { Modal } from 'react-bootstrap'
import React from 'react'
import Button from '../Ui/Button'
import styledComponents from 'styled-components'


const Style =styledComponents.div`
background:${props=>props.BG ?props.BG :'white' };
padding:1rem;

`

export const ModalComp =({show,size,bg,handleClose,bodyData,center,...props})=> {
  return (
    <>
         <Modal show={show} onHide={handleClose}
         centered={center?true:false}
          size={size}
          aria-labelledby="contained-modal-title-vcenter"
          // centered
          {...props}
         
         >
       <Style BG={bg}>{bodyData}</Style>
      
          
        
        
         {/* <Modal.Footer>
         <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button> 
        </Modal.Footer>*/}
      </Modal>
    </>
  )
}
