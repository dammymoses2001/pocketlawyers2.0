import Head from 'next/head'
import '../styles/globals.css'
import {Providers} from '../Context/providers'
import SSRProvider from "react-bootstrap/SSRProvider";
import Aos from 'aos';
import { aosComp } from '../utils';
import { QueryClientProvider,QueryClient } from 'react-query';
import { Toaster } from "react-hot-toast";
import { useEffect } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "animate.css";
// eslint-disable-next-line
import "swiper/css/bundle";
import 'aos/dist/aos.css'; 

const queryClient = new QueryClient()
function MyApp({ Component, pageProps }) {
  useEffect(() => {
    Aos.init(aosComp);
  }, []);
  return <>
   <Head>
        <title>PocketLawyers</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/icon.svg" />
      </Head>
      <Providers>
        <SSRProvider>
        <QueryClientProvider client={queryClient}>
            <Toaster />
            <Component {...pageProps} />
            </QueryClientProvider>
        </SSRProvider>
      </Providers>
  </>
}

export default MyApp
