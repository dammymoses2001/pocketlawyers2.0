/* eslint-disable @next/next/no-img-element */
import  { useEffect,useState } from 'react'

export const MessagerLawyer =({item,userProfile,message})=> {

    const [messageData,setMessageData] =useState()

    useEffect(() => {
     
    }, [])
    
  return (
    <div>
         
         <div>
                      {item?.sender !== userProfile?.user?.id && (
                        <div className="text-end mb-3">
                          <div className="d-flex align-items-center justify-content-end">
                          {/* {console.log(item,'looking')}  */}
                            <span className="me-3 px-3 py-1 rounded bg-6 text-7 h9 maxwidth">
                              {item?.text}
                            </span>
                            <div className="imageWrapperMessage me-3">
                              <img
                                src={
                                   "https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg"
                                }
                                alt=""
                              />
                            </div>
                          </div>
                        </div>
                      )}

                      {item?.sender ===userProfile?.user?.id && (
                        <div className="mb-3">
                          <div className="d-flex align-items-center justify-content-start">
                            <div className="imageWrapperMessage me-3">
                              <img
                                src={
                                  message?.user?.pic_name?  message?.user?.pic_name:'https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg'
                                }
                                alt=""
                              />
                            </div>
                            <span className="me-3 h9 px-3 py-1 rounded bg-type2 text-white maxwidth rounded-bottom">
                              {item?.text}
                            </span>
                          </div>
                        </div>
                      )}
                    </div>
    </div>
  )
}
