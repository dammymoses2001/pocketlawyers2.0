import styled from "styled-components"
import Logo from '../../../assest/Image/loginImage.svg'
import Image from 'next/image'
import { InputComp } from "../../Ui"
import Button from "../../Ui/Button"
import Link from 'next/link'
import { useState } from "react"

const Style = styled.section`
padding:5rem 0 15rem 0;
.form-width{
    width:40%;
}
@media (max-width: 992px) {
    .form-width{
        width:60% !important;
    }
}
@media (max-width: 786px) {
    .form-width{
        width:80% !important;
    }
}
`
export const LawyerCreatePassword =({userData,handleOnChange,handelSubmit,setSignupLevel,loading})=> {
    const [level,setLevel]=useState(0)
  return (
    <Style>
        <div className="container  text-black">
            <h5 className="fw-2 mb-5 text-center">Create Password</h5>
            
            <div className="d-flex justify-content-center">
                <div className="form-width">
                    <div>
                    <InputComp label={"Password"} className='border-3 py-3' placeholder='Password' typeComp={'password'}
                    name='password'
                    
                    icon
                    
                    value={userData?.password} onChange={handleOnChange}
                    />
                    </div>
                    <div className="">
                    <InputComp label={"Confirm Password"} icon className='border-3 py-3'   placeholder='Confirm password' typeComp={'password'}
                     name='confirm_password'
                     value={userData?.confirm_password} onChange={handleOnChange}
                    />
                    </div>
                    <div className="text-center mt-7">
                        <Button  onClick={handelSubmit} loading={loading} disabled={loading}  bntText={'Submit'} className='w-100 py-3 h4 fw-bolder bg-type1 text-white mb-3'/>
                        <Button  onClick={()=>setSignupLevel(2)}   bntText={'Go back'} className='w-100 py-3 h4 fw-bolder bg-8 text-black border mb-3'/>
                      
                    </div>
                </div>
            </div>
        </div>
    </Style>
  )
}
