import { useState } from "react";
import { FiEyeOff, FiEye } from "react-icons/fi";
import Link from 'next/link'
import {Form} from 'react-bootstrap'
import styledComponents from "styled-components";
import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";
import CurrencyInput from 'react-currency-input-field';



export const InputComp = ({ label,labelClassName, className,smallText, icon = false,typeComp,required,borderClassName, ...props }) => {
  const [toogle, setToogle] = useState(false);
  return (
    <div>
      <div className="mb-3 mb-lg-4">
        {label && (
          <label className={`form-label  fw-2 mb-2 fw-2 ${labelClassName}`}>
            {label} {required && <span className="text-danger">*</span>}
          </label>
        )}
        <div className={` align-items-center borderC-3 rounded inputWrapper ${icon &&'d-flex pe-3'} mb-1 bg-white ${borderClassName}`}>
          <input
            className={`form-control w-100 border-0 py-2 me-2 ${className}`}
            type={!toogle ?typeComp:'text'}
            {...props}
          />
          {icon ? (
            !toogle ? (
              <span className="text-muted" onClick={() => setToogle(!toogle)}>
                {" "}
                <FiEyeOff size={25} />
              </span>
            ) : (
              <span className="text-muted" onClick={() => setToogle(!toogle)}>
                <FiEye size={25} />
              </span>
            )
          ) : (
            ""
          )}{" "}
        </div>
        {!props?.value && props?.showError &&<small className='text-danger text-capitalize fw-2'>{`${label} is required....` }</small>} 

      {smallText &&<div className="text-end"> <Link href='/forgot-password'><a className="text-7 fw-1 h7">Forget Password</a></Link></div> }  
      </div>
    </div>
  );
};

const Style =styledComponents.div`
.Wrapper{
  border:2px solid ${props=>props.bordercolor ?props.bordercolor :props.bg};
  background:${props=>props.bg?props.bg :''}
}
input{
  // border:2px solid ${props=>props.bg} !important;
  background:${props=>props.bg?props.bg :''} !important;
}
.form-label {
  font-size: ${props=>props.labelSize ?props.labelSize  :'18px'} !important;
}
input::-webkit-input-placeholder {
  font-size: ${props=>props.placeholdersize==='sm' ?'14px' :'18px'};
  line-height: 3;
}
`
const StyleTAg =styledComponents.div`
.react-tag-input__input,.react-tag-input__tag{
  padding:1rem 0;
  height:auto !important;
}
`
export const InputComp2 = ({ label, bordercolor, placeholdersize,labelClassName,labelSize, className,smallText, icon = false,typeComp,required,bg,borderClassName, ...props }) => {
  const [toogle, setToogle] = useState(false);
  return (
    <Style bg={bg} bordercolor={bordercolor} labelSize={labelSize} placeholdersize={placeholdersize}>
      <div className="mb-3 mb-lg-4">
        {label && (
          <label className={`form-label  text-capitalize mb-2 fw-2 ${labelClassName}`}>
            {label} {required && <span className="text-danger">*</span>}
          </label>
        )}
        <div className={'border-2 Wrapper rounded'}>
          <input
            className={`form-control rounded w-100 border-0 py-2 me-2 ${className}  `}
            type={typeComp}
            {...props}
          />
        
        </div>
      {smallText &&<div className="text-end"> <Link href='/forgetpassword'><a className="text-type1 fw-1">Forget Password</a></Link></div> }  
      </div>
    </Style>
  );
};


export const InputCompTag = ({ label, bordercolor, placeholdersize,labelClassName,labelSize,
   className,smallText, icon = false,typeComp,required,bg,borderClassName,setProfessionalBody,professionalBody, ...props }) => {
  // const [tags, setTags] = useState(["example tag"])
  return (
    <div className="mb-3 mb-lg-4">
    {label && (
      <label className="form-label  fw-2 mb-2 fw-2">
        {label} {required && <span className="text-danger">*</span>}
      </label>
    )}
    <StyleTAg bg={bg} bordercolor={bordercolor} labelSize={labelSize} placeholdersize={placeholdersize}>
       <ReactTagInput 
       className='py-3'
      tags={professionalBody} 
      onChange={(newTags) => setProfessionalBody(newTags)}
    />

    </StyleTAg>
    {professionalBody?.length===0 && props?.showError &&<small className='text-danger text-capitalize fw-2'>{`${props?.name} is required and press "Enter"....` }</small>} 

    </div>
  );
};

export const MoneyInputComp =({LabelText,LabelClassName,inputclassname,handleChange,...props})=> {
  return (
    <div>
    <Form.Group className={ `border-0 ${LabelClassName}`}>
  {LabelText && <Form.Label>{LabelText}</Form.Label>}  
  <div>
  <CurrencyInput 
  className={inputclassname}  
  placeholder="Please enter a number"
  // defaultValue={000}
   decimalsLimit={2}
   onValueChange={handleChange} {...props}/>
  </div>
    {/* <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text> */}
  </Form.Group>
    </div>
  )
}