/* eslint-disable @next/next/no-img-element */
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { AiFillPlusCircle } from "react-icons/ai";
import { FiPlus } from "react-icons/fi";
import styled from "styled-components";
import { FaqData, ourClientData, retailSolutionData } from "../../utils";
import { Carousel, Carousel2 } from "../Carousel";
import { Accordiance } from "../Ui";


const WrapperStyle =styled.div`
margin: 0 5px;
`

export const OurClient = () => {
  return (
    <WrapperStyle>
      <div className="container bg-13 py-5 rounded-4 mt-3 ">
        <div className="d-flex justify-content-center mb-5">
          <div className="col-lg-10 text-center text-lg-start">
            <h5>Our Clients</h5>
            <h3 className="font-4 text-9">Satisfied Startups & Businesses</h3>
          </div>
        </div>

        <div className="px-2 px-lg-5">
          <Carousel arrayData={ourClientData} />
        </div>
      </div>
      <div className=" py-5"></div>
    </WrapperStyle>
  );
};

export const WhatWeCanOffer = () => {
  return (
    <>
      <div className="container bg-13 py-5 rounded-4 mt-3">
        <div className="d-flex justify-content-center mb-5">
          <div className="col-lg-10 text-center text-lg-start">
            <h5>What we Offer</h5>
            <h3 className="font-4 text-9">Retail Solutions</h3>
          </div>
        </div>

        <div className="px-2 px-lg-5">
          <Carousel2 arrayData={retailSolutionData} />
        </div>
      </div>
      <div className="my-4 py-5"></div>
    </>
  );
};


export const RetailerPackages = () => {
  return (
    <>
    {/* <div className="py-4"></div> */}
      <div className="container bg-13 py-5 rounded-4 mt-3 ">
        <div className="d-flex justify-content-center mb-5">
          <div className="col-lg-10 text-center  text-lg-start">
            <h5 className="text-capitalize">we fit into your pocket, no matter your budget.</h5>
            <h3 className="font-4 text-9">Retainer Packages</h3>
          </div>
        </div>

        <div className="d-flex justify-content-center">
          <div className="col-10">
          <div className="row g-5">
         <div className="col-lg-6" data-aos="fade-down">
          <div className="bg-white ps-3 ps-lg-5 py-3 rounded">
            <div className="p-3">
              <div style={{width:'48px', height:'48px'}} className='mb-3'>
                <img src="/box.svg" className="w-100" alt=""/>
              </div>
              <h6 className="text-7 font-5">Small Scale Business</h6>
              <p className="font-1 h7 col-lg-7">
              Viverra amet, aliquam pretium consequat. Viverra amet, aliquam pretium consequat.
              </p>
              <div className="h9 text-9 text-decoration-underline fw-bold">See More</div>
            </div>
          </div>
         </div>
         <div className="col-lg-6" data-aos="fade-down">
          <div className="bg-white ps-3 ps-lg-5 py-3 rounded">
            <div className="p-3">
              <div style={{width:'48px', height:'48px'}} className='mb-3'>
                <img src="/box.svg" className="w-100" alt=""/>
              </div>
              <h6 className="text-7 font-5">Medium Scale Business</h6>
              <p className="font-1 h7 col-lg-7">
              This service is advised for Businesses/Corporations that want to outsource their Legal Departments. 
              </p>
              <div className="h9 text-9 text-decoration-underline fw-bold">See More</div>
            </div>
          </div>
         </div>
        </div>
          </div>
        </div>
      </div>
      <div className=" py-4"></div>
    </>
  );
};



export const FAQs = () => {
  return (
    <>
    <div className="py-4"></div>
      <div className="container py-5 rounded-4 mt-3">
        <div className="d-flex justify-content-center mb-5">
          <div className="text-center col-lg-10 text-lg-start">
            <h5 className="text-capitalize">Frequently Asked Questions</h5>
            <h3 className="font-4 text-9">FAQs</h3>
          </div>
        </div>
        {FaqData?.slice(undefined,7)?.map((item,index)=>

<div key={index} className="font-3 d-flex justify-content-center" data-aos="fade-left">
<div className="col-10 col-lg-8">
<Accordiance
id={index}
display={"none"}
Accordiontitle={<div className="d-flex align-items-end fw-2 font-1 h51"><span className="dot me-3 p-1"><FiPlus color="#fff"/></span>{item?.title}</div>}
AccordionBody={
    <div className="d-flex align-item-center ms-3">
        <div className="me-2 px-1 rounded py-4 my-2 bg-type1"></div>
        <div className="w-100 lh-base fw-3 text-justify" dangerouslySetInnerHTML={{__html: item?.desc}}>
        </div>
    </div>
}
/>
<hr className="my-2 bg-type4"/>
</div>






</div>
         )}
         <div className="col-lg-10" data-aos="fade-down-right" data-aos-delay="500">
          <div className="h7 text-center text-lg-end text-7 fw-bolder text-decoration-underline mt-3 pointer">
            <Link href={"/faq"}>< a className="h7 text-center text-lg-end text-7 fw-bolder text-decoration-underline mt-3 pointer" >See More</a></Link>
          </div>
         </div>
      </div>
      <div className=" py-3"></div>
    </>
  );
};