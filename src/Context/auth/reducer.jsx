/* eslint-disable sort-keys */
export const AUTH_START = "AUTH_START";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const LOGOUT_FAIL = "LOGOUT_FAIL";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const FORGET_PASSWORD_FAIL = "FORGET_PASSWORD_SUCCESS_FAIL";
export const FORGET_PASSWORD_SUCCESS = "FORGET_PASSWORD_SUCCESS";
export const RESET_PASSWORD_FAIL = "FORGET_PASSWORD_SUCCESS_FAIL";
export const RESET_PASSWORD_SUCCESS = "FORGET_PASSWORD_SUCCESS";
export const LOGOUT_START = "LOGOUT_START";
export const GET_CURRENT_USER_SUCCESS = "GET_CURRENT_USER_SUCCESS";
export const GET_CURRENT_USER_FAIL = "GET_CURRENT_USER_FAIL";
export const GET_COUNTRIES_SUCCESS = "GET_COUNTRIES_SUCCESS";
export const GET_LAWYER_SPECIALIZATION_FAIL = "GET_LAWYER_SPECIALIZATION_FAIL";
export const GET_LAWYER_SPECIALIZATION_SUCCESS =
  "GET_LAWYER_SPECIALIZATION_SUCCESS";
export const GET_COUNTRIES_FAIL = "GET_COUNTRIES_FAIL";
export const GET_STATE_SUCCESS = "GET_STATE_SUCCESS";
export const GET_STATE_FAIL = "GET_STATE_FAIL";
export const GET_CAMPUS_SUCCESS = "GET_CAMPUS_SUCCESS";
export const GET_CAMPUS_FAIL = "GET_CAMPUS_FAIL";
export const GET_RETAINER_PACKAGES_SUCCESS = "GET_RETAINER_PACKAGES_SUCCESS";
export const GET_RETAINER_PACKAGES_FAIL = "GET_RETAINER_PACKAGES_FAIL";
export const GET_RETAINER_PACKAGES_SUBSCRIPTION_SUCCESS =
  "GET_RETAINER_PACKAGES_SUBSCRIPTION_SUCCESS";
export const GET_RETAINER_PACKAGES_SUBSCRIPTION_FAIL =
  "GET_RETAINER_PACKAGES_SUBSCRIPTION_FAIL";
export const GET_RETAIL_SOLUTION_SUCCESS = "GET_RETAIL_SOLUTION_SUCCESS";
export const GET_RETAIL_SOLUTION_FAIL = "GET_RETAIL_SOLUTION_FAIL";
//
export const UPDATE_USER_PROFILE_SUCCESS = "UPDATE_USER_PROFILE_SUCCESS";
export const UPDATE_USER_PROFILE_FAIL = "UPDATE_USER_PROFILE_FAIL";
export const GET_SECOND_USER_SUCCESS = "GET_SECOND_USER_SUCCESS";
export const GET_SECOND_USER_FAIL = "GET_SECOND_USER_FAIL";
//convsersation/messages/chat
export const GET_ALL_CONVERSATION_SUCCESS = "GET_ALL_CONVERSATION_SUCCESS";
export const GET_ALL_CONVERSATION_FAIL = "GET_ALL_CONVERSATION_FAIL";
export const SEND_MESSAGE_SUCCESS = "SEND_MESSAGE_SUCCESS";
export const SEND_MESSAGE_FAIL = "SEND_MESSAGE_FAIL";

export const SUBSCRIBE_TO_PACKAGE_PLAN_SUCCESS =
  "SUBSCRIBE_TO_PACKAGE_PLAN_SUCCESS";
export const SUBSCRIBE_TO_PACKAGE_PLAN_FAIL = "SUBSCRIBE_TO_PACKAGE_PLAN_FAIL";

export const GET_CLIENT_SUB_SERVICE_SUCCESS = "GET_CLIENT_SUB_SERVICE_SUCCESS";
export const GET_CLIENT_SUB_SERVICE_FAIL = "GET_CLIENT_SUB_SERVICE_FAIL";

export const UPDATE_USER_PROFILE_PIC_SUCCESS =
  "UPDATE_USER_PROFILE_PIC_SUCCESS";
export const UPDATE_USER_PROFILE_PIC_FAIL = "UPDATE_USER_PROFILE_PIC_FAIL";

export const GET_LIST_LAWYER_FAIL = "GET_LIST_LAWYER_FAIL";
export const GET_LIST_LAWYER_SUCCESS = "GET_LIST_LAWYER_SUCCESS";

export const INITIATE_LAWYER_FAIL = "INITIATE_LAWYER_FAIL";
export const INITIATE_LAWYER_SUCCESS = "INITIATE_LAWYER_SUCCESS";



export const GET_KYC_SUCCESS = "GET_KYC_SUCCESS";
export const GET_PRESS_SUCCESS = "GET_PRESS_SUCCESS";
export const GET_NEWS_FEEDS_SUCCESS = "GET_NEWS_FEEDS_SUCCESS";
export const POST_KYC_SUCCESS = "POST_KYC_SUCCESS";



export const POST_SUPPORT_SUCCESS = "POST_SUPPORT_SUCCESS";
export const GET_LAWYER_PAYMENT_SUCCESS = "GET_LAWYER_PAYMENT_SUCCESS";
export const GET_USER_RETAIL_SOLUTION_SUBSCRIPTION ="GET_USER_RETAIL_SOLUTION_SUBSCRIPTION";
export const DEFAULT_SUCCESS = "DEFAULT_SUCCESS";

// export const GET_LAWYER_ = "SUBSCRIBE_TO_PACKAGE_PLAN_SUCCESS";
// export const SUBSCRIBE_TO_PACKAGE_PLAN_FAIL = "SUBSCRIBE_TO_PACKAGE_PLAN_FAIL";

export const authReducer = (state, action) => {
  // //console.log(action.type);
  ////console.log(action,'hello')
  switch (action.type) {
    case AUTH_START:
      return {
        ...state,
        isLoading: true,
      };

    case REGISTER_SUCCESS:
      return {
        ...state,
        data: action.payload,
        error: null,
        isLoading: false,
        isLoggedIn: true,
      };

    case REGISTER_FAIL:
      return {
        ...state,
        data: null,
        error: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        data: action.payload,
        error: null,
        isLoading: false,
        isLoggedIn: true,
      };

    case LOGIN_FAIL:
      return {
        ...state,
        data: null,
        error: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case LOGOUT_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case GET_CURRENT_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,

        isLoading: false,
        isLoggedIn: false,
      };
    case GET_SECOND_USER_SUCCESS:
      return {
        ...state,
        otheruser: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    // case GET_CURRENT_USER_SUCCESS:
    //   return {
    //     ...state,
    //     user: action.payload,

    //     isLoading: false,
    //     isLoggedIn: false,
    //   };
    case GET_SECOND_USER_SUCCESS:
      return {
        ...state,
        otheruser: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case GET_COUNTRIES_SUCCESS:
      return {
        ...state,
        countries: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case GET_CAMPUS_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case GET_CAMPUS_SUCCESS:
      return {
        ...state,
        campuses: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case INITIATE_LAWYER_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case INITIATE_LAWYER_SUCCESS:
      return {
        ...state,
        IntiateLawyers: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case GET_CLIENT_SUB_SERVICE_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case GET_CLIENT_SUB_SERVICE_SUCCESS:
      return {
        ...state,
        client_Sub_Service: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case GET_COUNTRIES_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case GET_STATE_SUCCESS:
      return {
        ...state,
        state: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case GET_STATE_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case GET_LAWYER_SPECIALIZATION_SUCCESS:
      return {
        ...state,
        lawyerspecialization: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case GET_LAWYER_SPECIALIZATION_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };

    case GET_LIST_LAWYER_SUCCESS:
      return {
        ...state,
        lawyerList: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case GET_LIST_LAWYER_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };

    case GET_RETAINER_PACKAGES_SUCCESS:
      return {
        ...state,
        retainer_data: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case GET_RETAINER_PACKAGES_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case GET_RETAIL_SOLUTION_SUCCESS:
      return {
        ...state,
        retailsolutions: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case GET_RETAIL_SOLUTION_SUCCESS:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };

    case GET_ALL_CONVERSATION_SUCCESS:
      return {
        ...state,
        conversations: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };

    case GET_ALL_CONVERSATION_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };

    case GET_RETAINER_PACKAGES_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        subscription_services: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case GET_RETAINER_PACKAGES_SUBSCRIPTION_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case UPDATE_USER_PROFILE_SUCCESS:
      return {
        ...state,
        user_profile: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case UPDATE_USER_PROFILE_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case UPDATE_USER_PROFILE_PIC_SUCCESS:
      return {
        ...state,
        user_profile_pic: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case UPDATE_USER_PROFILE_PIC_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    case SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        sendMessage: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
    case SEND_MESSAGE_FAIL:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
      case GET_KYC_SUCCESS:
      return {
        ...state,
        kycInfo: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
      case POST_KYC_SUCCESS:
      return {
        ...state,
        kycmessage: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
      case POST_SUPPORT_SUCCESS:
      return {
        ...state,
        supportMessage: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
      case GET_LAWYER_PAYMENT_SUCCESS:
      return {
        ...state,
        lawyerPayment: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
      case GET_USER_RETAIL_SOLUTION_SUBSCRIPTION:
      return {
        ...state,
        retailsolutionsubscribtion: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
      case GET_PRESS_SUCCESS:
      return {
        ...state,
        getPress: action.payload,
        isLoading: false,
        isLoggedIn: false,
      };
      case GET_NEWS_FEEDS_SUCCESS:
      return {
        ...state,
        newFeeds: action.payload,
        newFeedsLawyer:action.payload?.filter((item)=>item?.userType.includes('Lawyer')),
        newFeedsClient:action.payload?.filter((item)=>item?.userType.includes('Client')),
        isLoading: false,
        isLoggedIn: false,
      };
      case DEFAULT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        defaultMessage:action?.payload
       
      };
    
    default:
      return state;
  }
};
