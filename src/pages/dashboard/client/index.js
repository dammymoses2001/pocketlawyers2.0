/* eslint-disable @next/next/no-img-element */
import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { LayoutAuth } from "../../../Layout";
import HomeTopIcon from "../../../assest/Image/homeTop.svg";

import MoneyIcon from "../../../assest/Image/moneyicon.svg";
import MoneyIcon1 from "../../../assest/Image/moneyicon1.svg";
import LogoDashBoard from "../../../assest/Image/logodashboard.svg";
//import Requesting from "../../../assest/Image/requesting.svg";
import BackgroundImage from "../../../assest/Image/dasboardBack.png";
import People from "../../../assest/Image/people.svg";
import { MdArrowBackIos } from "react-icons/md";
import { io } from "socket.io-client";
import moment from "moment";

import {
  CheckBoxx,
  GetLawyerProfile,
  InputComp2,
  Loader,
  MyCalendar,
  RadioComp,
  RadioComp2,
  RadioComp4,
  RCard3,
  RCard4,
  RCard5,
  SelectComp,
  UpcomingEvent,
} from "../../../Components";
import {
  dashBoardServices,
  GeneralUrl,
  handleRetailServices,
  retailPackage2,
  Years,
} from "../../../utils";
import { useAuth } from "../../../hooks/useContext";
import { ModalComp } from "../../../Components/Modal";
import { MdOutlineArrowBackIosNew } from "react-icons/md";
import Button from "../../../Components/Ui/Button";

export default function Index() {
  const {
    logout,
    getRetainerPackageFunc,
    state,
    getRetainerPackageSubscriptionFunc,
    subscribeRetainerPackageFunc,
    getRetailSolutionFunc,
    userProfile,
    fetchLawyerForSpecializedServicefunc,
    IntiateLawyerFunc,
    getClientSubServiceFunc,
    getKYCFunc,
    PostKYCFunc,
    getUserRetainSolutionSubscriptionFunc,
    GetNewsFeeds
  } = useAuth();
  const [retailService, setRetailServices] = useState(null);
  const socket = useRef();
  const [retailerServiceData, setRetailerServiceData] = useState(null);
  const [show, setShow] = useState(false);
  const [requesting, setRequesting] = useState(false);
  const [oneTimeForm, setOneTimeForm] = useState([]);
  //toogle for form
  const [formToggle, setFormToggle] = useState(null);
  //retainerpackage payment
  const [switchfromPackageToPay, setSwitchFromPackageToPay] = useState(null);
  const [RetainerPackageId, setRetainerPackageId] = useState();
  const [retailSolutionId, setRetailSolutionID] = useState(null);
  //get retainer price before proceed button can work
  const [activeRetainerButton, setActiveRetainerButton] = useState();
  //request lawyer
  const [showLawyer, setShowLawyer] = useState(false);
  //change retail solution if not subscribe or activated
  const [chnageRetailService, setChangeRetailService] = useState(false);

  useEffect(() => {
    socket.current = io(GeneralUrl);
  }, []);
 


  useEffect(() => {
    socket?.current?.emit("addUser", userProfile?.user?.id);
    // socket.current.on("getOnlineUsers", (users) => {
    //   //console.log(users, "");
    // });
  }, [userProfile?.user?.id]);

  useEffect(() => {
    getClientSubServiceFunc();
    getUserRetainSolutionSubscriptionFunc();
  }, [getClientSubServiceFunc, getUserRetainSolutionSubscriptionFunc]);

  useEffect(() => {
    getKYCFunc();
  }, [getKYCFunc]);

  useEffect(() => {
    GetNewsFeeds()
  }, [GetNewsFeeds])

  const handleOnChangeOneTimeForm = (e) => {
    //   {
    //     "designation":"CEO",
    //     "businessName":"Horse and Dog",
    //     "businessDescription": "Horse and Dog",
    //     "businessSector": "Agriculture",
    //     "businessAddress": "117, Modakeke Avn. Challenge bus-stop Ibadan, Oyo state, Nigeria.",
    //     "businessEstablishmentYear": 2015
    // }
    const { name, value } = e.target;
    setOneTimeForm({ ...oneTimeForm, [name]: value });
  };
  const handleOneTimeFormSubmit = () => {
    PostKYCFunc(oneTimeForm);
  };

  const handleClose = () => setShow(false);

  useEffect(() => {
    if (!state?.isLoading && state?.lawyerList) {
      setShowLawyer(true);
    }
  }, [state?.isLoading, state?.lawyerList]);

  const HandleClose = () => {
    setRetailServices("");
  };

  const handleRetainerPackage = (
    retailerServiceData,
    retailPackageSubscription
  ) => {
    if (retailerServiceData) {
      return (
        <div className="overflow-hidden">
          <div className="bg-white px-3 py-4 position-relative overflow-hidden">
            <div className="back">
              <Image src={BackgroundImage} alt="" />
            </div>
            <div className="d-flex align-items-center mb-5 px-md-3">
              <span
                onClick={() => setRetailerServiceData("")}
                className="pointer"
              >
                <MdOutlineArrowBackIosNew size={30} className="text-type1" />
              </span>
              <div className="w-100 text-center">
                <h5 className="text-type2 fw-2 mb-0 mt-5">
                  {retailerServiceData?.long_name}
                </h5>
              </div>
            </div>
            <div className="position-relative">
              <div className="row justify-content-center">
                <div className="col-12 col-lg-8  text-center">
                  <h7 className="text-center ">{retailerServiceData?.desc1}</h7>
                  <div className="text-end mt-3 mb-5">
                    <a className="text-type1">
                      <u>Learn More</u>
                    </a>
                  </div>

                  {retailPackageSubscription?.isLoading ? (
                    <Loader height={"10vh"} />
                  ) : (
                    <div className="overflow-hidden">
                      <h5 className="text-type1 mb-3">Subscription Plans</h5>

                      <div className="mb-7">
                        <table className="table table table-bordered">
                          <tbody className="text-type1">
                            {retailPackageSubscription?.subscription_services?.map(
                              (item, index) => (
                                <tr key={index}>
                                  <td className="text-start h7">
                                    {item?.plan_type}
                                  </td>
                                  <td colSpan={4} className=" h7">
                                    <RadioComp2
                                      id={"price"}
                                      labelClassName={"h7"}
                                      hrnull
                                      value={item?.price}
                                      checked={activeRetainerButton}
                                      label={item?.price}
                                      brColor="#ed6710"
                                      onChange={(e) =>
                                        setActiveRetainerButton(e.target.value)
                                      }
                                    />
                                  </td>
                                </tr>
                              )
                            )}
                          </tbody>
                        </table>
                      </div>
                      <div>
                        <Button
                          bntText={"Proceed"}
                          disabled={!activeRetainerButton ? true : false}
                          className={`${
                            activeRetainerButton
                              ? "bg-type1"
                              : "text-muted border-0"
                          } py-3 fw-bold h5 px-5 text-white rounded `}
                          onClick={() => {
                            setSwitchFromPackageToPay(1);
                          }}
                        />
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return null;
  };

  const handleRetainerPayment = (state) => {
    return (
      <div>
        <div className="bg-white container position-relative">
          <div className="back">
            <Image src={BackgroundImage} alt="" />
          </div>
          <div className="d-flex align-items-center mb-4 px-md-3 ">
            <span
              onClick={() => setSwitchFromPackageToPay(0)}
              className="pointer"
            >
              <MdOutlineArrowBackIosNew size={30} className="text-type1" />
            </span>
            <div className="w-100 text-center">
              <h5 className="text-type2 fw-1 mb-0 mt-5">
                Select Payment method
              </h5>
            </div>
          </div>

          <div className="pb-5  position-relative">
            <div className="row justify-content-center">
              <div className="col-12 col-lg-8 mb-4 ">
                <div>
                  <div className="d-flex align-items-center rounded w-100 bg-type3 py-3 px-3">
                    <span className="me-2">
                      {" "}
                      <Image src={MoneyIcon} alt="" />
                    </span>
                    <div className="w-100">
                      {" "}
                      <RadioComp2
                        className={"h4 fw-2"}
                        label={"Bank Transfer"}
                        id="payment"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-lg-8 mb-7 ">
                <div>
                  <div className="d-flex align-items-center rounded w-100 bg-type3 p-3">
                    <span className="me-2">
                      {" "}
                      <Image src={MoneyIcon1} alt="" />
                    </span>
                    <div className="w-100">
                      {" "}
                      <RadioComp2
                        className={"h4 fw-2"}
                        label={"Credit/Debit Card"}
                        id="payment"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-7 text-center">
                <div className="text-center">
                  <Button
                    loading={state?.isLoading}
                    bntText={"Proceed"}
                    onClick={() => {
                      //console.log(retailerServiceData, "retailerServiceData");
                      subscribeRetainerPackageFunc(retailerServiceData?.id);
                      //setSwitchFromPackageToPay(3)
                    }}
                    className="h5 bg-type1 w-100 py-3 fw-bold rounded text-white"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  ////console.log(switchfromPackageToPay,'switchfromPackageToPay')
  const handleRetainerPaymentSuccessFul = (switchfromPackageToPay) => {
    if (switchfromPackageToPay === 3) {
      return (
        <div>
          <div className="bg-white container py-5  position-relative">
            <div className="back">
              <Image src={BackgroundImage} alt="" />
            </div>
            <div className="d-flex align-items-center mb-4 px-md-3 ">
              <span
                onClick={() => setSwitchFromPackageToPay(0)}
                className="pointer"
              >
                <MdOutlineArrowBackIosNew size={30} className="text-type1" />
              </span>
            </div>
            <div className="text-center mb-4">
              <Image src={LogoDashBoard} alt="" />
            </div>
            <div className="text-center mb-5">
              <Image src={People} alt="" />
            </div>
            <div className="pb-5">
              <div className="row justify-content-center">
                <div className="col-12 col-lg-10 mb-4 ">
                  <div className="text-center">
                    <h5 className="text-type2 fw-1 mb-3">
                      Your Retainer Plan was successful
                    </h5>
                    <h6 className="fw-1">
                      we are pleased to tell you that order{" "}
                      <span className="text-type1">“Small Busniess” </span>
                      <br /> for a month was successful.
                    </h6>
                    <h6 className="fw-2">
                      you now have access to all services within your plan.
                    </h6>
                  </div>
                </div>

                <div className="col-12 col-lg-7 text-center">
                  <div className="text-center">
                    <Button
                      bntText={"Proceed"}
                      onClick={() => setSwitchFromPackageToPay(0)}
                      className="h5 bg-type1 w-100 py-3 fw-bold rounded text-white"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  };

  const handleRetainerPackageAction = () => {
    //this is where the payment and package are combine
    return (
      <div>
        {" "}
        {switchfromPackageToPay === 0
          ? handleRetainerPackage(retailerServiceData, state)
          : switchfromPackageToPay === 1
          ? handleRetainerPayment(state)
          : switchfromPackageToPay === 3
          ? handleRetainerPaymentSuccessFul(switchfromPackageToPay)
          : null}
      </div>
    );
  };

  //one time form
  const handleForm = (setFormToggle) => {
    return (
      <div className="bg-white py-5  position-relative">
        <div className="back">
          <Image src={BackgroundImage} alt="" />
        </div>

        <div className="container px-5">
          {formToggle === 1 ? (
            <div className="">
              <div className="d-flex align-items-center mb-4 px-md-3 ">
                <span onClick={() => setFormToggle(0)} className="pointer pb-1">
                  <MdOutlineArrowBackIosNew size={30} className="text-type1" />
                </span>
              </div>
              <div className="text-center w-100 mb-4">
                <div className="">
                  <h5 className="fw-1  text-type2 mb-3">
                    Please kindly fill in the form <br /> accurately!
                  </h5>
                </div>
                <div>
                  <span className="ms-5 h5 text-type2">
                    Business information
                  </span>
                </div>
              </div>
              {/* <h6 className="text-type2 mb-3 fw-bolder">
                Business information
              </h6> */}
              <div className="row">
                <div className="col-lg-12">
                  <InputComp2
                    label={"Name of Business"}
                    bordercolor="#EEEEEE"
                    placeholdersize="sm"
                    labelClassName={"text-10"}
                    placeholder="Enter business name"
                    labelSize={"14px"}
                    className="py-2"
                    required
                    value={oneTimeForm?.businessName}
                    name="businessName"
                    onChange={handleOnChangeOneTimeForm}
                  />
                </div>
                <div className="col-lg-6">
                  <InputComp2
                    label={"Business addres"}
                    placeholdersize="sm"
                    labelClassName={"text-10"}
                    placeholder="Enter  address"
                    bordercolor="#EEEEEE"
                    labelSize={"14px"}
                    required
                    value={oneTimeForm?.businessAddress}
                    name="businessAddress"
                    onChange={handleOnChangeOneTimeForm}
                  />
                </div>

                <div className="col-lg-6 ">
                  <InputComp2
                    label={"Designation"}
                    bordercolor="#EEEEEE"
                    placeholdersize="sm"
                    labelClassName={"text-10"}
                    placeholder="CEO, COO, etc…)"
                    labelSize={"14px"}
                    className="py-2"
                    required
                    value={oneTimeForm?.designation}
                    name="designation"
                    onChange={handleOnChangeOneTimeForm}
                  />
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6">
                  <InputComp2
                    label={"What sector does your business fall"}
                    bordercolor="#EEEEEE"
                    placeholdersize="sm"
                    labelClassName={"text-10"}
                    placeholder="Agriculture, etc…)"
                    labelSize={"14px"}
                    className="py-2"
                    required
                    value={oneTimeForm?.businessSector}
                    name="businessSector"
                    onChange={handleOnChangeOneTimeForm}
                  />
                </div>
                <div className="col-lg-6">
                  <SelectComp
                    label={"Year Business was establishment "}
                    required
                    labelClassName={"text-10 h9"}
                    labelSize={"14px"}
                    className="py-2  text-muted"
                    arrayData={Years()}
                    input="name"
                    value={oneTimeForm?.businessEstablishmentYear}
                    name="businessEstablishmentYear"
                    onChange={handleOnChangeOneTimeForm}
                  />
                </div>
              </div>
              <div className="col-lg-12 mb-5">
                <InputComp2
                  label={"A Brief Description of your Business *"}
                  bordercolor="#EEEEEE"
                  placeholdersize="sm"
                  labelClassName={"text-10"}
                  placeholder=""
                  labelSize={"14px"}
                  className="py-2"
                  required
                  value={oneTimeForm?.businessDescription}
                  name="businessDescription"
                  onChange={handleOnChangeOneTimeForm}
                />
              </div>
              <div className="text-center">
                <Button
                  bntText={"Submit"}
                  onClick={handleOneTimeFormSubmit}
                  className="bg-type2 px-5 py-3 h5 fw-bold rounded text-white"
                />
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  };

  useEffect(() => {
    // window.location.href = "/login";
    if (requesting) {
      setTimeout(() => {
        setRequesting(false);
      }, 10000);
    }
  }, [requesting]);

  useEffect(() => {
    getRetainerPackageFunc();
    getRetailSolutionFunc();
    //console.log("GET_RETAINER_PACKAGES_SUCCESS");
  }, [getRetailSolutionFunc, getRetainerPackageFunc]);

  return (
    <LayoutAuth Title={"Home"}>
      
      <div className="mt-4 px-2 ">
        <div className="row mb-4">
          <div className="col-xl-12">
            <div className="ps-3 mb-0 pt-2 bg-white mb-4">
              <div className="row">
                <div className="col-3">
                  <Image src={HomeTopIcon} alt="" />
                </div>
                <div className="col-9 py-3 ">
                  <div className="pe-4">
                    <h5 className="text-black fw-bold">
                      Welcome to Pocketlawyers.{" "}
                    </h5>
                    <p className="h6 text-type2 fw-0 pe-lg-5">
                      Thanks! So, by filling out your Business Information, tell
                      us a little more about yourself so we can better assist
                      you.
                    </p>
                    {!state?.kycInfo && !state?.isLoading && (
                      <div className="text-end">
                        <Button
                          onClick={() => setFormToggle(1)}
                          bntText={"Click Here"}
                          className="bg-8 fw-bold text-type1"
                        />
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
            {/*  */}

            {formToggle && handleForm(setFormToggle)}

            {/* this for Retail Service single Display */}
            {/* {console.log(retailService,'retailService')} */}
            {retailService && (
              <RCard5
                items={retailService}
                chnageRetailService={chnageRetailService}
                setChangeRetailService={setChangeRetailService}
                HandleClose={HandleClose}
                setRequesting={setRequesting}
                setShowLawyer={setShowLawyer}
                fetchLawyerForSpecializedServicefunc={
                  fetchLawyerForSpecializedServicefunc
                }
                state={state}
                setRetailSolutionID={setRetailSolutionID}
              />
            )}
            {/*handleRetainerPackage for retainer details payment options which i have in datautils  */}

            {/* { handleRetainerPackage(retailerServiceData) } */}

            {state?.isLoading && !state?.retailsolutions ? (
              <div>
                <Loader height={"50vh"} />
              </div>
            ) : (
              handleRetailServices(
                retailService,
                setRetailerServiceData,
                setRetailServices,
                retailerServiceData,
                setSwitchFromPackageToPay,
                formToggle,
                //the real retainer package data
                state?.retainer_data,
                getRetainerPackageSubscriptionFunc,
                state?.retailsolutions,
                // state?.client_Sub_Service?.services,
                state?.retailsolutionsubscribtion
              )
            )}
 {state?.newFeedsClient?.length>0 && <div className="mb-5 mb-lg-0">
                  <div className="text-type1 fw-bolder h8 my-3">News Feed</div>
                  <div>
                    {state?.newFeedsClient?.map((item,index)=>
                    <div key={index} className=" g-3">
                    <span>
                      {item?.body}
                    </span>
                  </div>
                    
                    )}
                    
                  </div>
                </div>
                    }
            {handleRetainerPackageAction(retailerServiceData)}
          </div>
         
        </div>
      </div>
      <ModalComp
        size={"md"}
        show={showLawyer && retailSolutionId?.id}
        center
        bodyData={
          <div>
            <div className="mb-3">
              <MdArrowBackIos
                onClick={() => setShowLawyer(false)}
                size={35}
                className="text-type1 pointer"
              />
            </div>
            {state?.lawyerList?.map((item, index) => (
              <div key={index}>
                <GetLawyerProfile
                  IntiateLawyerFunc={IntiateLawyerFunc}
                  LawyerId={item}
                  retailSolutionId={retailSolutionId}
                  state={state}
                />
              </div>
            ))}
          </div>
        }
      />
    </LayoutAuth>
  );
}
