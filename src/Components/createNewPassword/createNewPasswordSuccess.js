import VerficationIcon from "../../assest/Image/passwordchnage.svg";
import Image from "next/image";
import { InputComp2 } from "..";
import Button from "../Ui/Button";
export const CreateNewPasswordSuccess = ({router}) => {
  return (
    <div>
      <div className="py-5 my-4"></div>
      <div className="container d-flex justify-content-center mb-5">
        <div className="col-10 col-lg-8 col-xl-4">
          <div className="text-center mb-3">
            <Image src={VerficationIcon} alt="" />
          </div>
          <div className="text-center mb-3">
            <h5 className="mb-3 text-uppercase">Password updated successfully</h5>
            <div className="text-type2 mb-5 h10">
            Your new password must be different from your
pervious  used password.
            </div>
          </div>
          <div className="">
            <div className="mt-5 mb-3">
              <Button
              onClick={()=>router.push('/login')}
                bntText={"Back to login"}
                className="bg-type1 text-white h5 fw-bold py-3  w-100 px-3 rounded"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
