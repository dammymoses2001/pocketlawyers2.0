/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/no-unescaped-entities */
import Image from "next/image";
import { Card } from "react-bootstrap";
import Slider from "react-slick";
import styled from "styled-components";
import { FaQuoteLeft } from "react-icons/fa";

import Pic from "../../assest/Image/feedbback2.svg";
import Pic1 from "../../assest/Image/feedbback1.svg";
import { Settings } from "../../utils";
import { Carousel3 } from "../Carousel";

const Style = styled.div`
  overflow: hidden;
 
  .slick-slide > div:first-of-type {
    height: 100%;
  }
  // .card {
  //   background: rgba(13, 68, 71, 0.05);
  //   border-radius: 9.13922px;
  //   border: none;
  // }
  img {
    border-radius: 50%;
    border: 5px solid white;
  }
  .imageWrapper {
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
    position: absolute;
    top: 30px;
    z-index: 90;
  }
  // h5 {
  //   font-size: 20px !important;
  // }
  // i {
  //   width: 40px;
  //   height: 40px;
  //   border: 1px solid #ed6710;

  //   border-radius: 50%;
  //   display: flex;
  //   justify-content: center;
  //   align-items: center;
  //   background: #ed6710;
  // }
  // p {
  //   text-align: justify;
  //   color: black;
  //   font-weight: 500;
  // }
  .slick-slide {
    margin-bottom: 61px;
  }

  .slick-list {
    margin-bottom: 61px;
  }
`;

export const ClientFeedBackComp = ({ data = [] }) => {
  return (
    <Style>
      <div className="container position-relative">
      <div className="d-flex justify-content-center mb-5">
          <div className="col-lg-10 text-center text-lg-start">
            <h5 className="text-capitalize">what people are saying about us</h5>
            <h3 className="font-4 text-9">Client Feedback</h3>
          </div>
        </div>
        <div className="position-relative">
        
        <Carousel3 arrayData={data} data-aos="zoom-in-left"/>
        </div>
      </div>
      <div className=" py-5"></div>
    </Style>
  );
};
