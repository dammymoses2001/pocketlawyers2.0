import styled from "styled-components";
import Logo from "../../../assest/Image/loginImage.svg";
import Image from "next/image";
import { InputComp, RadioComp, SelectComp, DrapandDropUpload, MutipleSelect } from "../../Ui";
import Button from "../../Ui/Button";
import Link from "next/link";
import { useState } from "react";
import { OptionData } from "../../../utils";
import { useRouter } from 'next/router'
import toast from "react-hot-toast";


const Style = styled.section`
  padding: 3rem 0 15rem 0;
  .form-width {
    width: 70%;
  }
  .d-inline {
    border-bottom: 2px solid;
  }
  @media (max-width: 992px) {
    .form-width {
      width: 90% !important;
    }
  }
  @media (max-width: 786px) {
    .form-width {
      width: 90% !important;
    }
  }
`;
export const AdditionalInformation = ({bar_cert,FindStateSelected,Years, setSignupLevel,state ,getLawyerSpecialization,userData,handleOnChange,setAreaOfSpec,areaofSpec,cv,setCv,setBar_cert,showError, setShowError}) => {
    const router = useRouter()
  const [level, setLevel] = useState(0);
  const {residential_state,full_time_job,self_employed,CAC_accredition_year} =userData
  // console.log(!residential_state,!full_time_job,!self_employed,!CAC_accredition_year,!bar_cert?.name,!cv?.name,areaofSpec?.length==0)
  const checkFill =() => {
    return !residential_state||!full_time_job||!self_employed||!CAC_accredition_year||!bar_cert?.name||!cv?.name||areaofSpec?.length==0
  }
  return (
    <Style>
      <div className="container  text-black">
        <div className="d-flex justify-content-center">
          <form className="form-width">
            <h5 className=" fw-2 ">Additional Information</h5>

            <div className="mt-5">
              <div>
                <div className="row">
                  <div className=" col-lg-4">
                    <SelectComp
                      label={"State of Residence/Practice?"}
                      required
                      arrayData={state}
                      input="name"
                      name="residential_state"
                      value={userData?.residential_state}
                      onChange={handleOnChange
                        
                      }
                      showError
                    />
                    {/* {console.log(state,userData?.residential_state,FindStateSelected(userData?.residential_state))}  */}
                    
                  </div>
                  <div className=" col-lg-4">
                    <SelectComp
                      label={"Do you have a full time job?"}
                      required
                      arrayData={OptionData}
                      input="name"
                      name="full_time_job"
                      value={userData?.full_time_job}
                      onChange={handleOnChange}
                      showError
                    />
                  </div>
                  <div className=" col-lg-4">
                    <SelectComp
                      label={"Are you self-employed?"}
                      required
                      arrayData={OptionData}
                      input="name"
                      name="self_employed"
                      value={userData?.self_employed}
                      onChange={handleOnChange}
                      showError
                    />
                  </div>
                </div>
              </div>

              <div className="row">
                <div className=" col-lg-7">
                  <MutipleSelect
                  arrayData={getLawyerSpecialization}
                  className={"py-3"}
                    label={
                      <span>
                        {" "}
                        <br /> Areas of Specialization
                      </span>
                    }
                    required
                    setSelected={ setAreaOfSpec }
                    selected={areaofSpec}
                    placeholder="Enter  email address"
                    showError
                    name='
                    Areas of Specialization'
                  />
                </div>
                <div className="col-lg-5">
                  <SelectComp
                    label={
                      "Are you accredited with the Corporate Affairs Commission (CAC)?"
                    }
                    required
                    arrayData={OptionData}
                    input="name"
                    onChange={handleOnChange}
                    name='CAC_accredition_year'
                    value={userData?.CAC_accredition_year }
                    showError
                  />
                </div>
              </div>
            </div>
            <div className=" mb-5">
              <DrapandDropUpload
                fileName={cv}
                label={"Upload your CV "}
                fileUpload={setCv}
                showError
                name="Cv"
              />
            
              
            </div>
            <div className=" mb-5">
              <DrapandDropUpload
                 fileName={bar_cert}
                label={"Upload your call to bar certificates"}
                fileUpload={setBar_cert}
                showError
                name=" call to bar certificates"
              />
            </div>
            <div className=" mb-5">
              <h6 className="text-center text-type2">
                By submitting this form, I hereby certify that, to the best of
                my knowledge,
                <br />
                the provided information is true and accurate.
              </h6>
            </div>
            <div className="text-center px-10">
              <Button
                bntText={"Proceed"}
                className={`w-100 py-3 h4 fw-bolder bg-type1 text-white mb-3 ${checkFill()&& 'opacity-75'}`}
                onClick={(e) => {
                    e.preventDefault();
                    if(checkFill()){
                      setShowError(true)
                      return toast.error('All Field are Mandatory....')
                    }
                    setSignupLevel(3)
                  // router.push('/welcome')
                }}
              />
            </div>
            <div className="text-center px-10">
            <Button  onClick={()=>setSignupLevel(0)}   bntText={'Back'} className='w-100 py-3 h4 fw-bolder bg-8 text-black border mb-3'/>

            </div>
          </form>
        </div>
      </div>
    </Style>
  );
};
