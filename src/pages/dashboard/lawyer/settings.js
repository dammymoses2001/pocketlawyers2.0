/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import { LayoutAuth, LayoutAuthLawyer } from "../../../Layout";
import Image from "next/image";
import {
  CardData,
  FaqData,
  GeneralUrl,
  LawyerCardData,
  LawyerSettingSideBar,
  notificationSettings,
  SettingSideBar,
} from "../../../utils";
import { MdOutlineArrowForwardIos } from "react-icons/md";
import UserAvater from "../../../assest/Image/userImage.svg";
import uploadicon from "../../../assest/Image/addImageIcon.svg";
import Toggle from "../../../assest/Image/toggle.svg";
import ToggleOne from "../../../assest/Image/toggle1.svg";
import styledComponents from "styled-components";
import { Accordiance, InputComp, InputComp2, ResuableImageContainer, TextAreaComp } from "../../../Components";
import Button from "../../../Components/Ui/Button";
import { useAuth } from "../../../hooks/useContext";
import { BsThreeDotsVertical } from "react-icons/bs";
import AddCardIcon from "../../../assest/Image/addCardIcon.svg";
import { FiMinus } from "react-icons/fi";

const Style = styledComponents.div`
.profileImage{
  width: 300px;
    height: 300px;
    overflow: hidden;
    border-radius: 50%;
}
.uploadImage{
  position: absolute;
    bottom: 34px;
    right: 20px;
    z-index: 900;
    width:40%;
}
@media (max-width: 786px) {
  .profileImage{
    width: 200px;
      height: 200px;
      overflow: hidden;
      border-radius: 50%;
  }
  .uploadImage{
    // position: absolute;
    //   bottom: 34px;
    //   right: 20px;
    //   z-index: 900;
      width:20%;
  }
}

`;

const Style2 = styledComponents.div`
h6{
  display:contents !important;
}
`;

export default function Index() {
  const {
    userProfile: { user },UpdateProfileFunc,
    state:{isLoading},UpdateProfilePicFunc,
    PostSupportFunc,state,getOtherProfileFunc,UpdateBankFunc,AddBankFunc
  } = useAuth();
  const initialState = {
    ...user,
  };
  const initialSupport = {
    subject: "",
    message: "",
  };
  const [userData, setUserData] = useState(initialState);
  const [supportData, setSupportData] = useState(initialSupport);
  const [selectMenu, setSelectMenu] = useState(0);
  const [editProfile, setEditProfile] = useState(false);
  const [cardToggle, setCardToggle] = useState(null);
  const [bankDetails,setBankDetails]=useState(null);
  const [saveBankDetails,setSaveBankDetails]=useState(null)
  const [pic,setPic]=useState()
  const [showPic,setShowPic]=useState()
  const hiddenFileInput = React.useRef(null);
  useEffect(() => {
    getOtherProfileFunc(user?.id)
  }, [getOtherProfileFunc]);

  useEffect(() => {
    if(state?.otheruser){
      setBankDetails(state?.otheruser?.lawyerBankAccountDetail)
    }
  }, [state?.otheruser])

  useEffect(() => {
    if(!user?.lawyerBankAccountDetail){
      setCardToggle(1)
    }
  }, [])
  
  

  const handleClick = event => {
    hiddenFileInput.current.click();
  };
  const handleChange = event => {

    const fileUploaded = event.target.files[0];
    setPic(fileUploaded)
    const pic = {
      profile_pic:fileUploaded
    }
    const form = new FormData()

    form.append("profile_pic",fileUploaded)

   
    if(fileUploaded){
      UpdateProfilePicFunc(form)
    }

    setShowPic(URL.createObjectURL(fileUploaded));
  

  };
  const handleUpdateUserProfile = () => {
    //console.log(userData,'UpdateProfileFunc')
    const value = {
      ...userData,
      first_name:userData?.first_name,
      last_name:userData?.last_name,
      profile_pic:pic
    }
    UpdateProfileFunc(value)
    //console.log(value,'value')
  }


  //console.log(user, "handleUserProfile");

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setUserData({ userData, [name]: value });
  };

  const handleOnChangeSupport = (e) => {
    const { name, value } = e.target;
    setSupportData({ ...supportData, [name]: value });
  };
  const handleOnChangeBank = (e) => {
    const { name, value } = e.target;
    setBankDetails({ ...bankDetails, [name]: value });
  };
  const handleOnAddBank = (e) => {
    const { name, value } = e.target;
    AddBankFunc({ ...bankDetails, [name]: value });
  };


  const handleSubmitBankDetails = () =>{
    //  console.log(saveBankDetails)
    const value ={
      "accountNumber":`${bankDetails?.accountNumber}`,
      "accountName":bankDetails?.accountName,
      "bankName":bankDetails?.bankName,
      // accountName:bankDetails?.accountName,
      // bankName:bankDetails?.bankName,
      // accountNumber:bankDetails?.accountNumber
    }
    // console.log(value)
    UpdateBankFunc(value)
  }



  //console.log(user,'state')

  const handlePayment = (cardToggle) => {
    return (
      <div>
        {!cardToggle ? (
          <div
            className="bg-white  py-5 px-3 px-lg-5 d-flex flex-column justify-content-between"
            style={{ height: "100vh" }}
          >
            <div>
              <div className="d-flex justify-content-between align-items-center mb-4">
                <h5 className="fw-bold mb-3">Saved Bank Details</h5>
                <Button
                  bntText={"Change Account"}
                  onClick={() => setCardToggle(1)}
                  className="text-white h6 bg-type1 px-5 rounded py-3 fw-bold"
                />
              </div>

              {LawyerCardData.map((item, index) => (
                <div
                  key={index}
                  className="bg-6 py-4 px-3 d-flex justify-content-between  align-items-center"
                >
                  <div className="d-flex  align-items-center">
                    <div className="me-4">
                      {" "}
                      <Image src={item?.icon} alt="" />
                    </div>
                    <div>
                      <h6 className="fw-2 "> {bankDetails?.accountName}</h6>
                      <h6 className=""> {bankDetails?.bankName}</h6>
                      <h6 className="text-muted"> {bankDetails?.accountNumber}</h6>
                    </div>
                  </div>
                  <div>
                    <BsThreeDotsVertical size={30} />
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <div
            className="bg-white  py-5 px-3 px-lg-5 d-flex flex-column justify-content-between"
            style={{ height: "100vh" }}
          >
            <div>
              <h5 className="fw-bold mb-3">Add card</h5>
              <div>
                <div className="text-center">
                  <Image src={AddCardIcon} alt="" />
                </div>
              </div>
              <div className="d-flex justify-content-center">
                <div className="col-lg-8">
                  <div className="col-12">
                    <InputComp2 placeholder=" Bank Name" 
                    bg={"#F2F2F2"}
                    name='bankName'
                    onChange={handleOnChangeBank}
                    value={bankDetails?.bankName}
                     />
                  </div>
                  <div className="col-12">
                    <InputComp2
                      placeholder="Account Name"
                      bg={"#F2F2F2"}
                      name='accountName'
                      onChange={handleOnChangeBank}
                      value={bankDetails?.accountName}
                    />
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <InputComp2
                        placeholder={"Account Number"}
                        type='number'
                        bg={"#F2F2F2"}
                        name='accountNumber'
                        onChange={handleOnChangeBank}
                        value={bankDetails?.accountNumber}
                      />
                    </div>
                    <div className="py-3 py-lg-5"></div>
                    <div className="text-center">
                      {user?.lawyerBankAccountDetail ?
                    <Button
                    bntText={"Update Card"}
                    onClick={handleSubmitBankDetails}loading={state?.isLoading}
                    className="text-white h5 bg-type1 px-5 rounded py-3 fw-bold"
                  /> :  
                  <Button
                        bntText={"Add Card"}
                        onClick={handleOnAddBank}loading={state?.isLoading}
                        className="text-white h5 bg-type1 px-5 rounded py-3 fw-bold"
                      />
                    }
                      
                      
                    </div>
                    <div className="text-center">
                      <Button
                        bntText={"Back"}
                        onClick={() => setCardToggle(null)}
                        className=" btn text-black h5  px-5 rounded py-3 fw-bold"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  };

  const handleUserProfile = () => {
    return (
      <Style>
        <div className=" ">
        <div className=" text-center mb-5">
          {/* {console.log(userData)} */}
            <div className="profileImage mx-auto position-relative">
            <ResuableImageContainer Width={"100%"} Height={"100%"} picture={GeneralUrl+userData?.pic_name } newlySlectedImage={showPic}/>
              {/* <img
                src={showPic || userData?.pic_name ?GeneralUrl+userData?.pic_name :'https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg'}
                alt=""
              /> */}
               <div className="uploadImage pointer" onClick={handleClick}>
              <Image src={uploadicon} alt="" />
            </div>
            <input
        type="file"
        ref={hiddenFileInput}
        onChange={handleChange}
        style={{display: 'none'}} 
        accept="image/*"
      />
            </div>
           
          </div>
          <div className="container">
            <div className=" ">
              <h5 className="fw-2 mb-5">Account Information</h5>
              {!editProfile ? (
                <div>
                  <div className="row mb-3 mb-lg-5">
                    <div className="col- col-xl-2">
                      <h6 className="fw-bold">Name:</h6>
                    </div>
                    <div className="col">
                      <h6 className="fw-0">
                        {userData?.first_name} {userData?.last_name}
                      </h6>
                    </div>
                  </div>
                  <div className="row mb-3 mb-lg-5">
                  <div className="col-lg-5 col-xl-2">
                      <h6 className="fw-bold">Email:</h6>
                    </div>
                    <div className="col-8">
                      <h6 className="fw-0">{userData?.email}</h6>
                    </div>
                  </div>
                  <div className="row mb-3 mb-lg-5">
                  <div className="col-lg-5 col-xl-2">
                      <h6 className="fw-bold">Phone:</h6>
                    </div>
                    <div className="col-8">
                      <h6 className="fw-0">{userData?.phone}</h6>
                    </div>
                  </div>
                  <div className="row mb-3 mb-lg-5">
                  <div className="col-lg-5 col-xl-2">
                      <h6 className="fw-bold">Address:</h6>
                    </div>
                    <div className="col-8">
                      <h6 className="fw-0">
                        {/* {console.log(userData)} */}
                       {userData?.home_address}
                        <br />
                        <br /> {userData?.city}, {userData?.country?.name}
                      </h6>
                    </div>
                  </div>
                  <div className="p-3 py-lg-5"></div>
                  <div className="text-center">
                    <Button
                      bntText={"Edit Profile"}
                      onClick={() => setEditProfile(true)}
                      className="bg-type1 text-white h4 py-2 px-5"
                    />
                  </div>
                </div>
              ) : (
                <div>
                  <div className="row align-items-center mb-3">
                    <div className="col-md-3 col-lg-3 col-xl-2">
                      <h6 className="fw-bold">Name:</h6>
                    </div>
                    <div className="col">
                      <InputComp2
                        value={userData?.first_name}
                        name={"first_name"}
                        onChange={handleOnChange}
                        bordercolor={"#E0E0E0"}
                        bg="#E0E0E0"
                      />
                    </div>
                  </div>
                  <div className="row align-items-center mb-3">
                    <div className="col-md-3 col-lg-3 col-xl-2">
                      <h6 className="fw-bold">Email:</h6>
                    </div>
                    <div className="col">
                      <InputComp2
                        bordercolor={"#E0E0E0"}
                        bg="#E0E0E0"
                        value={userData?.email}
                        name={"email"}
                        onChange={handleOnChange}
                      />
                    </div>
                  </div>
                  <div className="row align-items-center mb-3">
                    <div className="col-md-3 col-lg-3 col-xl-2">
                      <h6 className="fw-bold">Phone:</h6>
                    </div>
                    <div className="col">
                      <InputComp2
                        bordercolor={"#E0E0E0"}
                        bg="#E0E0E0"
                        value={userData?.phone}
                        name={"phone"}
                        onChange={handleOnChange}
                      />
                    </div>
                  </div>
                  <div className="row align-items-center mb-3">
                    <div className="col-md-3 col-lg-3 col-xl-2">
                      <h6 className="fw-bold">Address:</h6>
                    </div>
                    <div className="col">
                      <InputComp2 
                        bordercolor={"#E0E0E0"}
                        value={userData?.home_address}
                        name={"home_address"}
                        onChange={handleOnChange}
                        bg="#E0E0E0"
                        disabled={true}
                      />
                    </div>
                  </div>
                  <div className="row mb-3 mb-lg-5">
                    <div className="col-md-3 col-lg-3 col-xl-2"></div>
                    <div className="col">
                      <div className="row">
                        <div className="col-md-6 mb-3">
                          <InputComp2 
                          bordercolor={"#E0E0E0"}
                          value={userData?.stateOfOrigin?.name}
                          name={"stateOfOrigin"}
                          onChange={handleOnChange}
                          bg="#E0E0E0"
                          disabled={true}
                          />
                        </div>
                        <div className="col-md-6">
                          <InputComp2 
                          bordercolor={"#E0E0E0"}
                          value={userData?.country?.name}
                          name={"country"}
                          onChange={handleOnChange}
                          bg="#E0E0E0"
                          disabled={true}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row mb-3 mb-lg-5">
                    <div className="col-md-3 col-lg-3 col-xl-2"></div>
                    <div className="col">
                      <div className="row">
                        <div className="text-center">
                          <Button
                            bntText={"Save Profile"}
                            className="bg-type1 text-white h4 py-3 px-5"
                            onClick={handleUpdateUserProfile}
                            loading={isLoading}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </Style>
    );
  };

  const sideBar = () => {
    return (
      <Style2>
        <h5 className="fw-2 mb-5">Setting</h5>
        {LawyerSettingSideBar.map((item, index) => (
          <div key={index}>
            <div
              className="py-3 ps-4 pe-4 rounded bg-white mb-3 d-flex align-items-center justify-content-between pointer"
              onClick={() => setSelectMenu(index)}
            >
              <div className="d-flex align-items-center">
                <div className="me-3">
                  <Image
                    src={selectMenu === index ? item?.icon : item?.icon2}
                    alt=""
                  />
                </div>
                <h5
                  className={`h51 ${
                    selectMenu === index ? "text-7 " : "text-muted"
                  }`}
                >
                  {item?.name}
                </h5>
              </div>
              <div className={selectMenu === index ? "text-7" : "text-muted"}>
                <MdOutlineArrowForwardIos size={20} />
              </div>
            </div>
          </div>
        ))}
      </Style2>
    );
  };

  const handleNotification = () => {
    return (
      <div>
        <div>
          <h5 className="fw-bold">Notification:</h5>
          <hr />
          <div className="mt-5">
            {notificationSettings.map((item, index) => (
              <div
                key={index}
                className="d-flex justify-content-between mb-3 align-item-center"
              >
                <h6 className="fw-0">{item?.name}</h6>
                <div className="pointer">
                  <Image src={item?.status ? ToggleOne : Toggle} alt="" />
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  };
  const handleFAQ = () => {
    return(
      <div className="">
            <h5 className="fw-bold">FAQ:</h5>
          <hr />
      <div></div>
      {FaqData?.map((item,index)=>
       <div key={index} className="font-3">
       <Accordiance
       id={index}
       display={"none"}
       Accordiontitle={<h6 className="d-flex align-items-end fw-2 font-1"><span className="dot me-3"><FiMinus color="#fff"/></span>{item?.title}</h6>}
       AccordionBody={
           <div className="d-flex align-item-center ms-3">
               <div className="me-2 px-1 rounded py-4 my-2 bg-type1"></div>
               <div className="w-100 lh-base fw-3" dangerouslySetInnerHTML={{__html: item?.desc}}>
               </div>
           </div>
       }
       />
       <hr className="my-2"/>
      
      
   


       
   </div>
      )}
     
</div>
    )
  }

  const handleSubmitSupoort = () => {
  
    PostSupportFunc(supportData);
    // "subject":"Subject.",
    // "message": "Message"
  };
  useEffect(() => {
    if (state?.supportMessage) {
      setSupportData(initialSupport);
    }
  }, [initialSupport, state?.supportMessage]);

  const handleSupport = () => {
    return (
      <div>
        <div className="pe-lg-5">
          <h5 className="fw-bold">Support:</h5>
          <hr />
          <div className="mt-5">
            <div className="mb-3">
              <InputComp2
                labelClassName={"text-muted"}
                bordercolor={"#E0E0E0"}
                label={"What is the support about?"}
                name="subject"
                onChange={handleOnChangeSupport}
                value={supportData?.subject}
              />
            </div>
            <div>
              {" "}
              <TextAreaComp
                rows="5"
                label={"Message"}
                labelClassName="text-muted"
                name="message"
                onChange={handleOnChangeSupport}
                value={supportData?.message}
              />
            </div>
            <div className="text-end">
              <Button
                bntText={"Send Message"}
                className="bg-type1 text-white h4 py-3 px-5"
                onClick={handleSubmitSupoort}
                loading={state?.isLoading}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };
  return (
    <LayoutAuthLawyer Title={"Settings"}>
      <div className="mb-4"></div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-xl-4">{sideBar()}</div>
          <div className="col-xl-8">
            <div
              className="bg-white py-5 px-3 px-lg-5"
              style={{ minHeight: "100vh" }}
            >
              {selectMenu === 0 ? (
                handleUserProfile()
              ) : selectMenu === 1 ? (
                handlePayment(cardToggle)
              ) : selectMenu === 2 ? (
                handleNotification()
              ) : selectMenu === 3 ? (
                handleSupport()
              ) : (
                handleFAQ()
              )}
            </div>
          </div>
        </div>
      </div>
    </LayoutAuthLawyer>
  );
}
