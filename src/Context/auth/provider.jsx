import React, {
  createContext,
  useCallback,
  useEffect,
  useReducer,
  useState,
} from "react";
import toast from "react-hot-toast";
import { Loader, Loader2 } from "../../Components";
import { useRouter } from "next/router";
import {
  AUTH_START,
  authReducer,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  GET_CURRENT_USER_SUCCESS,
  GET_SECOND_USER_SUCCESS,
  GET_COUNTRIES_FAIL,
  GET_COUNTRIES_SUCCESS,
  GET_STATE_FAIL,
  GET_STATE_SUCCESS,
  GET_RETAINER_PACKAGES_FAIL,
  GET_RETAINER_PACKAGES_SUCCESS,
  GET_RETAINER_PACKAGES_SUBSCRIPTION_FAIL,
  GET_RETAINER_PACKAGES_SUBSCRIPTION_SUCCESS,
  GET_RETAIL_SOLUTION_SUCCESS,
  UPDATE_USER_PROFILE_FAIL,
  UPDATE_USER_PROFILE_SUCCESS,
  GET_CAMPUS_SUCCESS,

  //message/chat
  GET_ALL_CONVERSATION_SUCCESS,
  GET_ALL_CONVERSATION_FAIL,
  SEND_MESSAGE_FAIL,
  SEND_MESSAGE_SUCCESS,

  //get Lawyer
  GET_LAWYER_SPECIALIZATION_FAIL,
  GET_LAWYER_SPECIALIZATION_SUCCESS,
  GET_CLIENT_SUB_SERVICE_FAIL,
  GET_CLIENT_SUB_SERVICE_SUCCESS,
  UPDATE_USER_PROFILE_PIC_FAIL,
  UPDATE_USER_PROFILE_PIC_SUCCESS,
  GET_LIST_LAWYER_FAIL,
  GET_LIST_LAWYER_SUCCESS,
  INITIATE_LAWYER_FAIL,
  INITIATE_LAWYER_SUCCESS,
  SUBSCRIBE_TO_PACKAGE_PLAN_FAIL,
  SUBSCRIBE_TO_PACKAGE_PLAN_SUCCESS,
  GET_KYC_SUCCESS,
  POST_KYC_SUCCESS,
  POST_SUPPORT_SUCCESS,
  DEFAULT_SUCCESS,
  GET_LAWYER_PAYMENT_SUCCESS,
  GET_USER_RETAIL_SOLUTION_SUBSCRIPTION,
  GET_PRESS_SUCCESS,
  GET_NEWS_FEEDS_SUCCESS,
} from "./reducer";

import {
  // getAccountTypes,
  loginUser,
  getUserProfile,
  getOtherUserProfile,
  RegisterClient,
  getAllCountries,
  getAllState,
  getRetainerPackage,
  getRetainerPackageSubscription,
  updateUserProfile,
  getCampus,
  getLawyerSpecialization,
  getAllConversation,
  sendMessage,
  subscriptionToRetainerPackage,
  getRetailSolution,
  fetchLawyerForSpecializedService,
  getClientSubService,
  updateUserProfilePic,
  initiateConversation,
  subscribePackage,
  kyc,
  support,
  kycpost,
  RegisterLawyer,
  BankDetails,
  updateBankDetails,
  verifyAccount,
  restPassword,
  getLawyerPayment,
  closeMessage,
  AddBankDetails,
  getUserRetainSolutionSubscription,
  lawyerAvailablity,
  subscribeNewLetter,
  contactUs,
  changePassword,
  updateKycPost,
  getPress,
  getNewsFeeds,
  //
} from "../../services";
import { getAuthToken, setAuthToken, setLocationHistory } from "../../utils";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const router = useRouter();
  const [loadingApp, setLoadingApp] = useState(true);
  const [loading, setLoading] = useState(true);
  const [state, dispatch] = useReducer(authReducer, {
    data: null,
    error: null,
    isLoading: false,
    isLoggedIn: false,retainer_data:[],
  });
  const [userProfile, setUserProfile] = useReducer(authReducer, {
    data: null,
    error: null,
    user: [],
    isLoading: false,
    isLoggedIn: false,
  });

  const [getCountries, setGetCountries] = useReducer(authReducer, {
    data: null,
    error: null,
    countries: [],
    isLoading: false,
    isLoggedIn: false,
  });

  const [getState, setGetState] = useReducer(authReducer, {
    data: null,
    error: null,
    state: [],
    isLoading: false,
    isLoggedIn: false,
  });

  const [getRetainerPackageData, setRetainerPackageData] = useReducer(
    authReducer,
    {
      data: null,
      error: null,
      state: [],
      isLoading: false,
      isLoggedIn: false,
    }
  );

  const Login = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await loginUser(values);

      if (data?.user?.account_type === "Client") {
        setAuthToken(data?.token);
        dispatch({
          payload: data?.token,
          type: LOGIN_SUCCESS,
        });
        //console.log(data?.user,'Hwelo')
        await getProfile();
        //console.log('check Client ')
        toast.success(data?.message);
        return router.push("/dashboard/client");
      }

      if (data?.user?.account_type === "Lawyer") {
        //console.log('check LAwyer')
        setAuthToken(data?.token);
        dispatch({
          payload: data?.token,
          type: LOGIN_SUCCESS,
        });
        //console.log(data?.user,'Hwelo')
        await getProfile();

        toast.success(data?.message);
        return router.push("/dashboard/lawyer");
      }
      toast.error("You are not Authorized");
      dispatch({
        payload: "error",
        type: LOGIN_FAIL,
      });

      //   window.location.reload();
    } catch (error) {
      if (error?.message === "Account verification required.") {
        // toast.error(errorMessage || error.response);
        router.push("/verify-page");
      }
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const getProfile = useCallback(async () => {
    setUserProfile({ type: AUTH_START });
    try {
      const data = await getUserProfile();

      //   //console.log(data, "state");
      setUserProfile({ payload: data?.user, type: GET_CURRENT_USER_SUCCESS });

      return data;
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      let message = "";
      if (error?.response?.data?.statusCode === 401) {
        message = "Session Expired or Invalid Token!";
      }
      
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const getClientSubServiceFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getClientSubService();

      //console.log(data, "state");
      dispatch({ payload: data?.data, type: GET_CLIENT_SUB_SERVICE_SUCCESS });

      return data;
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: GET_CLIENT_SUB_SERVICE_FAIL,
      });
      let message = "";
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const getOtherProfileFunc = useCallback(async (id) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getOtherUserProfile(id);

      //console.log(data, "state2");
      dispatch({ payload: data?.user, type: GET_SECOND_USER_SUCCESS });

      return data;
    } catch (error) {
      let message = "";
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const getRetainerPackageFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getRetainerPackage();

      //console.log(data, "hello1");
      dispatch({ payload: data?.data, type: GET_RETAINER_PACKAGES_SUCCESS });

      return data;
    } catch (error) {
      let message = "";
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
      dispatch({
        payload: errorMessage || error.response,
        type: GET_RETAINER_PACKAGES_FAIL,
      });
    }
  }, []);

  const getRetainerPackageSubscriptionFunc = useCallback(async (id) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getRetainerPackageSubscription(id);
      //console.log(getRetainerPackageSubscriptionFunc,'getRetainerPackageSubscriptionFunc')

      dispatch({
        payload: data?.data,
        type: GET_RETAINER_PACKAGES_SUBSCRIPTION_SUCCESS,
      });

      return data;
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: GET_RETAINER_PACKAGES_SUBSCRIPTION_FAIL,
      });
      let message = "";
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
  }, []);

  const getRetailSolutionFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getRetailSolution();
      //console.log(data,'getRetailSolutionFunc')

      dispatch({ payload: data?.data, type: GET_RETAIL_SOLUTION_SUCCESS });

      return data;
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      let message = "";
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
  }, []);

  const IntiateLawyerFunc = async (ids, subjectMatters) => {
    //console.log(ids,subjectMatters, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await initiateConversation(ids, subjectMatters);
      //console.log(data,'IntiateLawyerFunc')
      dispatch({
        payload: data?.message,
        type: INITIATE_LAWYER_SUCCESS,
      });
      toast.success(data?.message);

      router.push("/dashboard/client/message");

      // window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: INITIATE_LAWYER_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const UpdateProfileFunc = async (values) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await updateUserProfile(values);
// console.log(values,'values')
setUserProfile({
        payload: values,
        type: GET_CURRENT_USER_SUCCESS,
      });
      //setUserProfile({ payload: data?.user, type: GET_CURRENT_USER_SUCCESS });
      toast.success(data?.message);

      // window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: UPDATE_USER_PROFILE_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const UpdateProfilePicFunc = async (values) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await updateUserProfilePic(values);

      dispatch({
        payload: data?.message,
        type: UPDATE_USER_PROFILE_PIC_SUCCESS,
      });
      toast.success(data?.message);

      // window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: UPDATE_USER_PROFILE_PIC_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const subscribeRetainerPackageFunc = async (values) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await subscriptionToRetainerPackage(values);
      //console.log(data,'subscribeRetainerPackageFunc')
      dispatch({
        payload: data?.message,
        type: LOGIN_SUCCESS,
      });
      // if(data?.authorizationUrl){
      //   setTimeout(() => {
      //     //authorizationUrl
      //   return  setLocationHistory(data?.authorizationUrl)
      //   }, timeout);
      // }
      toast.success(data?.message);
      if (data?.authorizationUrl) {
        setTimeout(() => {
          window.location.href = data?.authorizationUrl;
        }, 3000);
      }
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const LawyerPaymentFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getLawyerPayment();

      console.log(data, "state");
      dispatch({ payload: data?.data, type: GET_LAWYER_PAYMENT_SUCCESS });

      return data;
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      let message = "";
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const CloseMessageFunc = async (conveId) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await closeMessage(conveId);

      //  dispatch({
      //   payload: data?.message,
      //   type: UPDATE_USER_PROFILE_PIC_SUCCESS
      // })
      toast.success(data?.message);

      window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: UPDATE_USER_PROFILE_PIC_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  //----------------------------------------------------Message Conversation--------------------------------------------

  const getallConversationFunc = useCallback(async (userid) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getAllConversation(userid);

      //   //console.log(data, "state");
      dispatch({ payload: data, type: GET_ALL_CONVERSATION_SUCCESS });

      return data;
    } catch (error) {
      let message = "";
      dispatch({
        payload: errorMessage || error.response,
        type: GET_ALL_CONVERSATION_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const sendMessageFunc = async (values) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await sendMessage(values);

      dispatch({
        payload: data?.message,
        type: SEND_MESSAGE_SUCCESS,
      });

      // data?.conversationId
      const newMessage = state?.conversations.find(
        (item, index) => item?.id === data?.conversationId
      );
      const newMessageposition = state?.conversations.find(
        (item, index) => item?.id === data?.conversationId
      );
      const hello = newMessage?.messages.push(data);

      const newArray = state?.conversations.filter(
        (item) => item?.id !== data?.conversationId
      );
      const addArray = [...newArray, newMessage];
      const sortaddArray = addArray.sort((a, b) => {
        return a.id - b.id;
      });

      //          const  original = [1,2,4,4]
      // const el = 3
      // const replaceAt = 2
      // const newArray = [...original.slice(0,replaceAt ), el, ...arr.slice(replaceAt + 1)]
      dispatch({ payload: sortaddArray, type: GET_ALL_CONVERSATION_SUCCESS });
      //   //console.log(hello,'mediaPath1')
      //  //console.log(addArray,state?.conversations,sortaddArray,'mediaPath2')
      //  //console.log(newMessage,addArray,'mediaPath3')
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: SEND_MESSAGE_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const logout = async () => {
    try {
      localStorage.clear();
      sessionStorage.clear();
      setUserProfile("");
      
      router.push("/login");
      // setInterval(() => {

      // }, 3000);

      // window.location.reload();
    } catch (error) {}
  };

  const RegisterClientComp = async (values) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await RegisterClient(values);

      //  setAuthToken(data?.token);
      dispatch({
        payload: data?.token,
        type: REGISTER_SUCCESS,
      });
      //  await getProfile()
      router.push("/welcome");

      toast.success(data?.message);
      //   window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: REGISTER_FAIL,
      });

      const errorMessage = error?.response?.data?.message || error.message;
      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const RegisterLawyerComp = async (values) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await RegisterLawyer(values);
      //console.log(data,'RegisterLawyerComp')
      //  setAuthToken(data?.token);
      dispatch({
        payload: data?.token,
        type: REGISTER_SUCCESS,
      });
      //  await getProfile()
      router.push("/welcome");

      toast.success(data?.message);
      //   window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: REGISTER_FAIL,
      });

      const errorMessage = error?.response?.data?.message || error.message;
      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
    dispatch({
      payload: "",
      type: REGISTER_FAIL,
    });
  };

  const getCountriesFunc = useCallback(async () => {
    setGetCountries({ type: AUTH_START });
    try {
      const data = await getAllCountries();

      //   //console.log(data, "state");
      setGetCountries({ payload: data?.data, type: GET_COUNTRIES_SUCCESS });

      return data;
    } catch (error) {
      let message = "";
      setGetCountries({
        payload: errorMessage || error.response,
        type: GET_COUNTRIES_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const getCampusFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getCampus();

      //   //console.log(data, "state");
      dispatch({ payload: data?.data, type: GET_CAMPUS_SUCCESS });

      return data;
    } catch (error) {
      let message = "";
      //   setGetCountries({
      //     payload: errorMessage || error.response,
      //     type: GET_COUNTRIES_FAIL
      // });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const getlawyerSpecalizationFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getLawyerSpecialization();

      //   //console.log(data, "state");
      dispatch({
        payload: data?.data,
        type: GET_LAWYER_SPECIALIZATION_SUCCESS,
      });

      return data;
    } catch (error) {
      let message = "";
      //   setGetCountries({
      //     payload: errorMessage || error.response,
      //     type: GET_COUNTRIES_FAIL
      // });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  //fetchLawyerForSpecializedServicefunc
  const fetchLawyerForSpecializedServicefunc = async (id) => {
    // //console.log(values, "values");
    dispatch({ type: AUTH_START });
    try {
      const data = await fetchLawyerForSpecializedService(id);

      //  setAuthToken(data?.token);
      dispatch({
        payload: data?.data,
        type: GET_LIST_LAWYER_SUCCESS,
      });
      //  await getProfile()
      //  router.push("/welcome");

      toast.success(data?.message);
      //   window.location.reload();
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: GET_LIST_LAWYER_FAIL,
      });

      const errorMessage = error?.response?.data?.message || error.message;
      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const getStateFunc = useCallback(async () => {
    setGetState({ type: AUTH_START });
    try {
      const data = await getAllState();

      //console.log(data?.data, "state");

      setGetState({ payload: data?.data, type: GET_STATE_SUCCESS });

      return data;
    } catch (error) {
      let message = "";
      setGetState({
        payload: errorMessage || error.response,
        type: GET_STATE_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const getKYCFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await kyc();

      console.log(data, "KYC");

      dispatch({ payload: data, type: GET_KYC_SUCCESS });
    } catch (error) {
      let message = "";
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);
  const GetPress = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getPress();

      console.log(data, "KYC");

      dispatch({ payload: data?.data, type: GET_PRESS_SUCCESS });
    } catch (error) {
      let message = "";
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  const PostKYCFunc = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await kycpost(values);
      
      dispatch({
        payload: data?.message,
        type: POST_KYC_SUCCESS,
      });
     
      toast.success(data?.message);

      setTimeout(() => {
        dispatch({
          payload: "",
          type: POST_KYC_SUCCESS,
        });
        // window.location.reload();
      }, 1000);
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };
  const UpdateKycPost = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await updateKycPost(values);
      const userProfile1 ={
        ...userProfile?.user,business:values
      }
      // console.log(userProfile,userProfile1,'PostKYCFunc')
      
      setUserProfile({
        payload:userProfile1,
        type: GET_CURRENT_USER_SUCCESS,
      });
      dispatch({
        payload: "",
        type: LOGIN_FAIL,
      });
      toast.success(data?.message);

    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  //Suport

  const PostSupportFunc = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await support(values);
      //console.log(data,'PostSupportFunc')
      dispatch({
        payload: data?.message,
        type: POST_SUPPORT_SUCCESS,
      });
      toast.success(data?.message);

      setTimeout(() => {
        dispatch({
          payload: "",
          type: POST_SUPPORT_SUCCESS,
        });
      }, 1000);
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  //Bank

  const UpdateBankFunc = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await updateBankDetails(values);
      //console.log(data,'PostSupportFunc')
      //   dispatch({
      //    payload: data?.message,
      //    type: POST_SUPPORT_SUCCESS
      //  })
      toast.success(data?.message);

      setTimeout(() => {
        router.push("/dashboard/lawyer/settings");
      }, 2000);

      setTimeout(() => {
        dispatch({
          payload: "",
          type: POST_SUPPORT_SUCCESS,
        });
      }, 1000);
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const AddBankFunc = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await AddBankDetails(values);
      //console.log(data,'PostSupportFunc')
      //   dispatch({
      //    payload: data?.message,
      //    type: POST_SUPPORT_SUCCESS
      //  })
      toast.success(data?.message);

      // setTimeout(() => {
      //   router.push("/dashboard/lawyer/settings");
      // }, 2000);

      setTimeout(() => {
        dispatch({
          payload: "",
          type: POST_SUPPORT_SUCCESS,
        });
        window.location.reload();
      }, 1000);
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };

  const VerifyAccountFunc = async (email, code) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await verifyAccount(email, code);
      //console.log(data,'PostSupportFunc')
      dispatch({
        payload: data?.message,
        type: DEFAULT_SUCCESS,
      });
      toast.success("Account Verified");

      setTimeout(() => {
        router.push("/login");
      }, 2000);

      // setTimeout(() => {
      //   dispatch({
      //     payload: "",
      //     type: POST_SUPPORT_SUCCESS
      //   })
      // }, 1000);
    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };
  const RequestPassordReset = async (value) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await restPassword(value);
      //console.log(data,'PostSupportFunc')
      dispatch({
        payload: data?.message,
        type: DEFAULT_SUCCESS,
      });
      toast.success("Check your email....");
      router.push("/resetPassword");

    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      toast.error(errorMessage || error.response);
    }
  };

  const ChangePassword = async (value) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await changePassword(value);
      //console.log(data,'PostSupportFunc')
      dispatch({
        payload: data?.message||"done",
        type: DEFAULT_SUCCESS,
      });
      toast.success(data?.message);
      // router.push("/resetPassword");

    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      toast.error(errorMessage || error.response);
    }
  };

  const LawyerAvailablityFunc = async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await lawyerAvailablity();
      //  console.log(data,'LawyerAvailablityFunc')
      dispatch({
        payload: data?.message,
        type: DEFAULT_SUCCESS,
      });
      toast.success(data?.message);

    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      ////console.log(errorMessage, error.response, "error");
      toast.error(errorMessage || error.response);
    }
  };
  const SubscribeNewLetter = async (email) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await subscribeNewLetter(email);
      //  console.log(data,'LawyerAvailablityFunc')
      dispatch({
        payload: data?.message,
        type: DEFAULT_SUCCESS,
      });
      if(data?.message ==="email taken!"){
        return toast.error(data?.message)
      }
      toast.success("You have subscribe successfully....");

    } catch (error) {
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      const errorMessage = error?.response?.data?.message || error.message;

      toast.error(errorMessage || error.response);
    }
  };
  const ContactUs = async (values) => {
    dispatch({ type: AUTH_START });
    try {
      const data = await contactUs(values);
      //  console.log(data,'LawyerAvailablityFunc')
      dispatch({
        payload: data?.message||"done",
        type: DEFAULT_SUCCESS,
      });
      setTimeout(() => {
        dispatch({
          payload: "",
          type: DEFAULT_SUCCESS,
        });
      }, 2000);
      if(data?.message ==="email taken!"){
        return toast.error(data?.message)
      }
      
      toast.success("Thanks  for the submission...");

    } catch (error) {
      
      const errorMessage = error?.response?.data?.message || error.message;
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      toast.error(errorMessage || error.response);
    }
  };


  const getUserRetainSolutionSubscriptionFunc = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getUserRetainSolutionSubscription();


      dispatch({
        payload: data?.data,
        type: GET_USER_RETAIL_SOLUTION_SUBSCRIPTION,
      });
    } catch (error) {
      let message = "";
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);


  const GetNewsFeeds = useCallback(async () => {
    dispatch({ type: AUTH_START });
    try {
      const data = await getNewsFeeds();


      dispatch({
        payload: data?.data,
        type: GET_NEWS_FEEDS_SUCCESS,
      });
    } catch (error) {
      let message = "";
      dispatch({
        payload: errorMessage || error.response,
        type: LOGIN_FAIL,
      });
      if (error?.response?.data?.statusCode === 401) {
       
      }
      const errorMessage =
        message || error?.response?.data?.message || error.message;
    }
    setLoading(false);
  }, []);

  useEffect(() => {
    const loadApp = async () => {
      setLoadingApp(true);
      if (getAuthToken()) {
        await getProfile();

        // await getRetainerPackageFunc()
      }
      setLoadingApp(false);
    };

    loadApp();
  }, [getProfile, getRetainerPackageFunc]);

  // if (loadingApp) {
  //     return <Loader2 fullscreen />;
  //     return <div></div>
  // }

  return (
    <AuthContext.Provider
      value={{
        name: "NAAN",
        Login,
        state,
        userProfile,
        isAuthenticated: userProfile?.user?.id,
        getState,
        dispatch,
        logout,
        getProfile,
        RegisterClientComp,
        RegisterLawyerComp,
        getCountriesFunc,
        getStateFunc,
        getRetainerPackageFunc,
        UpdateProfileFunc,
        getRetainerPackageSubscriptionFunc,
        getCampusFunc,
        getlawyerSpecalizationFunc,
        getCountries,
        getOtherProfileFunc,
        getallConversationFunc,
        sendMessageFunc,
        subscribeRetainerPackageFunc,
        getRetailSolutionFunc,
        UpdateProfilePicFunc,
        fetchLawyerForSpecializedServicefunc,
        getClientSubServiceFunc,
        IntiateLawyerFunc,
        getKYCFunc,
        PostKYCFunc,
        UpdateKycPost,
        PostSupportFunc,
        UpdateBankFunc,
        VerifyAccountFunc,
        RequestPassordReset,
        ChangePassword,
        LawyerPaymentFunc,
        CloseMessageFunc,
        AddBankFunc,
        LawyerAvailablityFunc,
        getUserRetainSolutionSubscriptionFunc,
        SubscribeNewLetter,
        ContactUs,
        GetPress,
        GetNewsFeeds
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
