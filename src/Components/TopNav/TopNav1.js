/* eslint-disable react/display-name */
/* eslint-disable @next/next/no-img-element */
import { RiSearch2Line, RiNotificationLine } from "react-icons/ri";
import { RiMenu2Fill, RiCloseFill } from "react-icons/ri";
import { FiSearch } from "react-icons/fi";
import { useAuth } from "../../hooks/useContext";
import Avatar from "../../assest/Image/avatar.jpg";
import { Dropdown } from "react-bootstrap";
import { DropdDownComp, ResuableImageContainer } from "../Ui";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { GeneralUrl } from "../../utils";
import toast from "react-hot-toast";

export const TopNav1 = ({setToogleSideBar}) => {
  const {
    userProfile: { user },
    logout,
    state,
    getRetainerPackageFunc,
  } = useAuth();
  const [retainerPackageid, setRetainerPackageId] = useState();
  const router = useRouter();
  // console.log(state, "userProfile");

  const [showSearch, setShowSearch] = useState(false);
  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </a>
  ));

  useEffect(() => {
    getRetainerPackageFunc();
  }, [getRetainerPackageFunc]);

  const handleFindPacakge = () => {
    const result = state?.retainer_data?.find(
      (item) => item?.name === retainerPackageid
    );
    //console.log(result,'retainerPackageid1')
    if(!result?.id){
    return  toast.error('Not found, Reach out to the Admin...')
    }
    
  router.push(`/dashboard/client/subcription/${result?.id}`);
  };
  return (
    <div className="animate__animated animate__fadeIn position-relative" style={{zIndex:'10'}}>
      <div className=" py-3  px-3 px-md-4">
        <div className="row justify-content-between align-items-center">
          <div className="col-md-6 col-lg-5 order-2 order-md-0">
            <div className="d-none d-md-flex border d-flex align-items-center rounded bg-secondary-muted px-3">
              <input
                className="py-2 w-100 bg-7 py-3 px-4"
                placeholder="Search for Packages"
                list="retailsolution"
                onChange={(e) => setRetainerPackageId(e.target.value)}
                value={retainerPackageid}
              />
              <datalist id="retailsolution">
                {state?.retainer_data?.map((item, index) => (
                  <>
                  <option key={index} id={item?.id} value={item?.name}>
                    {item?.name}
                  </option>
                 
                  </>
                ))}
                {state?.retainer_data?.length===0 && <option value="Not Found"/>} 
              </datalist>

              <RiSearch2Line
                size={25}
                className={"pointer"}
                onClick={handleFindPacakge}
              />
            </div>
          </div>
          <div className="col-md-6 col-lg-5 col-xl-3">
            {!showSearch ? (
              <div className="animate__animated animate__fadeIn  d-flex align-items-center justify-content-between justify-content-md-end">
                <div className="d-inline d-md-none">
                  <RiMenu2Fill onClick={()=>setToogleSideBar(true)} size={30} className="me-2 pointer" />
                  <FiSearch
                    size={30}
                    className="pointer"
                    onClick={() => setShowSearch(true)}
                  />
                </div>

                <div className="d-flex align-items-center ">
                  <div className="me-2 me-md-4 pointer">
                    <RiNotificationLine size={25} />
                  </div>
                  <div className="d-flex align-items-center ">
                    
                    <Dropdown>
                      <Dropdown.Toggle
                        className=" btn-default bg-7"
                        id="dropdown-basic"
                      >

                        <div className="imageWrapper ">
                        <ResuableImageContainer Width={"100%"} Height={"100%"} picture={GeneralUrl+user?.pic_name } />

                         
                        </div>
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item>
                          Welcome, {user?.first_name}
                        </Dropdown.Item>
                        <Dropdown.Item onClick={logout}>Log out</Dropdown.Item>
                        {/* <Dropdown.Item href="#/action-3">Something else</Dropdown.Item> */}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  <div className="d-block d-md-none">
                    {/* <DropdDownComp/> */}
                  </div>
                </div>
              </div>
            ) : (
              <div className="d-flex align-items-center border py-1 px-2 rounded">
                <input
                 list="retailsolution"
                  className="bg-7 py-2 w-100"
                  placeholder="Search for solutions"
                  onChange={(e) => setRetainerPackageId(e.target.value)}
                  value={retainerPackageid}
                />
                <datalist id="retailsolution">
                {state?.retainer_data?.map((item, index) => (
                  <>
                  <option key={index} id={item?.id} value={item?.name}>
                    {item?.name}
                  </option>
                 
                  </>
                ))}
                {state?.retainer_data?.length===0 && <option value="Not Found"/>} 
              </datalist>
                <RiCloseFill
                  size={35}
                  className="me-3 pointer"
                  onClick={() => setShowSearch(false)}
                />{" "}
                <FiSearch
                  size={30}
                  className=""
                  onClick={handleFindPacakge}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
