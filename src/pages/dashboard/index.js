import React, { useEffect } from 'react'
import { useAuth } from '../../hooks/useContext'
import { useRouter } from "next/router";
import { Loader } from '../../Components';

export default function Index() {
    const {userProfile} =useAuth()
    const router = useRouter()
    // console.log(userProfile)
    useEffect(() => {
      if(userProfile?.user?.account_type ==="Client"){
        router.push("/dashboard/client");
      }
      else if(userProfile?.user?.account_type ==="Lawyer"){
        router.push("/dashboard/lawyer");
      }
      else{
        router.push("/login");
      }
      
    }, [userProfile?.user?.account_type])
    
  return (
    <div><Loader /></div>
  )
}
