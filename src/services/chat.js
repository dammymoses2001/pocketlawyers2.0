import { request } from "../utils";

export const getAllConversation = async (value) => {
    try {
        const res = await request.get(`/conversations/${value}`);
         //console.log(res?.data,value, "getAllConversation");
        return res?.data;
    } catch (error) {
        const err = error?.response?.data?.message || error?.message;
        throw new Error(err);
    }
  };

  export const sendMessage= async (value) => {
    try {
        const res = await request.post(`/messages` ,value);
         //console.log(res?.data,value, "getAllConversation");
        return res?.data;
    } catch (error) {
        const err = error?.response?.data?.message || error?.message;
        throw new Error(err);
    }
  };

  
  export const closeMessage= async (convID) => {
    try {
        const res = await request.post(`/conversations/closeConversation/${convID}`);
         //console.log(res?.data,value, "getAllConversation");
        return res?.data;
    } catch (error) {
        const err = error?.response?.data?.message || error?.message;
        throw new Error(err);
    }
  };


// export const getAllConversation = async (value) => {

//     try {
//         const res = await request.get(`/conversations/${value}`);
//          //console.log(res?.data,value, "getAllConversation");
//         return res?.data;
//     } catch (error) {
//         const err = error?.response?.data?.message || error?.message;
//         throw new Error(err);
//     }
//   };