// contexts/auth.js
// append this new bit a the end:
import { useRouter } from "next/router";
import Router from "next/router";
import { Loader } from "../Components";
//import { useAuth } from "../hooks/useContext";
import { useEffect, useState } from "react";
import { useAuth } from "./useContext";
import { getAuthToken } from "../utils";
import { getUserProfile } from "../services";
export const ProtectRoute = ({ children }) => {
  const router = useRouter();
  const { isAuthenticated, userProfile } = useAuth();
  const [getdata,setGetData]=useState(false)


  useEffect(() => {
   setGetData(true)
   const storage = getAuthToken()
   if(storage === null) 
  //  router.push("/login")
   getUserProfile()
   .then((res) => {
     setGetData(true)
    //  console.log(res,'getdatagetdatagetdata')
       !res?.data?.id ? router.push("/login") : null 
   })
   .catch(err => {router.push("/login")})
  
 },[router])


  if (!isAuthenticated) {
    return <Loader />;
  }
  return children;
};
