import { useEffect, useState } from "react";
import { LayoutAuth } from "../../../../Layout";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  CardData,
  formatMoney,
  retailPackage1,
  selectedPlan,
  subcriptionPlan,
  subscriptionSideBar,
} from "../../../../utils";
import Image from "next/image";
import { MdOutlineArrowForwardIos } from "react-icons/md";
import { BsThreeDotsVertical } from "react-icons/bs";
import Button from "../../../../Components/Ui/Button";
import AddCardIcon from "../../../../assest/Image/addCardIcon.svg";
import MoneyIcon from "../../../../assest/Image/moneyicon.svg";
import MoneyIcon1 from "../../../../assest/Image/moneyicon1.svg";
import {
  CheckBoxx,
  InputComp,
  InputComp2,
  RadioComp2,
  RadioComp4,
  RadioComp5,
  RetailPackage,
} from "../../../../Components";
import { useAuth } from "../../../../hooks/useContext";

export default function Index() {
  const [selectMenu, setSelectMenu] = useState(0);
  const [cardToggle, setCardToggle] = useState(0);
  const {
    state,
    subscribeRetainerPackageFunc,
    getClientSubServiceFunc,
    getRetainerPackageFunc,
    getRetainerPackageSubscriptionFunc,
  } = useAuth();
  const [subscriptionToggle, setSubscriptionToggle] = useState(0);
  const [plan, setPlan] = useState([]);
  const [planPrice,setPlanPrice]=useState()
  const { query } = useRouter();
  const { id } = query;

  // console.log(state, "statesub");
  useEffect(() => {
    //console.log(id,'stateid')
    if (id == 1) {
      setSubscriptionToggle(3);
    }
  }, [id]);

  useEffect(() => {
    getClientSubServiceFunc();
    getRetainerPackageFunc();
  }, [getClientSubServiceFunc, getRetainerPackageFunc]);

  useEffect(() => {
    if (id) {
      getRetainerPackageSubscriptionFunc(id);
    }
  }, [id]);

  const sideBar = (index, item) => {
    return (
      <div key={index}>
        <div
          className="py-3 px-3 rounded bg-white mb-3 d-flex align-items-center justify-content-between pointer"
          onClick={() => setSelectMenu(index)}
        >
          <div className="d-flex align-items-cente">
            <div className="me-3">
              <Image
                src={selectMenu === index ? item?.icon : item?.icon1}
                alt=""
              />
            </div>
            <h6 className={`${selectMenu === index ? "text-type1" : ""}`}>
              {item?.name}
            </h6>
          </div>
          <div className={selectMenu === index ? "text-type1" : ""}>
            <MdOutlineArrowForwardIos size={20} />
          </div>
        </div>
      </div>
    );
  };

  const showCurrentSubscriptionside = (index, item, state) => {
    return (
      <div className=" py-5" key={index}>
        <div>
          <h5 className="mb-2 fw-2 px-3 px-lg-5">
            <span className="me-3">Subcription Plan:</span>{" "}
            <span className="fw-0">{state?.client_Sub_Service?.name}</span>
          </h5>{" "}
          <hr />
        </div>
        <div>
          <h5 className="py-3 fw-1 px-3 px-lg-5">
            <span className="">Status:</span>{" "}
            <span className="fw-0">{item?.status}</span>
          </h5>{" "}
          <hr />
        </div>
        <div className="mb-3">
          <h5 className="px-3 px-lg-5 mb-5">Summary</h5>
          <div className="col-lg-10 px-3 px-lg-5">
            <table className="table table table-bordered">
              <tbody className="">
                {item?.Summary?.map((item, index) => (
                  <tr key={index}>
                    <td className="text-start h7">{item?.name}</td>
                    <td className=" h7">{item?.assets}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <div className="mb-5">
          <h6 className="px-3 px-lg-5 fw-2 mb-3">Legal Services (per month)</h6>
          <div className="col-lg-10 px-3 px-lg-5">
            <table className="table table table-bordered">
              <tbody className="">
                {item?.legalService?.map((item, index) => (
                  <tr key={index}>
                    <td className="text-start h7">{item?.legal}</td>
                    <td colSpan={2} className=" h7">
                      {item?.no ? item?.no : "/"}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <div className="px-5">
          <Button
            className={"py-3 px-5 h5 fw-bolder bg-type1 text-white"}
            bntText={"Change Plan"}
            onClick={() => setSubscriptionToggle(1)}
          />
        </div>
      </div>
    );
  };

  const ListPlan = subcriptionPlan?.find((item, index) => item?.name == plan);
  const getSub = state?.retainer_data?.find(
    (item, index) => item?.id === Number(id)
  );
  //  console.log(getSub,state?.retainer_data,'sub')
  return (
    <LayoutAuth Title={"Subcription"}>
      <div className="mb-3"></div>
      <div className="container-fluid">
        <div className="row">
          {/* <div className="col-lg-3">
            <h5 className="fw-2 mb-5">Subcription</h5>
            {subscriptionSideBar.map((item, index) => (
              sideBar(index,item)
            ))}
          </div> */}
          <div className="col-lg-12">
            <div className="bg-white row">
              <div className="col-lg-3">
                <h5 className="mt-5 ms-3 fw-bold">Subcription Plan</h5>
              </div>
              <div className="col-lg-9">
                <div className="pt-7 pb-5 d-flex justify-content-end font-1 px-3">
                  <div className="col-12 col-lg-10 ">
                    <div>
                      <div className="text-center text-md-start">
                        <h5 className="fw-bold">{getSub?.name}</h5>
                        <p>Select subcription period</p>
                      </div>
                      <div className="d-flex justify-content-center justify-content-lg-start">
                        {getSub?.retainerPackagePlans?.length > 0 ? (
                          <table className="">
                            <tbody>
                              {getSub?.retainerPackagePlans?.map(
                                (item, index) => (
                                  <tr key={index}>
                                    <td className="px-3 px-lg-5 py-1 bg-6 text-end fw-2">
                                      <h6>{item?.plan_type} </h6>
                                    </td>
                                    <td className="ps-3 py-1 ">
                                      {" "}
                                      <RadioComp5
                                        className={"fw-2"}
                                        value={item?.price}
                                        onChange={(e)=>
                                        {
                                          setPlanPrice({
                                            price:e.target.value,
                                            id:item?.id
                                          })
                                        }
                                        }
                                        label={
                                          <span>
                                            ₦ {formatMoney(item?.price)}
                                          </span>
                                        }
                                        labelClassName="text-end fw-2"
                                      />
                                    </td>
                                  </tr>
                                )
                              )}
                              <tr>
                                <td className=""></td>
                              {planPrice?.price &&
                               <td className="ps-3  pt-3 ">
                               <h6 className="fw-0">
                                 Bill:
                                 <span className="text-type1 fw-2">
                                   ₦{" "}
                                   {formatMoney(
                                     planPrice?.price
                                   )}
                                 </span>
                               </h6>
                             </td>
                              } 
                              
                              </tr>
                            </tbody>
                          </table>
                        ) : (
                          <div className="text-center text-danger">
                            No Retainer Package Added Yet
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="d-flex justify-content-start">
                  <div className="bg-white container position-relative col-12 col-md-8">
                    <div className="d-flex align-items-center mb-4 px-md-3 ">
                    
                      <div className="w-100 text-center">
                        <h5 className="text-type2 fw-1 mb-0 mt-5">
                          Select Payment method
                        </h5>
                      </div>
                    </div>

                    <div className="pb-5  position-relative">
                      <div className="row justify-content-start">
                        <div className="col-12  mb-4 ">
                          <div>
                            <div className="d-flex align-items-center rounded w-100 bg-type3 py-3 px-3">
                              <span className="me-2">
                                {" "}
                                <Image src={MoneyIcon} alt="" />
                              </span>
                              <div className="w-100">
                                {" "}
                                <RadioComp2
                                  className={"h5 fw-2"}
                                  label={"Bank Transfer"}
                                  id="payment"
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-12  mb-7 ">
                          <div>
                            <div className="d-flex align-items-center rounded w-100 bg-type3 p-3">
                              <span className="me-2">
                                {" "}
                                <Image src={MoneyIcon1} alt="" />
                              </span>
                              <div className="w-100">
                                {" "}
                                <RadioComp2
                                  className={"h5 fw-2"}
                                  label={"Credit/Debit Card"}
                                  id="payment"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-12  text-center">
                          <div className="text-center">
                            
                            <Button
                              loading={state?.isLoading}
                              bntText={"Proceed"}
                              disabled={!planPrice?.id}
                              onClick={() => {
                                //  //console.log(retailerServiceData, "retailerServiceData");
                                subscribeRetainerPackageFunc(planPrice?.id);
                                //  subscribeRetainerPackageFunc(retailerServiceData?.id);
                                //setSwitchFromPackageToPay(3)
                              }}
                              className={`h5 ${planPrice?.id ?'opacity-100':'opacity-75'}  bg-type1 w-100 py-3 fw-bold rounded text-white`}
                            />
                            <Link href={"/dashboard/client/subcription"}>
                              <a className="h5 bg-7 w-100 py-3 fw-bold rounded text-black">
                                Cancel
                              </a>
                            </Link>
                            {/* <Button
               
                   bntText={"Cancel"}
                   onClick={() => setSubscriptionToggle(getSub?.id)}
                   className="h5 bg-7 w-100 py-3 fw-bold rounded text-black"
                 /> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </LayoutAuth>
  );
}
