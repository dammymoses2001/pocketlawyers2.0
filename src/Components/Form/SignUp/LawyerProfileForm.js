

import styled from "styled-components"
import Logo from '../../../assest/Image/loginImage.svg'
import Image from 'next/image'
import { InputComp,RadioComp,SelectComp } from "../../Ui"
import Button from "../../Ui/Button"
import Link from 'next/link'
import { useState } from "react"
import { TitleData } from "../../../utils"
import {useRouter} from 'next/router'
import toast from "react-hot-toast"



const Style = styled.section`
padding:3rem 0 15rem 0;
.form-width{
    width:70%;
}
.d-inline{
    border-bottom:2px solid;
    
}
@media (max-width: 992px) {
    .form-width{
        width:90% !important;
    }
}
@media (max-width: 786px) {
    .form-width{
        width:90% !important;
    }
}
`
export const LawyerProfileForm =({setSignupLevel,state,userData,handleOnChange,FindStateSelected,showError,setShowError})=> {
    const router =useRouter()
    const {title,first_name,last_name,email,phone_number,dob,gender,home_address,city,stat} =userData;

    const isFilled =() =>{
        return !title||!first_name||!last_name||!email||!dob||!gender||!phone_number||!home_address||!city||!state
    }

    const [level,setLevel]=useState(0)
  return (
    <Style>
        <div className="container  text-black">
   
            <div className="d-flex justify-content-center">
                <form className="form-width">
                <h5 className=" fw-2 ">Personal Information</h5>
                   
                   <div className="mt-5">
                       <div>
                       <div className="row">
                           <div className="col-5 col-lg-2">
                           <SelectComp label={'Title'} arrayData={TitleData} onChange={handleOnChange}    value={userData?.title} name='title'  input='name' showError={showError}/>
                           </div>
                           <div className="col-12 col-lg-5">
                               <InputComp label={'First Name'} placeholder='First Name' className={"py-3"} onChange={handleOnChange} name='first_name' value={userData?.first_name} showError={showError}/>
                           </div>
                           <div className="col-lg-5">
                           <InputComp label={'Last Name'} placeholder='Last Name' className={"py-3"} onChange={handleOnChange} name='last_name' value={userData?.last_name} showError={showError}/>
                           </div>
                       </div>
                       </div>
                       <div>
                       <div className="row">
                           
                           <div className=" col-lg-6">
                               <InputComp label={'Email Address'} className={"py-3"} placeholder='Enter  email address' onChange={handleOnChange} name='email' value={userData?.email} showError={showError}/>
                           </div>
                           <div className="col-lg-6">
                           <InputComp label={'Phone Number'} typeComp='number' className={"py-3"} placeholder='e.g +234 123 456 7890' onChange={handleOnChange} name='phone_number' value={userData?.phone_number} showError={showError}/>
                           </div>
                       </div>

                       <div className="row">
                           
                           <div className=" col-lg-6">
                          
                              <div className="pe-lg-5">
                                   <InputComp label={'Date of Birth'}  typeComp='date' className={"text-uppercase py-3 "}  onChange={handleOnChange} name='dob' value={userData?.dob} showError={showError}/></div>
                           </div>
                           <div className="col-lg-6">
                           <label className='form-label  text-capitalize mb-4 fw-2'>Gender</label>
                        <div className="row">
                        <div className="col-6 col-lg-5">
                        <RadioComp label={"Male"} id="gender"  name='gender'
                        value={"M"}  onChange={handleOnChange}
                        checked={userData?.gender === 'M'}
                        />
                      </div>

                      {/* <input className="form-check-input" type="radio" value={"hello"} name="gender" id="flexRadioDefault1" onChange={(e)=>//console.log(e.target.value)}/> */}
                      <div className="col-6 col-lg-5">
                        <RadioComp label={"Female"} id="gender" name='gender'
                        value={"F"} onChange={handleOnChange}  checked={userData?.gender === 'F'}/>
                      </div>
                        </div>
                        {!gender && showError &&<small className='text-danger text-capitalize fw-2'>{`${"Gender"} is required....` }</small>} 

                           </div>
                       </div>
                       <div className="row">
                           
                           <div className=" col-lg-12">
                               <InputComp label={'Permanent Home Address'} className={"py-3"} placeholder=' address'
                                name='home_address'
                                value={userData?.home_address} onChange={handleOnChange}
                                showError={showError}
                               />
                           </div>
                          
                       </div>
                       <div className="row mb-5">
                           
                           <div className=" col-lg-6">
                               <InputComp label={'City'} className={"py-3"} placeholder='City'  name='city'
                         value={userData?.city} onChange={handleOnChange} showError={showError}
                         />
                           </div>
                           <div className="col-lg-6">
                           <SelectComp arrayData={state} input='name' label={'State'}className={"py-3"}  placeholder='State' 
                             name='state_of_origin'
                             value={(userData?.state_of_origin)} onChange={handleOnChange}
                             showError={showError}
                           />
                           {/* {console.log(FindStateSelected(userData?.state_of_origin),userData?.state_of_origin,'userData?.state_of_origin')} */}
                           </div>
                       </div>
                       </div>
                   </div>
                    <div className="text-center px-10">
                        <Button bntText={'Proceed'} className={`w-100 py-3 h4 fw-bolder bg-type1 text-white mb-3 ${isFilled()&&'opacity-75'}`} onClick={(e)=>{
                            e.preventDefault()
                            if(isFilled()){
                                setShowError(true)
                                return toast.error('All Field are Mandatory.....')
                            }
                            setSignupLevel(1)
                            window.scroll(0,0)
                        }}/>
                      
                    </div>
                    <div className="text-center px-10">
            <Button  onClick={(e)=>{
                e.preventDefault();
                router.push('/signup')
            }}   bntText={'Go back'} className='w-100 py-3 h4 fw-bolder bg-8 text-black border mb-3'/>

            </div>
                </form>
            </div>
        </div>
    </Style>
  )
}
