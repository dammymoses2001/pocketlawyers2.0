import styled from 'styled-components'
import DownloadImage from '../../assest/Image/downlaod.svg'
import Image from 'next/image'
import { AppleButton, GoogleButton } from '../Ui'

const Style = styled.section``
export  const DownloadComp =()=> {
  return (
    <Style>
        <div  className='container'>
            <div>
                <h4 className='text-center fw-bolder text-type2 mb-5'>Download App</h4>
                <div className='text-center mb-5'>
                    <Image src={DownloadImage} alt=''/>
                </div>
                <div className="d-flex align-items-center justify-content-center ">
              <GoogleButton className={""} />
              <AppleButton />
            </div>
            </div>
        </div>
    </Style>
  )
}
