import React from 'react'
import { RetailServices } from '../../Components'
import { LayoutNoAuth } from '../../Layout'
import { retailserviceData2 } from '../../utils'

export default function index() {
  return (
    <LayoutNoAuth Title={"Retail Solutions"}>
        <RetailServices retailserviceData2={retailserviceData2}/>
    </LayoutNoAuth>
  )
}
