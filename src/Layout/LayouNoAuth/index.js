import { NavBar,Footer } from "../../Components"
import Head from 'next/head'

export  const LayoutNoAuth =({children,footer,Title}) =>{
  return (
      <>
      <Head>
        <title>PocketLawyers | {Title}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/icon.svg" />
      </Head>
      <NavBar/>
      <div style={{background:'#fff',minHeight:'100vh'}}>{children}</div>
     {!footer &&  <Footer/>}
      </>
   
  )
}
