import React from 'react'

export const TextAreaComp=({label,className,labelClassName,required,...props})=> {
  return (
    <div>
        <div className="mb-5">
            {label &&<label  className={`form-label h6 text-captalize fw-2 ${labelClassName}`}>{label} {required && <span className="text-danger">*</span>}</label> }
  <textarea  className={`form-control w-100 py-3 ${className}`} {...props}></textarea>
</div>
    </div>
  )
}
