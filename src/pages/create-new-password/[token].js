/* eslint-disable react/no-unescaped-entities */
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import  { useState } from 'react'
import toast from 'react-hot-toast'
import { CreateNewPasswordSuccess,CreateNewPasswordForm } from '../../Components'
import { useAuth } from '../../hooks/useContext'
import { LayoutNoAuth } from '../../Layout'


export default function Index() {
    const [toggle,setToogle] =useState(0)
    const router = useRouter()
    const {ChangePassword,state:{defaultMessage,isLoading}} =useAuth()
    const {token} =router.query;
    const intialState ={
        new_password:"",
        confirm_new_password:"",
        email:""
    }
   
    const [userData,setUserData] =useState(intialState)

    const handleOnChange = (e) => {
        const { name, value } = e.target;
        setUserData({...userData,[name]:value})
      }; 
    console.log(router?.query)

    const handleSubmit= ()=>{
        const {confirm_new_password,new_password,email} =userData
        if(!confirm_new_password || !new_password){
            return toast.error('All field are mandatory....')
        }
        if(confirm_new_password !== new_password){
            return toast.error('Password Must be the same....')
        }
        const value ={
          repeat_password:confirm_new_password,
            new_password:new_password,
            email:email,
            token:token
        }
        ChangePassword(value)
    }
    useEffect(() => {
      if(defaultMessage){
        setToogle(1)
      }
    }, [defaultMessage])
    
    
  return (
    <LayoutNoAuth Title={"Create New Password"} footer>
        <div className='pb-8 mt-5'>
            {toggle==0 ?
            <CreateNewPasswordForm handleOnChange={handleOnChange} handleSubmit={handleSubmit} isLoading={isLoading}/>:
<CreateNewPasswordSuccess setToogle={setToogle} router={router}/>
}

        </div>
    </LayoutNoAuth>
  )
}
