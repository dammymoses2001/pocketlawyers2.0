import styled from "styled-components";

import { InputComp, RadioComp, SelectComp, MutipleSelect, InputCompTag } from "../../Ui";
import Button from "../../Ui/Button";

import { useState } from "react";
import { OptionData } from "../../../utils";
import toast from "react-hot-toast";

const Style = styled.section`
  padding: 3rem 0 15rem 0;
  .form-width {
    width: 70%;
  }
  .d-inline {
    border-bottom: 2px solid;
  }
  @media (max-width: 992px) {
    .form-width {
      width: 90% !important;
    }
  }
  @media (max-width: 786px) {
    .form-width {
      width: 90% !important;
    }
  }
`;
export const LawyerEducationalForm = ({
  setSignupLevel,
  Years,
  HowManyYears,
  campuses,
  getLawyerSpecialization,
  getCountriesData,
  userData,
  handleOnChange,
  setPraticeCountries,
  practiceCountries,
  setProfessionalBody,
  professionalBody,
  otherdegree,
  setOthersDegree,
  showError,setShowError
}) => {
  const [level, setLevel] = useState(0);
  const {learning_institution,grad_year,year_of_bar,years_of_practice,law_school_campus} =userData
// console.log(learning_institution,grad_year,year_of_bar,years_of_practice,practiceCountries?.length,otherdegree?.length,professionalBody?.length)

  const checkFill = () => {
   return  !learning_institution||!grad_year||grad_year ==='Select'||!law_school_campus||law_school_campus==="Select"||!year_of_bar||year_of_bar==='Select'||!years_of_practice||professionalBody.length===0
  }
  return (
    <Style>
      <div className="container  text-black">
        <div className="d-flex justify-content-center">
          <form className="form-width">
            <h5 className=" fw-2 ">Educational Information</h5>

            <div className="mt-5">
              <div>
                <div className="row">
                  <div className="col-12 col-lg-8">
                    <InputComp
                      label={"University attended"}
                      placeholder="Enter University name"
                      className={"py-3"}
                      required
                      name="learning_institution"
                      value={userData?.learning_institution}
                      onChange={handleOnChange}
                      showError
                    />
                  </div>
                  <div className="col-lg-4">
                    <SelectComp
                      label={"Year of Graduation"}
                      arrayData={Years()}
                      input="name"
                      required
                      name="grad_year"
                      value={userData?.grad_year}
                      onChange={handleOnChange}
                      showError
                    />
                  </div>
                </div>
              </div>
              <div>
                <div className="row">
                  <div className="col-12 col-lg-8">
                    <SelectComp
                      label={"Nigeria Law School Campus attended"}
                      arrayData={campuses}
                      input="name"
                      required
                      name="law_school_campus"
                      onChange={handleOnChange}
                      value={law_school_campus}
                      showError
                    />
                  </div>
                  <div className="col-lg-4">
                    <SelectComp
                      label={"Year of Call"}
                      arrayData={Years()}
                      input="name"
                      required
                      name="year_of_bar"
                      onChange={handleOnChange}
                      value={userData?.year_of_bar}
                      showError
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-lg-12">
                    {/* <InputComp
                      label={"What professional bodies do you belong to?"}
                      className={"py-3"}
                      name="professional_bodies"
                      value={userData?.professional_bodies}
                      onChange={handleOnChange}
                      required
                    /> */}
                    <InputCompTag  
                      label={"What professional bodies do you belong to?"}
                      setProfessionalBody={setProfessionalBody}
                      professionalBody={professionalBody}
                      name='professional bodies'
                      showError
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-8">
                    <div>
                      <label className="form-label  text-capitalize mb-2 fw-2 w-75">
                        Do you have any other degrees other than Law? If YES,
                        Kindly state <span className="text-danger">*</span>
                      </label>
                      <div className="row">
                        <div className="col-4">
                          <SelectComp
                            arrayData={OptionData}
                            input="name"
                            name="other_degree"
                            disabled={true}
                            value={otherdegree?.length>0 ?'Yes':'No'}
                            onChange={handleOnChange}
                          />
                        </div>
                     
                        <div className="col-8">
                          <InputCompTag
                            className={"py-3"}
                            name="degrees"
                            setProfessionalBody={setOthersDegree}
                            professionalBody={otherdegree}
                            
                      
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <SelectComp
                      label={
                        "How many years have you been in active Legal practice? *"
                      }
                      arrayData={HowManyYears()}
                      input="name"
                      required
                      name="years_of_practice"
                      value={userData?.years_of_practice}
                      onChange={handleOnChange}
                      showError
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12">
                    <div>
                      <label className="form-label  text-capitalize mb-2 fw-2 ">
                        Are you qualified to practice Law outside Nigeria, If
                        YES, Kindly indicate what Country{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <div className="row">
                        <div className="col-4">
                          <SelectComp
                            arrayData={OptionData}
                            input="name"
                            name="areyou"
                            value={practiceCountries?.length>0?'Yes':'No'}
                            onChange={handleOnChange}
                            disabled={true}
                            
                          />
                          {/* {console.log(practiceCountries?.length,'practiceCountries')} */}
                        </div>
                        <div className="col-8">
                          <MutipleSelect
                            arrayData={getCountriesData}
                            className={"py-3"}
                            setSelected={setPraticeCountries}
                            selected={practiceCountries}
                            
                            name='Country/Countries'
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="text-center px-10 mt-5">
              <Button
                bntText={"Proceed"}
                className={`w-100 py-3 h4 fw-bolder bg-type1 text-white mb-3 ${checkFill() && 'opacity-75'}`}
                onClick={(e) => {
                  e.preventDefault()
                  if(checkFill()){
                    setShowError(true)
                    return toast.error('All Fields are Mandatory...')
                  }
                  setSignupLevel(2)}}
              />
            </div>
            <div className="text-center px-10">
            <Button  onClick={(e)=>{
             
              e.preventDefault();
              setSignupLevel(0)
            }}   bntText={'Go back'} className='w-100 py-3 h4 fw-bolder bg-8 text-black border mb-3'/>

            </div>
          </form>
        </div>
      </div>
    </Style>
  );
};
