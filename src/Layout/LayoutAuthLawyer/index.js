import styled from "styled-components";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { ProtectRoute } from "../../hooks/ProtectedRoute";
import {Providers} from "../../Context/providers";
import { useAuth } from "../../hooks/useContext";
import { LayoutAuthLogin } from "../LayoutAuthLogin";
import { LawyerSideBar,Loader,TopNav1 } from "../../Components";
import toast from "react-hot-toast";
import Head from 'next/head'

//import { userDetailsContext } from "../../context";


export const LayoutAuthLawyer = ({ children ,Title}) => {
  const router = useRouter();
  const [toogleSideBar,setToogleSideBar] =useState(false)
  const {userProfile,state,getProfile,logout} =useAuth()

//  console.log('i Was here...',userProfile)
 useEffect(() => {
  if (userProfile?.user?.id&&userProfile?.user?.account_type !== 'Lawyer') {
    toast.error('You are not authorized')
    logout()
  }
}, [logout, userProfile?.user?.account_type, userProfile?.user?.id]);


 if(!userProfile?.user?.account_type==='Lawyer'){
  return <><Loader/></>
}
 

  


    return (
      
        <ProtectRoute>
        <Head>
        <title>PocketLawyers | {Title}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/icon.svg" />
      </Head>
          <Style sideBar={toogleSideBar}>
            <div className="d-flex">
              <div className="sidebar   pt-4 ">
                {/* <SideBar /> */}
              <div className="mx-auto ">
              <LawyerSideBar setToogleSideBar={setToogleSideBar}/>
              </div>
              </div>
              <main className="main px-2 px-lg-3">
              <TopNav1 setToogleSideBar={setToogleSideBar}/>
                {/* <Navbar /> */}
                {children}
              </main>
            </div>
          </Style>
          </ProtectRoute>
      
    );
  // }
  // else{
  //   return <><Loader/></>
  // }
 
};

const Style = styled.div`
  background: #fbfafc;
  section {
    position: relative;
    min-height: 100vh;
  }
.top{
  z-index:10;
}
  .sidebar {
    /* border: 1px solid; */
    background: #fff;
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    width: 251px;
    z-index: 11;
    // z-index:9000;
  }
  .main {
    width: 100%;
    /* border: 1px solid; */
    margin-left: 251px;
    background: #f2f2f2;
    overflow: hidden;
  }
  input,
  select {
    box-shadow: none !important;
    outline: none !important;
  }
  main {
    min-height: 100vh;
  }

  @media (max-width: 765px) {
    .main {
     
      margin-left: 0;
     
    }
    .sidebar {
      display:${props=>props?.sideBar?'':'none'};
    }
  }
  .imageWrapper{
    border-radius: 50%;
    width: 40px;
    height: 40px;
    overflow: hidden;
  }
  .imageWrapper img{
    width:100%;
    height:100%;
  }
`;