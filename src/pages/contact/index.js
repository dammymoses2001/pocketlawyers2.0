import React from 'react'
import { ContactForm } from '../../Components'
import { LayoutNoAuth } from '../../Layout'

export default function index() {
  
  return (
    <LayoutNoAuth Title={"Contact"}>
        <ContactForm/>
    </LayoutNoAuth>
  )
}
