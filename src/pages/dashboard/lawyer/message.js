/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useRef, useState } from "react";
import styledComponents from "styled-components";
import { LayoutAuth, LayoutAuthLawyer } from "../../../Layout";
import Image from "next/image";
import { GoPrimitiveDot } from "react-icons/go";
import { FiSend } from "react-icons/fi";
import { MdAttachFile } from "react-icons/md";
import { GeneralUrl, MessageData, request } from "../../../utils";
import { io } from "socket.io-client";
import { useAuth } from "../../../hooks/useContext";
import { getOtherUserProfile } from "../../../services";
import {
  ListUsers,
  ListUsersLawyer,
  Messager,
  MessagerLawyer,
} from "../../../Components";
import { useQuery } from "react-query";
//import { QueryClient, QueryClientProvider, useQuery } from 'react-query'

const Style = styledComponents.div`
.imageWrapperMessage{
  border-radius: 50%;
  width: 50px;
  height: 50px;
  overflow: hidden;
}
.h-1{
  min-height:100vh;
}
textarea{
  border:none !IMPORTANT;
  outline:none;
}
`;

export default function Index() {
  const [message, setMessage] = useState();
  const [convsersation, setConversation] = useState();
  const [sendMessage, setSendMessage] = useState();
  const [getSelectedMessages, setGetSelectedMessages] = useState([]);
  const [arrivalMessage, setArrivalMessage] = useState();
  const [activeUser, setActiveUser] = useState();

  //
  const [pic, setPic] = useState();
  const [showPic, setShowPic] = useState();


  const {
    getOtherProfileFunc,
    userProfile,
    getallConversationFunc,
    state,
    state: { otheruser },
    CloseMessageFunc,
    sendMessageFunc,
  } = useAuth();
  const [otherUserId, setOtherUserId] = useState(14);
  const scrollRef = useRef();
  const socket = useRef();
  const scrollTo = () =>
    scrollRef?.current?.scrollIntoView({ behavior: "smooth" });
  //const socket = useRef(io("https://pocketlawyers.herokuapp.com/"))

  const startSocket = () => {
    socket.current = io(GeneralUrl);
    // socket.on("getMessage", (data) => {
   
    // })
    socket.current.on("getMessage", (data) => {
       //console.log(data, "socketTop");
      scrollTo();
      setArrivalMessage({
        conversationId: data?.conversationId,
        sender: data?.senderId,
        text: data?.message,
        media:data?.media,
        createdAt: Date.now(),
      });

      // scrollTo();
    });
  };

  useEffect(() => {
    startSocket();
  }, []);

  //console.log(message?.conversation, arrivalMessage, "socket");

  useEffect(() => {
    //console.log(arrivalMessage, "socket12");
    // message?.senderId?.includes(arrivalMessage?.sender)&&
    message?.conversation?.messages[0]?.conversationId ===
      arrivalMessage?.conversationId &&
      arrivalMessage &&
      setGetSelectedMessages([...getSelectedMessages, arrivalMessage]);
  }, [arrivalMessage]);

 // console.log(arrivalMessage,'arrivalMessage')
  useEffect(() => {
    socket.current.on("hello", (msg) => {
      //console.log(msg, "sockettest");
    });
  }, [socket]);

  //console.log(message, "socket");

  useEffect(() => {
    socket.current.emit("addUser", userProfile?.user?.id);
    socket.current.on("getOnlineUsers", (users) => {
      setActiveUser(users?.users);
      // console.log(users, "sockettest");
    });
  }, [userProfile?.user?.id]);

  //console.log(message, "socket");



  useEffect(() => {
    // window.location.href = "/login";
    // clean up controller
    let isSubscribed = true;
    if (userProfile?.user?.id) {
      const GetallConversation = async () => {
        // //console.log('state' ,'find')
        getallConversationFunc(userProfile?.user?.id);
      };
      GetallConversation();
    }

    // cancel subscription to useEffect
    return () => (isSubscribed = false);
  }, [getallConversationFunc, userProfile?.user?.id]);
  //https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg

  //console.log(message, "dispatch334");

  useEffect(() => {
    if (state?.isLoading) {
      setSendMessage("");
      scrollTo();
      // window.scroll(0,10000)
    }
  }, [state?.isLoading]);

  const getConversation =async () => {
    try {
      const res = await request.get(`/messages/${message?.conversation?.id}`);
      //console.log(res?.data, "socket1");
      setGetSelectedMessages(res?.data);
      //  setGetSelectedMessages([...getSelectedMessages,res?.data])
      //  scrollTo();
      //  setSendMessage("")
      return res?.data;
    } catch (error) {
      const err = error?.response?.data?.message || error?.message;
      // throw new Error(err);
    }
  }
  const CheckCat=()=>{
    scrollTo();
  }

  //getCurrentMessage
  const { data,isLoading,isError,error,isFetching } = useQuery("getSingleProduct",getConversation,{
    // enabled:router?.isReady,
     onSuccess:CheckCat,
    
    // onError: (error) =>
    //     toast.error(`Oops, something went wrong : ${error.message}`),
  })
 // console.log(data,'helloss')

  useEffect(() => {
    const getCurrentMessage = async () => {
      try {
        const res = await request.get(`/messages/${message?.conversation?.id}`);
        //console.log(res?.data, "socket1");
        setGetSelectedMessages(res?.data);
        //  setGetSelectedMessages([...getSelectedMessages,res?.data])
         scrollTo();
        //  setSendMessage("")
        return res?.data;
      } catch (error) {
        const err = error?.response?.data?.message || error?.message;
        // throw new Error(err);
      }
    };
    getCurrentMessage();
  }, [message?.conversation?.id]);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if(message?.conversation?.id &&
       message?.conversation?.id && 
       message?.conversation?.senderId ){

    const value = {
      conversationId: message?.conversation?.id,
      text: sendMessage,
      mediaFile: "",
    };

    const Form = new FormData();
    Form.append('conversationId',message?.conversation?.id)
    Form.append('text',sendMessage)
    
    Form.append('mediaFile',showPic)
    const checkout = {

      conversationId: message?.conversation?.id,
      receiverId : userProfile?.user?.id,
      senderId: message?.conversation?.senderId,
      message: sendMessage,
      media: pic,
    }

    //console.log(checkout,message?.conversation,userProfile?.user?.id)


     socket.current.emit("sendMessage", {
      conversationId: message?.conversation?.id,
      senderId: userProfile?.user?.id,
      receiverId: message?.conversation?.senderId,
      message: sendMessage,
      media: pic,
    });

    const getOtherUser = async () => {
      try {
        const res = await request.post(`/messages`, Form);
        //console.log(res?.data, "socket1");
        setPic(null);
        setShowPic(null)
        setGetSelectedMessages([...getSelectedMessages, res?.data]);
        scrollTo();
        setSendMessage("");
        return res?.data;
      } catch (error) {
        const err = error?.response?.data?.message || error?.message;
        // window.location.reload()
        // throw new Error(err);
      }
    };
    getOtherUser();
  }
    // startSocket()
  };

  const hiddenFileInput = React.useRef(null);
  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    const reader = new FileReader();
    setShowPic(fileUploaded)
    reader?.readAsDataURL(fileUploaded)
    reader.onload =function(){
      setPic(reader?.result);
      console.log(reader?.result)
    }
    //console.log(fileUploaded,'picture')
   

    const form = new FormData();

    form.append("profile_pic", fileUploaded);

    // if (fileUploaded) {
    //   UpdateProfilePicFunc(form);
    // }

    // setShowPic(URL.createObjectURL(fileUploaded));
  };

  const handleCloseMessage = (data) =>{
    // console.log(data)
    CloseMessageFunc(data?.id)
  }
  useEffect(() => {
    //  scrollRef?.current.scrollIntoView({behavior:'smooth'})
    scrollTo();
  }, [message]);

  return (
    <LayoutAuthLawyer Title={"Message"}>
      <Style className="row">
     
     {state?.conversations?.length>0 &&<div className="col-lg-3">
          <div>
            <h5 className="fw-2 mb-3"> Message </h5>
            <div className="messagebox1 mt-5">
              
              {state?.conversations?.map((conversation, index) => (
               
                  <ListUsersLawyer
                  key={index}
                  state={state}
                  message={MessageData}
                  setMessage={setMessage}
                  setSendMessage={setSendMessage}
                  conversation={conversation}
                  userProfile={userProfile}
                  otheruser={otheruser}
                  getOtherUserProfile={getOtherProfileFunc}
                  setGetSelectedMessages={setGetSelectedMessages}
                  activeUser={activeUser}
                  getSelectedMessages={getSelectedMessages}
                  arrivalMessage={arrivalMessage}
                />
           
              ))}
            </div>
          </div>
        </div>}
        <div className="col-lg-9 mb-5 mb-md-0 position-relative">
          {/* {console.log(message?.conversation?.closed,'message?.conversation')} */}
          {/* {state?.conversations && message?.conversation  ? ( */}
        {state?.conversations && message?.conversation  ? (
            <div className="bg-white container pb-5 h-100">
              <div className="row h-100">
                <div className="col-md-12 border-end h-100">
                  <div className=" border-bottom d-flex align-items-center py-4 justify-content-between  topHeader">
                  <div className=" d-flex align-items-center">
                    <div className="imageWrapperMessage me-3">
                   
                      <img
                        src={
                          message?.user?.pic_name
                          ? (GeneralUrl+message?.user?.pic_name)
                            : "https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg"
                        }
                        alt=""
                      />
                    </div>
                    <h5 className="me-3 mb-0">{message?.user?.first_name}</h5>
                    {activeUser?.map((item, index) =>
                      item?.userId === message?.user?.id ? (
                        <div className="d-flex align-items-center" key={index}>
                          <GoPrimitiveDot color="green" size={20} />
                          <h6 className="mb-0">Active</h6>
                        </div>
                      ) : (
                        <div className="d-flex align-items-center">
                          <h6 className="mb-0">Offline</h6>
                          {/* <GoPrimitiveDot color="green" size={20} />
                    <h6 className="mb-0">Active</h6> */}
                        </div>
                      )
                    )}
                    <h6 className="mb-0 ps-3">
                      {message?.conversation?.serviceName}
                    </h6>
                  </div>
                  <div>{message?.conversation?.closed ? <span className="text-danger fw-bolder">Chat Closed</span>:<span onClick={()=>handleCloseMessage(message?.conversation)} className="me-4 text-decoration-underline pointer">End Service</span>}</div>
                  </div>

                  <div className="hello">
                    <div className="pt-8 pb-5 messagebox">
                      {getSelectedMessages?.map((item, index) => (
                        <div className="" key={index} ref={scrollRef}>
                          
                          <Messager
                            item={item}
                            userProfile={userProfile}
                            message={message}
                          />
                        </div>
                      ))}
                    </div>
                    <div className="h9 text-6">{(pic?"File Added":"")}</div>

                    <div className="d-flex align-items-center w-100 bg-10 mt-3 px-2">
                      <form
                        onSubmit={handleOnSubmit}
                        className="d-flex align-items-center w-100 bg-10 mt-3 py-0 px-2"
                      >
                        <div className="w-100 bg-10 pb-3 d-flex">
                        <span>
                        {message?.conversation?.closed?
                          <MdAttachFile className="text-muted "  size={35} />
                          :
                          <MdAttachFile className="text-muted pointer" onClick={handleClick} size={35} />
                        }
                          <input
                type="file"
                ref={hiddenFileInput}
                onChange={handleChange}
                style={{ display: "none" }}
                accept="image/*,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.pdf"

               
              />
              {/* {console.log(message?.conversation?.closed)} */}
                        </span>
                        <input
                          className="w-100 bg-10"
                          value={sendMessage}
                          onChange={(e) => setSendMessage(e.target.value)}
                          disabled={message?.conversation?.closed?true:false}
                        />
                      </div>
                      <div>
                        {message?.conversation?.closed?
                        <FiSend
                        className="text-muted "
                        size={30}
                        // onClick={}
                      />:
                      <FiSend
                          className="text-muted pointer"
                          size={30}
                          onClick={handleOnSubmit}
                        />}
                        
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div>
              {!state?.conversation?  <div className="text-center">
              <span className="noConversation2 ">
                No Conversation <br /> Currently
              </span>
            </div> :
             <span className="noConversation">
             Open a Conversation to <br /> start a chat
           </span>}
             
            </div>
          )}
        </div>
      </Style>
    </LayoutAuthLawyer>
  );
}
