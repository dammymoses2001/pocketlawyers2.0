import { Form } from "react-bootstrap";
import styledComponents from "styled-components";

const Style2 = styledComponents.div`
.form-check .form-check-input {
  float: right !important;
  margin-left: -1.5em;
  border: 2px solid #218698 !important;
}

`;

const Style3 = styledComponents.div`
label{
  font-size:${(props) =>
    props.Size === "sm"
      ? "16px"
      : (props) =>
          props.Size === "md"
            ? "20px"
            : (props) => (props.Size === "lg" ? "24px" : props.Size)};
}

@media (max-width: 540px) {
  label{
    font-size:15px;
  }
}

.form-check .form-check-input {
  float: right !important;
  margin-left: -1.5em;
  border: 2px solid ${(props) =>
    props.brColor ? props.brColor : "#218698 "} !important;
}
.form-check{
  display: flex !important;
  justify-content: space-between;
  align-items: center;
  flex-direction: row-reverse;
}
.form-check-input:checked {
  background-color: #ed6710 !important;
  border-color: #ed6710 !important;
}

}
`;

export const RadioComp2 = ({
  label,
  id,
  className,
  brColor,
  size,
  ...props
}) => {
  return (
    <Style3 className="" Size={size} brColor={brColor}>
      <div className={`fw-2   radio2 ${className}`}>
        <Form.Check
          type="radio"
          name={id}
          label={label}
          // checked={label=== props.value}
          {...props}
        />
      </div>
    </Style3>
  );
};

const Style = styledComponents.div`
label{
    font-size: 20px;
    font-weight: 500;
}
.form-check-input:checked {
  background-color: #ed6710 !important;
  border-color: #ed6710 !important;
}

@media (max-width: 540px) {
    label{
        font-size: 18px;
        font-weight: 500;
    }

}
`;

export const RadioComp = ({ label, id, ...props }) => {
  return (
    <Style className="mb-3 mb-lg-5">
      <div className="fw-2 h5">
        <Form.Check
          type="radio"
          name={id}
          label={label}
          // checked={label=== props.value}
          {...props}
        />
      </div>
    </Style>
  );
};

export const CheckBoxx = ({ label, labelClassName, hrnull, ...props }) => {
  return (
    <Style2>
      <div className="form-check">
        <label className={`form-check-label`}>
          <h6 className={`fw-bold text-type2  ${labelClassName} `}>{label}</h6>
        </label>
        <input
          className="form-check-input"
          type="checkbox"
          id="flexCheckChecked"
          {...props}
        />
      </div>
      {!hrnull && <hr />}
    </Style2>
  );
};

export const RadioComp4 = ({
  label,
  labelClassName,
  className,
  id,
  ...props
}) => {
  return (
    <Style className={`mb-3 mb-lg-5 ${labelClassName}`}>
      <div className="fw-2 h5">
        <Form.Check
          type="radio"
          name={id}
          label={label}
          checked={label === props.value}
          {...props}
        />
      </div>
    </Style>
  );
};
export const RadioComp5 = ({
  label,
  labelClassName,
  className,
  id,
  ...props
}) => {
  return (
    <Style className={` ${labelClassName} d-flex w-100 justify-content-end`}>
      <h6 className="me-2">{label}</h6>
      <div className="form-check">
        <input
          className={`form-check-input className`}
          type="radio"
          name="flexRadioDefault"
          id="flexRadioDefault1"
          // checked={true}
          {...props}
        />
      </div>
    </Style>
  );
};

const StyleSwitch = styledComponents.div`
.form-switch .form-check-input {
  width: 3em;
}
.form-check-input:checked {
  background-color: #ED6710 !important;
  border-color: #ED6710 !important;
}
.form-check-input {
  
  height: 19px;
}
.form-check-input {
  background-color: #ed671052 !important;
  border-color: #ed671052 !important;
}

`;
export const SwitchButton = ({ label, className, checked, id, ...props }) => {
  return (
    <StyleSwitch className="">
      <Form>
        <Form.Check
          type="switch"
          id="custom-switch"
          label={label}
          checked={checked}
          onChange={(e) => console.log(e.target.checked)}
          {...props}
        />
      </Form>
    </StyleSwitch>
  );
};
