import styled from "styled-components";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { ProtectRoute } from "../../hooks/ProtectedRoute";
import {Providers} from "../../Context/providers";
import { useAuth } from "../../hooks/useContext";
import { LayoutAuthLogin } from "../LayoutAuthLogin";
import { Loader, SideBar,TopNav1 } from "../../Components";
import toast from "react-hot-toast";
import Head from 'next/head'
import Script from "next/script";
import { useState } from "react";


//import { userDetailsContext } from "../../context";
const Style = styled.div`
  background: #fbfafc;
  section {
    position: relative;
    min-height: 100vh;
  }

  .sidebar {
    /* border: 1px solid; */
    background: #fff;
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    width: 250px;
    z-index: 1;
    // z-index:9000;
  }
  .main {
    width: 100%;
    /* border: 1px solid; */
    margin-left: 250px;
    background: #f2f2f2;
    overflow: hidden;
  }
  input,
  select {
    box-shadow: none !important;
    outline: none !important;
  }
  main {
    min-height: 100vh;
  }

  @media (max-width: 765px) {
    .main {
     
      margin-left: 0;
     
    }
    .sidebar {
       display:${props=>props?.sideBar?'':'none'};
    }
  }
  .imageWrapper{
    border-radius: 50%;
    width: 40px;
    height: 40px;
    overflow: hidden;
  }
  .imageWrapper img{
    width:100%;
    height:100%;
  }
`;

export const LayoutAuth = ({ children,Title }) => {
  const router = useRouter();
  const {userProfile,state,logout} =useAuth()
  const [toogleSideBar,setToogleSideBar] =useState(false)
  useEffect(() => {
    if (userProfile?.user?.id&&userProfile?.user?.account_type !== 'Client') {
      toast.error('You are not authorized')
      logout()
    }
  }, [logout, userProfile?.user?.account_type, userProfile?.user?.id]);
  
  
  //  if(!userProfile?.user?.account_type==='Client'){
  //   return <><Loader/></>
  // }
   

  return (
    < >
      <ProtectRoute>
      <Head>
        <title>PocketLawyers | {Title}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/icon.svg" />
        
      </Head>
        <Style sideBar={toogleSideBar}>
          <div className="d-flex">
            <div className="sidebar   ">
              {/* <SideBar /> */}
            <div className="mx-auto ">
            <SideBar setToogleSideBar={setToogleSideBar}/>
            </div>
            </div>
            <main className="main px-2 px-lg-3">
              <TopNav1 setToogleSideBar={setToogleSideBar}/>
              {/* <Navbar /> */}
              {children}
            </main>
          </div>
        <div>
          <Script
        id="stripe-js"
        src="https://js.stripe.com/v3/"
        onLoad={() => {
          (function(d, w, c) {
            w.ChatraID = 'bMHks3xAj9mM7ehoM';
            var s = d.createElement('script');
            w[c] = w[c] || function() {
                (w[c].q = w[c].q || []).push(arguments);
            };
            s.async = true;
            s.src = 'https://call.chatra.io/chatra.js';
            if (d.head) d.head.appendChild(s);
        })(document, window, 'Chatra');
        }
      }
      />
            </div>
         
        </Style>
      </ProtectRoute>
    </>
  );



 return <div><Loader/></div>
}

