import React, { useEffect } from 'react'
import { LoginForm} from '../../Components'
import { LayoutNoAuth } from '../../Layout'
import { useAuth } from "../../hooks/useContext";
import {useRouter} from 'next/router'

export default function Index() {
  const router = useRouter();

  const {
    Login,state:{isLoading},userProfile:{user},isAuthenticated
  } = useAuth();
  //console.log(isLoading,'isAuthenticated')
  // useEffect(() => {
  //   // window.location.href = "/login";
  //   if (isAuthenticated && user?.account_type === 'Lawyer') {
  //     return router.push("/dashboard/lawyer");
  //   }
  //   if (isAuthenticated && user?.account_type === 'Client') {
  //     return router.push("/dashboard/client");
  //   }
  // }, [isAuthenticated, router,user?.account_type ]);

  // // //console.log(userProfile,'userProfile Login')

  // // //console.log(useAuth())
  return (
    <LayoutNoAuth footer Title={"Login"}>
        <LoginForm Login={Login} loading={isLoading}/>
    </LayoutNoAuth>
  )
}
