import { useState } from "react";
import toast from "react-hot-toast";
import { PersonalForm, CreatePassword } from "../../Components";
import { useAuth } from "../../hooks/useContext";
import { LayoutNoAuth } from "../../Layout";
import { retailserviceData2 } from "../../utils";

export default function Index() {
  const [signupLevel, setSignupLevel] = useState(0);
  const [showError,setShowError] =useState(false)
  const { RegisterClientComp,getStateFunc,getState ,state} = useAuth();
  const [getStateid,setgetStateid]=useState()
  const intialState = {
    title: "",
    first_name: "",
    last_name: "",
    password: "",
    confirm_password:'',
    email: "",
    gender: "",
    phone_number: "",
    dob: "",
    home_address: "",
    city: "",
    state_of_origin: "",
    country_of_origin: "",
  };
  const [userdata, setUserData] = useState(intialState);

  const handelSubmit = (e) => {
    e.preventDefault()
    ////console.log('there',getState?.state[getStateid]?.id)
    if(userdata?.password !== userdata?.confirm_password){
     return toast.error('You have entered different Password')
    }
    const  values ={
      ...userdata,
  
    country_of_origin: getState?.state[getStateid]?.country_id,
    state_of_origin: getState?.state[getStateid]?.id,
    }
  RegisterClientComp(values)
   // //console.log(values,'getStateFunc');
  };
 // //console.log(state,'state');
  const handleOnChange = (e) => {
    //console.log(e.target)
    const { name, value } = e.target;
    setUserData({ ...userdata, [name]: value });
  };
  return (
    <LayoutNoAuth Title={"Signup"} footer>
      {signupLevel == 0 ? (
        <PersonalForm setSignupLevel={setSignupLevel} showError={showError} setShowError={setShowError} setgetStateid={setgetStateid} userdata={userdata} state={getState?.state}   handleOnChange={handleOnChange} getStateFunc={getStateFunc}/>
      ) : (
        <CreatePassword handleOnChange={handleOnChange}  setSignupLevel={setSignupLevel} userdata={userdata} handelSubmit={handelSubmit} loading={state?.isLoading}/>
      )}
    </LayoutNoAuth>
  );
}
