/* eslint-disable react/no-unescaped-entities */
import { useState } from "react";
import { ForgetpasswordForm } from "../../Components";
import { useAuth } from "../../hooks/useContext";
import { LayoutNoAuth } from "../../Layout";

export default function Index() {
  const [toggle, setToogle] = useState(0);
  const intialState= {email:''}
  const {RequestPassordReset,state:{isLoading}} =useAuth()
  const [userData,setUserData]=useState(intialState)

  const handleOnchange = (e) =>{
    const {name,value}= e.target
    setUserData({...userData,[name]:value})
  }

  const handleSubmitEmail = ()=>{
    const value ={
      email:userData?.email
    }
    RequestPassordReset(value)
  }

  // const handleSubmitRestPassword = ()=>{

  // }
  return (
    <LayoutNoAuth Title={"Forget Password"} footer>
      <div className="pb-8 pt-5">
   
          <ForgetpasswordForm setToogle={setToogle} isLoading={isLoading}  handleOnchange={handleOnchange} handleSubmitEmail={handleSubmitEmail}/>
        
      </div>
    </LayoutNoAuth>
  );
}
