import { RCard3, RCard4 } from "../../Components";
import { dashBoardServices, retailPackage2 } from "../DataUtils";
import Link from 'next/link'
export function Settings(RetailServiceSize) {
  const settings = {
    dots: true,

    infinite: false,
    speed: 500,
    slidesToShow: RetailServiceSize >= 5 ? 5 : 3,
    slidesToScroll: RetailServiceSize >= 5 ? 1 : 2,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,

          dots: true,
        },
      },
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 580,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return settings;
}



export const handleRetailServices = (retailService,setRetailerServiceData,setRetailServices,retailerServiceData,setSwitchFromPackageToPay,formToggle,retailPackageMain,getRetainerPackageSubscriptionFunc,retailsolutions,clientSubRetainSolution) => {
// //console.log(clientSubRetainSolution,'retailPackageMain')
// const bola = [4,14]
  if(!retailService && !retailerServiceData && !formToggle){
    return <div className="">
    {/*  */}
    <div>
      <div className="text-type1 fw-bolder h8 my-3">
        Retail Solutions
      </div>
      <div>
        <div className="row g-3">
          {retailsolutions?.map((item, index) => (
            <div
              key={index}
              className="col-md-6 col-lg-4 col-xl-3 mb-1"
              
            >
         
         <RCard3 item={item} index={index} setRetailServices={setRetailServices} clientSubRetainSolution={clientSubRetainSolution}/>
            
            
            </div>
          ))}
        </div>
      </div>
    </div>
    {/*  */}
    <div>
      <div className="text-type1 fw-bolder h8 my-3">
        Retainer Packages
      </div>
      <div>
        <div className="row g-3">
          {/* {retailPackage2.map((item, index) => {
            if (item?.title) {
              return (
                <div
                  key={index}
                  className="col-6 col-md-4 col-lg-3 col-xl-2 mb-1"
                  onClick={() => {
                    // setShow(true);
                     setRetailServices("")
                    setRetailerServiceData(item);
                    setSwitchFromPackageToPay(0)
             
                  }}
                >
                  <RCard4 item={item} />
                </div>
              );
            }
          })} */}
          {retailPackageMain && retailPackageMain?.map((item, index) => {
            if (item?.name) {
              return (
                <div
                  key={index}
                  className="col-6 col-md-4 col-lg-3 col-xl-2 mb-1"
                  onClick={() => {
                    // setShow(true);
                     setRetailServices("")
                    // setRetailerServiceData(item);
                    // setSwitchFromPackageToPay(0)
                    // getRetainerPackageSubscriptionFunc(item?.id)
                    //console.log('getRetainerPackageSubscriptionFunc,getRetainerPackageSubscriptionFunc')
                  }}
                >
                  <Link href={`/dashboard/client/subcription/${item?.id}`}>
                    <a>
                    <RCard4 item={item} />
                    </a>
                  </Link>
                
                </div>
              );
            }
          })}
        </div>
      </div>
    </div>
    {/*  */}
    {/* <div className="mb-5 mb-lg-0">
      <div className="text-type1 fw-bolder h8 my-3">News Feed</div>
      <div>
        <div className=" g-3">
          <span>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Consectetur pellentesque ut ipsum interdum vulputate. Ut
            phasellus facilisis a cras. Vulputate dictumst mauris,
            metus fermentum, eget ut integer elit facilisis.
            Nascetur odio lacinia mi nullam consectetur lorem
            malesuada{" "}
          </span>
        </div>
      </div>
    </div> */}
  </div>
  }
   
  

}


export const Years = () => {
  const year = [];
  let i = 1958;
  let currentYear = new Date().getFullYear();
  for (i; i <= currentYear; i++) {
    //starts loop
    year.push({name:i});
  } //ends loop
  //Or:
 
  return year;
};


export const HowManyYears = () => {
  const year = [];
  let i = 1;
  // let currentYear = new Date().getFullYear();
  for (i ; i <= 20; i++) {
    //starts loop
    year.push({name:i});
  } //ends loop
 
  return year;
};
