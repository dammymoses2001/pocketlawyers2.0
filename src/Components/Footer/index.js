import React from "react";
import { FaFacebookF, FaLinkedinIn, FaTwitter } from "react-icons/fa";
import styled from "styled-components";
import Image from 'next/image'
import {CgArrowLongRight} from 'react-icons/cg'
import Logo from '../../assest/Image/Logo2.svg'
import Link from 'next/link'
import { useAuth } from "../../hooks/useContext";
import { useState } from "react";

const Style = styled.div`
  padding: 3rem 0 2rem 0;
  background: #124A54;

  hr {
    background-color: white;
  }
  i {
    width: 30px;
    height: 30px;
    border: 1px solid #fff;

    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    /* background: #ed6710; */
  }
  h6{
    font-size:16px !important;
  }
  button{
    outline:none;
    border:none;
    box-shadow:none !important;
  }
  input{
    border:none;
    outline:none;
  }
`;

export const Footer = () => {
  const {SubscribeNewLetter} =useAuth()
  const [email,setEmail]=useState()
  return (
    <Style>
      <div className="container">
        <div className="">
          <div className="row justify-content-center g-0">
            <div className='col-lg-4 mb-4 mb-lg-0'>
              <Image src={Logo} alt='' />
            </div>
            <div className='col-lg-2 text-white'>
              <div>
              
                <ul className="list-unstyled fw-0">
                  <li className="mb-3 fw-0"><Link href='/'><a>Home</a></Link></li>
                  <li className="mb-3 fw-0"><Link href='/about'><a>About</a></Link></li>
                  <li  className="mb-3 fw-0"><Link href='/services'><a>Services</a></Link></li>
                  <li  className="mb-3 fw-0"><Link href='/contact'><a>Contact</a></Link></li>
                </ul>
              </div>
            </div>
            <div className='col-lg-2 text-white mb-4'>
            <div>
             
                <ul className="list-unstyled fw-0">
                <li className="mb-3 fw-0"><Link href='/'><a>Terms of Use</a></Link></li>
                  <li className="mb-3 fw-0"><Link href='/about'><a>Privacy Policy</a></Link></li>
                  <li  className="mb-3 fw-0"><Link href='/services'><a>Disclaimer</a></Link></li>
                </ul>
              </div>
            </div>
            <div className='col-lg-4 text-white'>
            <div>
                <h6 className="mb-4 fw-bolder">Subscribe</h6>
                <div>
                  <div className="d-flex border-1 rounded">
                    <input className="w-100 py-3 px-3 border-0 rounded-start" name="email" type={"email"} placeholder='Get product updates' onChange={(e)=>setEmail({email:e.target.value})}/>
                    <button className="px-4 bg-type1 border-0 rounded-end" onClick={()=>SubscribeNewLetter(email)}><CgArrowLongRight size={30} color="#fff"/></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
         <div className="px-lg-5">
         <hr className="mb-4" />
          <div className="row justify-content-lg-evenly align-item-center">
            <div className="col-md-6 col-lg-2 mb-5">
              <div className="me-3">
                <div className="d-flex justify-content-md-between  ">
                <i className="">
                <a target={"_blank"} rel="noreferrer"   href={"https://www.linkedin.com/company/pocketlawyers-io/"}> <FaLinkedinIn color="#fff" /></a>
                   
                  </i>
                  <i className="mx-5 mx-md-0">
                  <a target={"_blank"} rel="noreferrer" href={"https://www.instagram.com/pocketlawyers.io?utm_medium=copy_link"} >    <FaFacebookF color="#fff" /></a>
               
                  </i>
                  <i className=" ">
                  <a target={"_blank"} rel="noreferrer"  href={"https://www.twitter.com/pocketlawyersio?t=VvNpXoOovM_qvDNUBm5XdQ&s=09"}>  <FaTwitter color="#fff" /></a>
                  
                  </i>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-2 mb-5">
              <h6 className="text-white fw-normal text-lg-center fw-1">
                +234 816 686 4154
              </h6>
            </div>
            <div className="col-md-6 col-lg-3 mb-3">
              <p className="text-white text-lg-center">
              office@pocketlawyers.io
              </p>
            </div>
            <div className="col-md-4 col-lg-3 mb-3">
              <p className="text-white text-lg-end">
                © 2022 All rights reserved
              </p>
            </div>
          </div>
         </div>
        </div>
      </div>
    </Style>
  );
};
