import React from 'react'
import { AboutService } from '../../Components'
import { LayoutNoAuth } from '../../Layout'


export default function index() {
  return (
    <LayoutNoAuth Title={"About"}>
     <AboutService/>
    </LayoutNoAuth>
  )
}
