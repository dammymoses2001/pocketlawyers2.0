import Image from "next/image";
import Google from "../../public/google.svg";
import Apple from "../../public/apple.svg";

export const GoogleButton = ({ className }) => {
  return (
    <button className={`px-0  mx-2 btn ${className}`}>
      <Image src={Google} alt="" />
    </button>
  );
};

export const AppleButton = ({ className }) => {
  return (
    <button className={`px-0  mx-2 btn ${className}`}>
      <Image src={Apple} alt="" />
    </button>
  );
};
