/* eslint-disable @next/next/no-img-element */
import styledComponents from "styled-components"
import AboutImage from '../../assest/Image/aboutImage.svg'
import Image from 'next/image'
import LogoDashBoard from "../../assest/Image/AboutBack.png";

const Style =styledComponents.div`
.background{
    position: absolute;
    right: 0;
    top: 80px;
    height: 80px;
    width: 35%;
}
.container{
    overflow:hidden;
}
`

export const AboutService =()=> {
  return (
    <Style>
        
        <div className="container position-relative">
        {/* <div className="background">
                   <Image src={LogoDashBoard} alt='' />
               </div> */}
               <div className="py-3 py-lg-5 my-lg-3"></div>
           <div className="row g-5 mb-5">

               <div className="col-lg-4">
               <div className=' rounded-4' data-aos="fade-right">
                <img src={'/about.svg'} className='w-100' alt='' style={{borderRadius:'10px'}}/>
            </div>
               </div>
               <div className="col-lg-7">
                <div data-aos="fade-left">
                <div className="text-type1 text-uppercase h5 font-5 fw-2">Who we are</div>
                <h5 className="h5 font-3 text-justify fw-3 mb-3 text-justify">
                PocketLawyers is Africa’s first fully integrative legal tech startup that offers access to affordable premium legal services and solutions to SMEs and Startups. 
                </h5>
                <div className="h51 font-3 mb-3">
                We are in the business of ensuring legal and financial inclusion by simplifying legal services and solutions to the problems Entrepreneurs face everyday on their journey. The need for legal documentation in everyday life and business cannot be overemphasized. Since 2020, we have been bridging the legal gap and fostering seamless business transactions in Nigeria and across Africa.


                </div>
                <div className="h51 font-3 mb-3 text-justify">
                We are strongly committed to promoting inclusion, breaking legal barriers and changing the age old legal narrative. We are about dispelling the notion that Lawyers are inaccessible and legal services are out of the financial reach of the average business owner. This is in key fulfillment of goal eight of the global goal for sustainable development to make sure everybody gets the benefit of entrepreneurship and innovation and goal sixteen access to justice and legal representation.



                </div>
                <div className="h51 font-3 mb-3 text-justify">
                Together with our team of energetic, forward thinking, young Professionals, PocketLawyers is building a roadmap to the future of how legal services should be offered and we are fixated on delivering on this promise. We aim to deliver value by staying abreast with current and all developing technologies and in order to be a distinguished industry contributor.


                </div>
                <div className="h51 text-7 text-decoration-underline fw-2">
                <a className="" href="/company profile.pdf" target={"_blank"} download>Download Company Profile</a>
                </div>
                </div>
            </div>
               
           </div>
           <div className="mb-5">
            <div className="py-3"></div>
            <div className="row g-5">
                <div className="col-lg-6 h-100  " data-aos="fade-left">
                    <div className="p-5" style={{background:'#FDF0E7',borderRadius:'20px',overflow:'hidden'}}>
                    <div className="h5 text-type1 fw-3 mb-3 font-4">
                    OUR VISION
                    </div>
                    <div className="h51 font-3">
                    To become the virtual firm of first choice for accessing legal services and solutions by business across the World.
                    </div>
                    </div>
                </div>
                <div className="col-lg-6  " data-aos="fade-right" >
                    <div className="p-5 h-100" style={{background:'#EAF8FA',borderRadius:'20px',overflow:'hidden'}}>
                    <div className="h5 text-type1 text-uppercase fw-3 mb-3 font-4">
                    OUR Mission
                    </div>
                    <div className="h51 font-3">
                    To demystify, simplify, democratize and quicken access to legal services and solutions.                    </div>
                    </div>
                </div>
            </div>
            <div className="py-3 py-lg-4"></div>
           </div>

        </div>
    </Style>
  )
}
