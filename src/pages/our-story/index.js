import React from 'react'
import styled from 'styled-components'
import { LayoutNoAuth } from '../../Layout'


const StoryStyle = styled.div`
.high{
    position: absolute;
    height: 450px;
    width: 450px;
}
`

export default function index() {
  return (
    <LayoutNoAuth Title={"Our Story"}>
        <StoryStyle>
         <div className="row justify-content-center">
          <div className="col-10 col-lg-10">
            <div className="py-5 py-lg-5 my-3"></div>
            <div className="mb-5">
              <h5 className=" fw-1 text-capitalize">How we started</h5>
              <h3 className="h3 font-4 text-type1">Our Story</h3>
            </div>
            <div className='position-relative' data-aos="flip-left">
                <div className='bg-13 high d-none d-md-block'></div>
                <div className=' py-lg-5 my-lg-5'></div>
                <div className='d-flex justify-content-end'>
                   
                <div className='bg-13  col-lg-9 p-3 p-lg-4 position-relative' style={{borderRadius:'20px'}}>
                    <div className='mb-3 h51 text-justify'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Donec massa sapien faucibus et. Velit dignissim sodales ut eu sem integer. Mollis nunc sed id semper risus. Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Diam quam nulla porttitor massa id. Quisque sagittis purus sit amet volutpat. Blandit cursus risus at ultrices mi tempus. Faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. In ante metus dictum at tempor commodo ullamcorper. Ullamcorper eget nulla facilisi etiam dignissim diam quis enim. Leo integer malesuada nunc vel risus commodo viverra.

           
                    </div>
                    <div className='mb-3 h51 text-justify'>
                    e sapien pellentesque habitant morbi. In tellus integer feugiat scelerisque varius morbi enim nunc. Mi proin sed libero enim sed. In iaculis nunc sed augue. Et netus et malesuada fames ac turpis egestas sed tempus. Mauris nunc congue nisi vitae suscipit tellus mauris a. Tristique nulla aliquet enim tortor at auctor. Metus vulputate eu scelerisque felis imperdiet proin fermentum. Consequat mauris nunc congue nisi vitae suscipit tellus mauris. Risus in hendrerit gravida rutrum. Amet est placerat in egestas erat imperdiet. In hendrerit gravida rutrum quisque. Nisl nisi scelerisque eu ultrices. Sed turpis tincidunt id aliquet risus. Accumsan tortor posuere ac ut consequat. Sit amet mauris commodo quis imperdiet. Feugiat vivamus at augue eget arcu dictum varius. Consectetur adipiscing elit duis tristique sollicitudin nibh sit. Accumsan tortor posuere ac ut consequat semper viverra.
         
                    </div>
                    <div className='mb-3 h51 text-justify'>
                    Molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit. Tellus in hac habitasse platea dictumst vestibulum rhoncus. Eget sit amet tellus cras adipiscing enim eu turpis. Auctor urna nunc id cursus. Tincidunt nunc pulvinar sapien et ligula. Non consectetur a erat nam at lectus urna duis convallis. Quam nulla porttitor massa id neque aliquam. Tellus orci ac auctor augue mauris augue neque. Et ultrices neque ornare aenean euismod elementum nisi quis eleifend. Nulla at volutpat diam ut.

A condimentum vita
                    </div>
                </div>
                </div>
            </div>
            <div className="py-3 py-lg-5 my-lg-3"></div>
            </div>
            </div>
            </StoryStyle>
    </LayoutNoAuth>
  )
}
