import Link from "next/link";
import React from "react";
import { Card } from "react-bootstrap";
// import Slider from "react-slick";
import styled from "styled-components";
import { HiArrowRight } from "react-icons/hi";
import { RetailCard1, RetailCard2 } from "..";

const Style = styled.div`
  padding: 5rem 0;
  h3 {
    font-weight: 700;
  }
  h4 {
    font-weight: 700 !important;
    font-size: 20px !important;
    color: #0d4447;
  }
 
  .card {
    background: #ffffff;
    border: 2px solid #0d4447;
    box-sizing: border-box;
    box-shadow: -1.41011px -1.41011px 2.82022px rgba(0, 0, 0, 0.15),
      1.41011px 1.41011px 2.82022px rgba(0, 0, 0, 0.15);
    border-radius: 7.05056px 7.05056px 0px 0px;
    margin: 0 10px;
    min-height: ${(props) => (props.SizeComp ? props.SizeComp : "15rem")};
  }

  .slick-slide {
    margin-bottom: 61px;
  }

  .slick-list {
    margin-bottom: 61px;
  }
  .slick-track {
    display: flex !important;
  }

  .slick-slide {
    height: auto !important;
  }
  .slick-slide > div:first-of-type {
    height: 100%;
  }
`;

export const RetailSolution = ({
  title,
  RetailService,
  retailpackage,
  size,
  SizeComp
  ,link
}) => {
  const RetailServiceSize = RetailService.length;

  // const settings = {
  //   dots: true,

  //   infinite: false,
  //   speed: 500,
  //   slidesToShow: RetailServiceSize >= 5 ? 5 : 3,
  //   slidesToScroll: RetailServiceSize >= 5 ? 1 : 2,
  //   responsive: [
  //     {
  //       breakpoint: 1024,
  //       settings: {
  //         slidesToShow: 3,
  //         slidesToScroll: 3,

  //         dots: true,
  //       },
  //     },
  //     {
  //       breakpoint: 600,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 2,
  //         initialSlide: 2,
  //       },
  //     },
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1,
  //       },
  //     },
  //   ],
  // };

  return (
    <Style SizeComp={size}>
      <div className="container">
        <div className="text-center">
          <h3 className=" text-black mb-5">{title}</h3>
        </div>
        <div>
          {/* <Slider {...settings}> */}
          <div className="row">
            {RetailService.map((item, index) => (
              <div
                key={index}
                className=" col-sm-6 col-md-4 col-lg-3 mb-3 text-type2"
                data-aos="zoom-out-right"
                data-aos-delay={index * 100}
              >
                <Card className="h-100">
                  {retailpackage ? (
                    <RetailCard2 item={item} size={'25px'} />
                  ) : (
                    <RetailCard1 item={item}  />
                  )}
                </Card>
              </div>
            ))}
          </div>
          {/* </Slider> */}
          <div className="text-end mt-5 d-flex justify-content-end">
            <Link href={link}>
            <a  className="bg-type1 text-white px-3 py-2 fw-2 rounded d-flex align-items-center"
            data-aos="fade-zoom-in"
            data-aos-easing="ease-in-back"
            data-aos-delay="300"
            data-aos-offset="0"
            >
              See All{" "}
              <i className="ms-3">
                <HiArrowRight />
              </i>
            </a>
            </Link>
          </div>
        </div>
      </div>
    </Style>
  );
};
