import StartCompanyImage from "../../../public/servicei.png";
import RegistrationImage from "../../../public/serviceii.png";
// import IntellectualImage from "../../public/serviceiii.png";
import IntellectualImage from "../../../public/serviceiii.png";
import DocumentImage from "../../../public/serviceiv.png";
import ContractImage from "../../../public/servicev.png";
import LegalImage from "../../../public/servicevi.png";
import DueImage from "../../../public/servicevii.png";
import LicensesImage from "../../../public/serviceviii.png";
import BusinessImage from "../../../public/serviceix.png";
import OthersImage from "../../../public/servicex.png";

import Pic from "../../assest/Image/dlm.jpg";
import Pic1 from "../../assest/Image/feedbback2.svg";
// import Pic3 from "../../public/Pic3.png";

//sidebar
import HomeIcon from "../../assest/Image/homeicon.svg";
import HomeIcon1 from "../../assest/Image/homeicon1.svg";
import MessageIcon from "../../assest/Image/Chat.svg";
import MessageIcon1 from "../../assest/Image/chat1.svg";
import Settings from "../../assest/Image/Setting.svg";
import Settings1 from "../../assest/Image/Setting1.svg";
import Subscription from "../../assest/Image/wallet.svg";
import Subscription1 from "../../assest/Image/wallet1.svg";

//settings sideBar
import UserIcon from "../../assest/Image/settingusericon.svg";
import UserIcon1 from "../../assest/Image/settingusericon1.svg";
import Notification from "../../assest/Image/notificationicon.svg";
import Notification1 from "../../assest/Image/notificationicon1.svg";
import Support from "../../assest/Image/supporticon.svg";
import Support1 from "../../assest/Image/supporticon1.svg";
import FAQ from "../../assest/Image/faqsIcon.svg";

import dashboard1 from "../../assest/Image/services1.svg";
import dashboard11 from "../../assest/Image/services11.svg";
import dashboard2 from "../../assest/Image/services2.svg";
import dashboard22 from "../../assest/Image/services22.svg";
import dashboard3 from "../../assest/Image/services3.svg";
import dashboard33 from "../../assest/Image/services33.svg";
import dashboard4 from "../../assest/Image/services4.svg";
import dashboard44 from "../../assest/Image/services44.svg";
import dashboard5 from "../../assest/Image/services5.svg";
import dashboard55 from "../../assest/Image/services55.svg";
import dashboard6 from "../../assest/Image/services6.svg";
import dashboard66 from "../../assest/Image/services66.svg";
import dashboard7 from "../../assest/Image/services7.svg";
import dashboard77 from "../../assest/Image/services77.svg";
import dashboard8 from "../../assest/Image/services8.svg";
import dashboard88 from "../../assest/Image/services88.svg";
import dashboard9 from "../../assest/Image/services9.svg";
import dashboard99 from "../../assest/Image/services9.9svg.svg";
import dashboard10 from "../../assest/Image/services10.svg";
import dashboard101 from "../../assest/Image/services101.svg";
// import dashboard1 from '../../assest/Image/services1.svg'
// import dashboard1 from '../../assest/Image/services1.svg'
import Avatar from '../../assest/Image/avatar.jpg'

//SubscriptionPage Icon
import SubscriptionIcon from "../../assest/Image/subscriptionlogo.svg";
import SubscriptionIcon1 from "../../assest/Image/subscriptionlogodark.svg";
import SubscriptionWallet from "../../assest/Image/subscriptionwallet.svg";
import SubscriptionWallet1 from "../../assest/Image/subscriptionwalletdark.svg";

//
import VisaIcon from "../../assest/Image/VisaIcon.svg";
import { BsThreeDots } from "react-icons/bs";

//Lawyer
import LawyerServiceIcon from "../../assest/Image/lawyerserviceicon.svg";
import LawyerReciptIcon from "../../assest/Image/lawyerrecipt.svg";
import LawyerShedularIcon from "../../assest/Image/lawyerscheduler.svg";
//
import LawyerPaymentIcon from "../../assest/Image/lawyerpaymenticon.svg";
import LawyerPaymentIcon1 from "../../assest/Image/lawyerpaymenticon1.svg";
import LawyerBankIcon from "../../assest/Image/bankicon.svg";
import Receipts1 from "../../assest/Image/receipts.svg";
import Receipts from "../../assest/Image/Recipt1.svg";

import { GrCheckmark } from "react-icons/gr";
import { TiTimes } from "react-icons/ti";
//

import LawyerSchedularicon1 from "../../assest/Image/caseIcon.svg";
import LawyerSchedularicon2 from "../../assest/Image/droplet.svg";

export const retailserviceData = [
  {
    name: "Start a company",
    icon: StartCompanyImage,
  },
  {
    name: "Post registration matters",
    icon: RegistrationImage,
  },
  {
    name: "Intellectual Property Protection",
    icon: IntellectualImage,
  },
  {
    name: "Document & Contract Review",
    icon: DocumentImage,
  },
  //   {
  //     name: "Contract & Letter  Drafting",
  //     icon: ContractImage,
  //   },
  //   {
  //     name: "Legal Advisor",
  //     icon: LegalImage,
  //   },
  //   {
  //     name: "Due diligence (request a quote) ",
  //     icon: DueImage,
  //   },
  //   {
  //     name: "Licenses & Permits. ",
  //     icon: LicensesImage,
  //   },
  //   {
  //     name: "Business Policy",
  //     icon: BusinessImage,
  //   },
  //   {
  //     name: "Others (Service isn’t mentioned)",
  //     icon: OthersImage,
  //   },
];

export const retailPackage = [
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> SS</span>
        <span className="fw-normal"> Business</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> SS</span>
        <span className="fw-normal"> Business+</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> SS</span>
        <span className="fw-normal subtitle"> Business Premuim</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> MS</span>
        <span className="fw-normal subtitle"> Business</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  // {
  //   title: (
  //     <div>
  //       <span>MS</span>
  //       <br />
  //       Business+
  //     </div>
  //   ),
  //   desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  // },
  // {
  //   title: (
  //     <div>
  //       <span>Other</span>
  //       <br />
  //       Business
  //     </div>
  //   ),
  //   desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  // },
];

export const ClientFeedBackData = [
  {
    name: "Ayodeji Odesola",
    type: "Cashia (CEO)",
    desc: "We have developed a great partnership with Pocket Lawyers and their dedication to our startup's project is evident in all their service offerings. We appreciate their attention to detail and creative approach to solving startup's legal needs.",
    pic: "/client1.svg",
    background: "#EAF8FA",
    iconbackground: "#218698",
  },
  {
    name: "Ayodeji Odesola",
    type: "Cashia CEO",
    desc: "We have developed a great partnership with Pocket Lawyers and their dedication to our startup's project is evident in all their service offerings. We appreciate their attention to detail and creative approach to solving startup's legal needs",
    pic: Avatar,
    background: "#FDF0E7",
    iconbackground: "#ED6710",
  },
  {
    name: "Aghoghome Agino",
    type: "Tahor kleaning services COO",
    desc: "Every Sailor needs a compass to safely sail the seas, pocket Lawyers have been our compass since inception  guiding us with a lot of patience and professionalism.We appreciate the way you handle all our legal ignorance with patience and how you systematically help set a structure  that safeguards our business",
    pic: Avatar,
    background: "#EAF8FA",
    iconbackground: "#218698",
  },
  {
    name: "Ayodeji Odesola",
    type: "Cashia CEO",
    desc: "We have developed a great partnership with Pocket Lawyers and their dedication to our startup's project is evident in all their service offerings. We appreciate their attention to detail and creative approach to solving startup's legal needs",
    pic: Avatar,
    background: "#FDF0E7",
    iconbackground: "#ED6710",
  },
  {
    name: "Aghoghome Agino",
    type: "Tahor kleaning services COO",
    desc: "Every Sailor needs a compass to safely sail the seas, pocket Lawyers have been our compass since inception  guiding us with a lot of patience and professionalism.We appreciate the way you handle all our legal ignorance with patience and how you systematically help set a structure  that safeguards our business",
    pic: Avatar,
    background: "#EAF8FA",
    iconbackground: "#218698",
  },
  // {
  //   name: "Malik Garuba",
  //   type: "SMEs",
  //   desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lectus fusce feugiat.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lectus fusce feugiat  Lorem ipsum dolor sit amet. ",
  //   pic: Pic1,
  // },
  // {
  //   name: "Malik Garuba",
  //   type: "SMEs",
  //   desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lectus fusce feugiat.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lectus fusce feugiat  Lorem ipsum dolor sit amet. ",
  //   pic: Pic,
  // },
];

export const retailserviceData2 = [
  {
    name: "Start a company",
    icon: StartCompanyImage,
  },
  {
    name: "Post registration matters",
    icon: RegistrationImage,
  },
  {
    name: "Intellectual Property Protection",
    icon: IntellectualImage,
  },
  {
    name: "Document & Contract Review",
    icon: DocumentImage,
  },
  {
    name: "Contract & Letter  Drafting",
    icon: ContractImage,
  },
  {
    name: "Legal Advisor",
    icon: LegalImage,
  },
  {
    name: "Due diligence (request a quote) ",
    icon: DueImage,
  },
  {
    name: "Licenses & Permits. ",
    icon: LicensesImage,
  },
  {
    name: "Business Policy",
    icon: BusinessImage,
  },
  {
    name: "Others (Service isn’t mentioned)",
    icon: OthersImage,
  },
];

export const retailPackage1 = [
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> SS</span>
        <span className="fw-normal"> Business</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> SS</span>
        <span className="fw-normal"> Business+</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> SS</span>
        <span className="fw-normal subtitle"> Business Premuim</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> MS</span>
        <span className="fw-normal subtitle"> Business</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> MS</span>
        <span className="fw-normal subtitle"> Business+</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold"> MS Premuim</span>
        <span className="fw-normal subtitle"> Business+</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
  {
    title: (
      <div>
        <span className="title d-block fw-bold">Other</span>
        <span className="fw-normal subtitle"> Business</span>
      </div>
    ),
    desc: "ursus maecenas donec duis. Neque semper mattis facilisis aliquet nibh cras fringilla semper. Mi enim tincidunt  pretium interdum",
  },
];

export const TitleData = [
  {
    name: "Mr",
  },
  {
    name: "Mrs",
  },
  {
    name: "Miss",
  },
];

export const OptionData = [
  {
    name: "Yes",
  },
  {
    name: "No",
  },
];

export const aosComp = {
  // Global settings:
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: "DOMContentLoaded", // name of the event dispatched on the document, that AOS should initialize on
  initClassName: "aos-init", // class applied after initialization
  animatedClassName: "aos-animate", // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 120, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 700, // values from 0 to 3000, with step 50ms
  easing: "ease", // default easing for AOS animations
  once: true, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: "top-bottom", // defines which position of the element regarding to window should trigger the animation
};

export const dashBoardServices = [
  {
    name: "Start a Company",
    icon: dashboard1,
    icon1: dashboard11,
    servicesRender: [
      {
        name: "Register a business name",
      },
      {
        name: "Company incorporation",
      },
      {
        name: "Private or public company",
      },
      {
        name: "Registration of incorporated trustee",
      },
    ],
  },
  {
    name: "Post registration matters",
    icon: dashboard2,
    icon1: dashboard22,
    servicesRender: [
      {
        name: "TIN Registration",
      },
      {
        name: "Preparation of statement of affairs",
      },
      {
        name: "Filing of annual returns",
      },
      {
        name: "Alteration of name",
      },
      {
        name: "Alteration of shares/shareholding",
      },
      {
        name: "Alteration of Secretary",
      },
      {
        name: "Alteration of Business address",
      },
    ],
  },
  {
    name: "Intellectual property protection",
    icon: dashboard3,
    icon1: dashboard33,
    servicesRender: [
      {
        name: "Trademark",
      },
      {
        name: "Copyright",
      },
      {
        name: "Patent",
      },
    ],
  },
  {
    name: "Staff  Onboarding ",
    icon: dashboard4,
    icon1: dashboard44,
  },
  {
    name: "Business Policy ",
    icon: dashboard5,
    icon1: dashboard55,
  },
  {
    name: "Document Review & Contract",
    icon: dashboard6,
    icon1: dashboard66,
  },
  {
    name: "Contract & Letter Drafting",
    icon: dashboard7,
    icon1: dashboard77,
  },
  {
    name: "Legal Advisory",
    icon: dashboard8,
    icon1: dashboard88,
  },

  {
    name: "Licenses & Permits  ",
    icon: dashboard9,
    icon1: dashboard99,
  },

  {
    name: "Others ",
    icon: dashboard10,
    icon1: dashboard101,
    desc: "Hi there, please let us know what your challenge is so we can offer you a solution, you can send us a Voice note, we will be waiting. thank you",
  },
];

export const retailPackage2 = [
  {
    title: "SS",
    desc: "Business",
    name: "SS (Small Scale) Business",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
    pricing: [
      {
        name: "Annual (1 year)",
        price: "₦ 300, 000",
      },
      {
        name: "Bi - Annual (6 months)",
        price: "₦ 160, 000",
      },
      {
        name: "Quarterly (3 months)",
        price: "₦ 85, 000",
      },
      {
        name: "Monthly (1 month)",
        price: "₦ 30, 000",
      },
    ],
  },
  {
    title: "SS",
    desc: "Business+",
    name: "SS (Small Scale) Business +",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
    pricing: [
      {
        name: "Annual (1 year)",
        price: "₦ 400, 000",
      },
      {
        name: "Bi - Annual (6 months)",
        price: "₦ 210, 000",
      },
      {
        name: "Quarterly (3 months)",
        price: "₦ 110, 000",
      },
      {
        name: "Monthly (1 month)",
        price: "₦ 40, 000",
      },
    ],
  },
  {
    title: "SS",
    desc: "Business Premium",
    name: "SS (Small Scale) Business Premuim",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
    pricing: [
      {
        name: "Annual (1 year)",
        price: "₦ 520, 000",
      },
      {
        name: "Bi - Annual (6 months)",
        price: "₦ 520, 000",
      },
      {
        name: "Quarterly (3 months)",
        price: "₦ 140, 000",
      },
      {
        name: "Monthly (1 month)",
        price: "₦ 50, 000",
      },
    ],
  },
  {
    title: "MS",
    desc: "Business",
    name: "MS (Medium Scale) Business",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
    pricing: [
      {
        name: "Annual (1 year)",
        price: "₦ 2, 250, 000",
      },
      {
        name: "Bi - Annual (6 months)",
        price: "₦ 1, 325, 000",
      },
    ],
  },
  {
    title: "MS",
    desc: "Business+",
    name: "MS (Medium Scale) Business +",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
    pricing: [
      {
        name: "Annual (1 year)",
        price: "₦ 3, 750, 000",
      },
      {
        name: "Bi - Annual (6 months)",
        price: "₦ 2, 075, 000",
      },
    ],
  },
  {
    title: "MS ",
    desc: "Business Premium",
    name: "MS (Medium Scale) Business Premium",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
    pricing: [
      {
        name: "Annual (1 year)",
        price: "₦ 5, 550, 000",
      },
      {
        name: "Bi - Annual (6 months)",
        price: "₦ 2, 975, 000",
      },
    ],
  },
  {
    name: "Other Business +",
    desc1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et id duis eu sit. Et elementum turpis pulvinar scelerisque dapibus. Sollicitudin et sed aenean gravida sit diam. ",
  },
];

export const sideBarAuthClient = [
  {
    name: "Home",
    icon: HomeIcon1,
    icon1: HomeIcon,
    path: "/dashboard/client",
  },
  {
    name: "Message",
    icon: MessageIcon1,
    icon1: MessageIcon,
    path: "/dashboard/client/message",
  },
  {
    name: "Subscription",
    icon: Subscription1,
    icon1: Subscription,
    path: "/dashboard/client/subcription",
  },
  {
    name: "Settings",
    icon: Settings,
    icon1: Settings1,
    path: "/dashboard/client/settings",
  },
];

export const sideBarLawyerAuthClient = [
  {
    name: "Home",
    icon: HomeIcon1,
    icon1: HomeIcon,
    path: "/dashboard/lawyer",
  },
  {
    name: "Message",
    icon: MessageIcon1,
    icon1: MessageIcon,
    path: "/dashboard/lawyer/message",
  },
  {
    name: "Receipts",
    icon: Receipts,
    icon1: Receipts1,
    path: "/dashboard/lawyer/receipts",
  },
  {
    name: "Settings",
    icon: Settings,
    icon1: Settings1,
    path: "/dashboard/lawyer/settings",
  },
];

export const GenderData = [
  {
    name: "Male",
  },
  {
    name: "Female",
  },
];

export const subscriptionSideBar = [
  {
    name: "My Plan",
    icon: SubscriptionIcon,
    icon1: SubscriptionIcon1,
  },
  // {
  //   name: "Card / Payment ",
  //   icon: SubscriptionWallet,
  //   icon1: SubscriptionWallet1,
  // },
];

export const selectedPlan = [
  {
    name: "SS Business",
    status: "Active 1 Month",
    Summary: [
      {
        name: "Staff Strength",
        assets: "2-5",
      },
      {
        name: "Assets**",
        assets: "Less than N5M",
      },
    ],
    legalService: [
      {
        legal: "Legal advice and opinions on Nigerian laws",
        no: true,
      },
      {
        legal: "Drafting simple contracts / Review and other legal documents",
        no: true,
      },
      {
        legal: "Attending negotiations (virtual)",
        no: "1",
      },
      {
        legal: "Free access to online legal documents",
        no: true,
      },
      {
        legal: "Verbal solutions over phone",
        no: true,
      },
      {
        legal: "Structuring Advice",
        no: false,
      },
      {
        legal: "Drafting and reviewing company policies",
        no: false,
      },
      {
        legal: "Filing of Annual Returns with CAC",
        no: false,
      },
      {
        legal: "Dedicated Lawyers",
        no: "1",
      },
    ],
  },
  {
    name: "SS Business +",
    status: "Active 1 Month",
    Summary: [
      {
        name: "Staff Strength",
        assets: "6-10",
      },
      {
        name: "Assets**",
        assets: "N5M -N7M",
      },
    ],
    legalService: [
      {
        legal: "Legal advice and opinions on Nigerian laws",
        no: true,
      },
      {
        legal: "Drafting simple contracts / Review and other legal documents",
        no: true,
      },
      {
        legal: "Attending negotiations (virtual)",
        no: "2",
      },
      {
        legal: "Free access to online legal documents",
        no: true,
      },
      {
        legal: "Verbal solutions over phone",
        no: true,
      },
      {
        legal: "Structuring Advice",
        no: true,
      },
      {
        legal: "Drafting and reviewing company policies",
        no: true,
      },
      {
        legal: "Filing of Annual Returns with CAC",
        no: true,
      },
      {
        legal: "Dedicated Lawyers",
        no: "1",
      },
    ],
  },
  {
    name: "SS Business Premium",
    status: "Active 1 Month",
    Summary: [
      {
        name: "Staff Strength",
        assets: "11-20",
      },
      {
        name: "Assets**",
        assets: "Above N7M -N10M",
      },
    ],
    legalService: [
      {
        legal: "Legal advice and opinions on Nigerian laws",
        no: true,
        available:true
      },
      {
        legal: "Drafting simple contracts / Review and other legal documents",
        no: true,
        available:true
      },
      {
        legal: "Attending negotiations (virtual)",
        no: "3",
      },
      {
        legal: "Free access to online legal documents",
        no: true,
        available:true
      },
      {
        legal: "Verbal solutions over phone",
        no: true,
        available:true
      },
      {
        legal: "Structuring Advice",
        no: true,
        available:true
      },
      {
        legal: "Drafting and reviewing company policies",
        no: true,
        available:true
      },
      {
        legal: "Filing of Annual Returns with CAC",
        no: true,
        available:true
      },
      {
        legal: "Dedicated Lawyers",
        no: "1",
      },
    ],
  },
];

export const CardData = [
  {
    id: (
      <span>
        <BsThreeDots /> 9905
      </span>
    ),
    expires: "12/21",
    icon: VisaIcon,
  },
];

export const LawyerCardData = [
  {
    bankname: "Access Bank",
    name: "Bola Tinubu",
    accountNumaber: "0771236468",
    icon: LawyerBankIcon,
  },
];

export const SettingSideBar = [
  {
    name: "Update Profile",
    icon: UserIcon,
    icon2: UserIcon1,
  },
  {
    name: "Notification Setting",
    icon: Notification,
    icon2: Notification1,
  },
  {
    name: "Support",
    icon: Support,
    icon2: Support1,
  },
  {
    name: "FAQS",
    icon: FAQ,
    icon2: FAQ,
  },
];

export const LawyerSettingSideBar = [
  {
    name: "Update Profile",
    icon: UserIcon,
    icon2: UserIcon1,
  },
  {
    name: "Payment Details",
    icon: LawyerPaymentIcon,
    icon2: LawyerPaymentIcon1,
  },
  {
    name: "Notification Setting",
    icon: Notification,
    icon2: Notification1,
  },
  {
    name: "Support",
    icon: Support,
    icon2: Support1,
  },
  {
    name: "FAQS",
    icon: FAQ,
    icon2: FAQ,
  },
];

export const notificationSettings = [
  {
    name: "Email Notication",
    status: true,
  },
  {
    name: "SMS Notication",
    status: false,
  },
  {
    name: "Newsletter",
    status: true,
  },
];

export const lawyerHomeData = [
  {
    name: "Service",
    icon: LawyerServiceIcon,
  },
  {
    name: "Receipts",
    icon: LawyerReciptIcon,
  },
  // {
  //   name: "Scheduler",
  //   icon: LawyerShedularIcon,
  // },
];

export const recentServicesData = [
  {
    name: "Steven Buhari",
    image:
      "https://www.nairaland.com/attachments/186644_this_is_beauty_JPG5cd324565d7facd402cd95e9943fd9b0",
    services: [
      {
        name: "Register a business name",
      },
      {
        name: "Comapny incorporation",
      },
      {
        name: "Registration of incorporated trustee",
      },
    ],
  },
  {
    name: "Steven Buhari",
    image:
      "http://static.becomegorgeous.com/img/arts/2013/Apr/25/10490/jennifer_lawrence_named_worlds_most_beautiful_woman_2.jpg",
    services: [
      {
        name: "Register a business name",
      },
      {
        name: "Comapny incorporation",
      },
      {
        name: "Registration of incorporated trustee",
      },
    ],
  },
];

export const ReceiptData = [
  {
    status: 1,
    date: "26/02/2022",
    desc: "Regulatory/Statutory compliance clients while we help you procure all your licenses and permits. ",
    receiptid: "713R2GD",
    amount: "₦20,000",
  },
  {
    status: 1,
    date: "26/02/2022",
    desc: "Regulatory/Statutory compliance clients while we help you procure all your licenses and permits. ",
    receiptid: "713R2GD",
    amount: "₦20,000",
  },
  {
    status: 0,
    date: "26/02/2022",
    desc: "Regulatory/Statutory compliance clients while we help you procure all your licenses and permits. ",
    receiptid: "713R2GD",
    amount: "₦20,000",
  },
  {
    status: 0,
    date: "26/02/2022",
    desc: "Regulatory/Statutory compliance clients while we help you procure all your licenses and permits. ",
    receiptid: "713R2GD",
    amount: "₦20,000",
  },
];

export const LawyerSchedularData = [
  {
    name: "Licenses Meetings",
    time: "9:00 AM - 10:00 AM",
    image:
      "http://img.izismile.com/img/img3/20100225/most_beautiful_men_16.jpg",
    status: 1,
  },
  {
    name: "Business Policy",
    time: "11:00 AM - 10:00 AM",
    image:
      "https://2.bp.blogspot.com/-5QsdniQ33Cs/V1A7AMmx5bI/AAAAAAAAGRY/arwR8XEgynMklVFZB-mV5N-I0hbpQlb-ACKgB/s1600/24.jpg",
    status: 2,
  },
];

export const MessageData = [
  {
    name: "Bola Ogunde",
    pic: "https://www.barnorama.com/wp-content/uploads/2020/02/Hot-Women-50.jpg",
    messages: [
      {
        me: "Thank you",
      },
      {
        them: "ursus maecenas donec duis. Neque semper mattis facilisis alique semper. Mi enim t",
      },
      {
        them: "ursus maecenas donec duis. Neque semper mattis facilisis alique semper. Mi enim t",
      },
      {
        me: "Thank you",
      },
    ],
  },
  {
    name: "Kola",
    pic: "https://i.pinimg.com/736x/78/be/9c/78be9cfb3c7ef988f054228dc16e87f1.jpg",
    messages: [
      {
        me: "Thank you",
      },
      {
        them: "ursus maecenas donec duis. Neque semper mattis facilisis alique semper. Mi enim t",
      },
      {
        them: "ursus maecenas donec duis. Neque semper mattis facilisis alique semper. Mi enim t",
      },
      {
        me: "Thank you",
      },
      {
        them: "Thank you 2",
      },
    ],
  },
];

export const subscriptiondetails = [
  // {
  //   id:4,
  //   type: 1,
  //   name: "Staff Strength",
  //   one: "2-5",
  //   two: "6-0",
  //   three: "11-20",
  // },
  // {
  //   id:64,
  //   type: 1,
  //   name: "Assets",
  //   one: "Less than ₦5M",
  //   two: "₦5M - ₦7M",
  //   three: "Above ₦7M - ₦10M",
  // },
  {
    type: 1,
    name: "Annual (1 year)",
    one: "₦300, 000",
    two: "₦400,000",
    three: "₦520,000",
  },
  {
    type: 2,
    name: "Annual (1 year)",
    one: "₦ 2,250,000",
    two: "₦ 3,750,000",
    three: "₦ 5,550,000",
  },
  {
    type: 1,
    name: "Bi-annual (6 months)",
    one: "₦160, 000",
    two: "₦210,000",
    three: "₦270,000",
  },
  {
    type: 2,
    name: "Bi-annual (6 months)",
    one: "₦ 1,325,000",
    two: "₦ 2,075,000",
    three: "₦ 2,975,000",
  },
  {
    type: 1,
    name: "Quarterly (3 months)",
    one: "₦85, 000",
    two: "₦110,000",
    three: "₦140,000",
  },
  {
    type: 1,
    name: "Monthly (1 month)",
    one: "₦30, 000",
    two: "₦40,000",
    three: "₦50,000",
  },
  {
    style: true,
    type: 1,
    name: "",
    one: "",
    two: "",
    three: "",
  },
  {
    style: true,
    type: 1,
    name: "",
    one: "",
    two: "",
    three: "",
  },
  {
    type: 1,
    style: true,
    name: "Legal Services (per month)",
    one: "",
    two: "",
    three: "",
  },
  {
    type: 1,
    style: true,
    name: "Legal advice and opinions on Nigerian laws",
    one: <GrCheckmark size={20} className="fw-bolder" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Legal research",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Drafting simple contracts/Review and other legal documents",
    one: <GrCheckmark size={20} className="fw-0 text-muted" />,
    two: <GrCheckmark size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Attending negotiations (virtual)",
    one: <div className="fw-bolder h5">1</div>,
    two: <div className="fw-bolder h5">2</div>,
    three: <div className="fw-bolder h5">3</div>,
  },
  {
    style: true,
    name: "Free access to online legal documents",
    one: <GrCheckmark size={20} className="fw-0 text-muted" />,
    two: <GrCheckmark size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Due diligence exercise",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Verbal solutions over phone",
    one: <GrCheckmark size={20} className="fw-0 text-muted" />,
    two: <GrCheckmark size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Regular updates on latest developments in the relevant practice area",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Employee Handbook",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Structuring Advice",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <GrCheckmark size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Employee Handbook",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Standard Operating Procedure",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Drafting and reviewing company policies",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <GrCheckmark size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Onsite business visits to discuss legal issues",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <TiTimes size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Annual Legal Business Audit",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <TiTimes size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Filing of Annual Returns with CAC",
    one: <TiTimes size={20} className="fw-0 text-muted" />,
    two: <GrCheckmark size={20} className="fw-0 text-muted" />,
    three: <GrCheckmark size={20} className="fw-0 text-muted" />,
  },
  {
    type: 1,
    style: true,
    name: "Dedicated Lawyers",
    one: <div className="fw-bolder h5">1</div>,
    two: <div className="fw-bolder h5">1</div>,
    three: <div className="fw-bolder h5">1</div>,
  },
];

export const subcriptionPlan = [
  {
    name: "SS Business",
    plan: [
      {
        name: "Annual (1 year)",
        price: "₦300, 000",
      },
      {
        name: "Bi-annual (6 months)",
        price: "₦160, 000",
      },
      {
        name: "Quarterly (3 months)",
        price: "₦85, 000",
      },
      {
        name: "Monthly (1 month)",
        price: "₦30, 000",
      },
    ],
    tota: " ₦300, 000 ",
  },
  {
    name: "SS Business +",
    plan: [
      {
        name: "Annual (1 year)",
        price: "₦400, 000",
      },
      {
        name: "Bi-annual (6 months)",
        price: "₦210, 000",
      },
      {
        name: "Quarterly (3 months)",
        price: "₦110, 000",
      },
      {
        name: "Monthly (1 month)",
        price: "₦40, 000",
      },
    ],
    tota: " ₦400, 000 ",
  },
  {
    name: "SS Business Premium",
    plan: [
      {
        name: "Annual (1 year)",
        price: "₦520, 000",
      },
      {
        name: "Bi-annual (6 months)",
        price: "₦270, 000",
      },
      {
        name: "Quarterly (3 months)",
        price: "₦140, 000",
      },
      {
        name: "Monthly (1 month)",
        price: "₦50, 000",
      },
    ],
    tota: " ₦520, 000 ",
  },
  {
    name: "MS Business +",
    plan: [
      {
        name: "Annual (1 year)",
        price: "₦ 3,750,000",
      },
      {
        name: "Bi-annual (6 months)",
        price: "₦ 2,075,000",
      },
    ],
    tota: " ₦3,750,000 ",
  },
  {
    name: "MS Business",
    plan: [
      {
        name: "Annual (1 year)",
        price: "₦ 2,250,000",
      },
      {
        name: "Bi-annual (6 months)",
        price: "₦ 1,325,000",
      },
    ],
    tota: " ₦ 2,250,000 ",
  },
  {
    name: "MS Business Premium",
    plan: [
      {
        name: "Annual (1 year)",
        price: "₦ 5,550,000",
      },
      {
        name: "Bi-annual (6 months)",
        price: "₦ 2,975,000",
      },
    ],
    tota: " ₦ 5,550,000 ",
  },
];


export const FaqData =[
  {
    id:1,
    title:'Who are We?',
    desc:`<div>
      <p className='lh-base'>PocketLawyers is a technology platform for businesses to access legal services and solutions. We offer Business Formation Services, Post Formation Services, Intellectual Property (Trademark, Patent and Copyright) Registration, Legal Business Structure Advisory, Funding/Fundraising Advisory, Contract and Correspondence Matters, Business Policy Formulation, Statutory/Regulatory Compliance, Staff Onboarding and Outboarding, Growth Advisory, Taxation, Contract Drafting and Review as well as bespoke startup advisory services</p>
    </div>`
  },
  {
    title:'Why PocketLawyers?',
    desc:`<div>
      <p>We understand that many Startups and SMEs are in need of fast, affordable and premium legal services and solutions, however, the challenge of getting one is an herculean task. PocketLawyers is here to provide fast, easy, access to premium legal services and solutions at an affordable rate. Our goal is to help businesses create structure, scale and thrive.</p>
    </div>`
  },
  {
    title:'Where can our services be accessed?',
    desc:`<div><p>Our services can be accessed via our Website, www.pocketlawyers.io.</p></div>`
  },
  {
    title:'Where are you located?',
    desc:`<div><p>We are located at Yaba, Lagos State, Nigeria. Our platform also enables our team members work remotely from anywhere. </p></div>`
  },
  {
    title:'What are your business hours?',
    desc:`<div><p>We are open to serve you from 24/7 via our website, www.pocketlawyers.io. Our office hours are 9am to 5pm, Monday to Friday WAT. We can be reached via email as well on hello@pocketlawyers.io</p></div>`
  },
  {
    title:'Will the information I share be confidential?',
    desc:`<div>
      <p>PocketLawyers will never share your data. In a situation where it is required by a Court of Law to share with any third party, you will be issued a prior notice.</p>
    </div>`
  },
  {
    title:'Do you offer Startup Advisory?',
    desc:`<div>
      <p>We work with innovative companies from ideation to when they are fully up and running. Our pricing packages are also flexible enough to accommodate the startup process. We understand it because we have been there ourselves. We do not see our companies just as clients but also as partners because we are invested in their successes as well. We also ensure that your company is compliant with all the necessary regulations within your sector and also help you get investment ready. We have worked with clients in a variety of industries from Healthcare, Security, Supply Chain, Data, Education, Finance, Logistics, Transportation and much more.</p>
    </div>`
  },
  {
    title:'How do I get started and what’s your process?',
    desc:`<div>
      <p>You have to sign up  on our website, www.pocketlawyers.io. and fill in all details to create your dashboard. If you know exactly the services you need, please go right ahead and make payment, if not, we have a Lawyer whom you can reach to help you clarify your issues and advise you on the best solution, then you go ahead and pay. Once you pay, the system routes you to available Lawyers to provide you with a solution/service. Once you chose one and are connected. Go ahead and chat them up. Please ensure that all transactions are done on the platform, as PocketLawyers will not be liable for any services/solutions offered outside the platform.</p>
      </div>`
  },
  {
    title:'Are there any hidden charges? ',
    desc:`<div>
      <p>At PocketLawyers, we operate a transparent system. This means you will always know the total cost of your legal bill from the start. Please note that we take full payment upfront into our trust account. </p>
      </div>`
  },
  {
    title:'How long will it take to register a business?',
    desc:`<div>
      <p>Company registration from the date payment is received and confirmed will take up to two (2) weeks. The timing is also affected by how quickly clients can give us all the details we request for. This may take longer as CAC is currently experiencing a system upgrade. However, where there is a delay, the client will be informed.</p>
    </div>`
  },
  {
    title:'How long does it take to get third party services done?',
    desc:`<div>
      <p>By 3rd party services, we mean services that we offer that involves other institutions like CAC, the various Tax institutions and so on. The timing varies and is mostly outside our control. We advise clients to ensure that they provide us with all the necessary information we need so as to save time and shorten the turnaround time. It takes 1-4 working for us to complete non-third party services, like contract/letter drafting and review, employee onboarding and so on. The timing is also affected by how quickly clients can give us all the details we request for.</p>
    </div>`
  },
  {
    title:'How long does it take to get non-third party services done?',
    desc:`<div>
      <p className="mb-2">It takes working 1-4 working for us to complete non-third party services, like contract/letter drafting and review, employee onboarding and so on. The timing is also affected by how quickly clients can give us all the details we request for.</p>
    <p>However, if the task is a complex one and will require more time the Client will be pre-informed. We will provide you with an estimated delivery time for your project once we get started, and we’ll keep you updated during your project if there is likely to be any change to this delivery time.</p>
    </div>`
  },
  {
    title:'How can I get more information about my problem or the products before I make payment?',
    desc:`<div>
      <p>You can chat with a Legal Advisor on the platform or you can shoot us an email via hello@pocketlawyers.io and a Legal advisor will be in touch in no time.</p>
      </div>`
  },
  {
    title:'Can I access this service from anywhere in the world?',
    desc:`<div>
      <p>Yes you can. Our website is available to you from anywhere in the world, as far as the legal issue you need solved is within the Nigerian Legal System.</p>
    </div>`
  },
  {
    title:'Is PocketLawyers my Lawyer?',
    desc:`<div>
      <p>PocketLawyers is your Lawyer if you are subscribed to our Retainer Package. </p>
    </div>`
  },
  {
    title:'16.How long do VAT and TIN Registration take?',
    desc:`<div>
      <p>Typically takes 5-10 working days from when we receive all the information we need.</p>
    </div>`
  },
  {
    title:'Pickup and Delivery of Documents',
    desc:`<div>
      <p>As much as possible we like to run a paperless system. However, in the case where we need to pick up and/or deliver documents, the Client will provide the address and cover the cost of the delivery. </p>
    </div>`
  },
  {
    title:'What do I need to register?',
    desc:`<div>
     <div>
     <div class='fw-bold'>A. Company Incorporation</div>
     <ul>
      <li>Your personal details.</li>
      <li>Personal details of the directors of the company.</li>
      <li>Personal details of the shareholders.</li>
      <li>Proof of qualification for professional or consultancy services e. g medical certificate for a doctor or investment advisory licence for an investment advisor.
</li>
      <li>A photocopy of a government issued identity card of the shareholders and directors of the company e.g. international passport, national identity card, or driver’s license
</li>
     </ul>
     </div>

<div>
<div class='fw-bold'>B.Your personal details.</div>
     <ul>
   <li>     Personal details of all other promoters.
</li>
   <li>Your passport photograph.
</li>
   <li>Passport photograph of all other promoters.
</li>
   <li>A photocopy of a government issued identity card of the shareholders and directors of the company e.g. international passport, national identity card, or driver’s license.
</li>
     </ul>
</div>
<div>
<div class='fw-bold'>C.Trademarks Registration</div>
<ul>
  <li>CAC registration documents.
</li>
  <li>Details of trademark owner.
</li>
  <li>Black and white representation of the mark, name or logo to be trademarked in MSWord PNG or JPEG version.
</li>
 
</ul>
</div>
<div>
<div class='fw-bold'>D. VAT & TIN Registration</div>
<ul>
  <li>Original CAC Documents (certificate of incorporation, Form CAC2, Form CAC7, memorandum & articles of association)
</li>
  <li>Copy of utility bill of address of operating office
</li>
  <li>Company stamp.
</li>
</ul>
</div>
    </div>`
  },
  {
    title:'Do you help startups with Investment, Readiness and Due Diligence?',
    desc:`<div>
      <p>We partner with startups to help them get ready for investment and we ensure that the due diligence process is as painless as possible. We take all that stress off our clients.</p>
    </div>`
  },
]

export const ourClientData = [
  '/cashia.svg','/ak.svg','/forcediscount.svg','/firstfounders.svg','/world.svg','/ebenom.svg','/llgm.svg','/gain.svg','/mabo.svg'
]

export const retailSolutionData=[
  {
    icon:'/1.svg',
    title:'Start a company',
    desc:'Registering a business entity is the first step to turning your idea into an actual business, Congratulations on this bold step! Let’s help you start this journey!'
  },
  {
    icon:'/2.svg',
    title:'Post registration matters',
    desc:'Congratulations on getting here, that means you have a thriving business, let’s help you sort out those in-house issues, shall we?!'
  },
  {
    icon:'/3.svg',
    title:'Intellectual Property Protection',
    desc:'Did you know that your intellectual property is part of the assets of your company, let’s help you increase the value of your business'
  },
  {
    icon:'/4.svg',
    title:'Document & Contract Review',
    desc:'Smart move! There is no such thing as a gentleman’s agreement, a handshake is not an Agreement, let’s help you put everything in black and white!'
  },
  {
    icon:'/5.svg',
    title:'Contract & Letter Drafting',
    desc:'Dear Sir/Ma, don’t stress about this, that why we are here, let us handle this for you so you can face other tasks'
  },
  {
    icon:'/6.svg',
    title:'Legal Advisory',
    desc:'Different sectors have different requirements and needs, let’s help take away the headache of complying, so you can face other important things, like getting the deals in'
  },
  {
    icon:'/7.svg',
    title:'Staff onboarding and outboarding',
    desc:'Employing staff, whatever type and terms, without proper documentation, is a recipe for internal combustion and finally, disaster! Let’s help you quell that fire!'
  },
  {
    icon:'/8.svg',
    title:'Business Policy ',
    desc:'Running a business without different policies, is like driving a car without brakes, it’s bound to crash! We are here to help you apply the brakes. '
  },
  {
    icon:'/9.svg',
    title:'Regulatory compliance ',
    desc:'Different sectors have different requirements and needs, let’s help take away the headache of complying, so you can face other important things, like getting the deals in.'
  }
]


export const endPoint = "https://apipocketlawyers.pocketlawyers.io"
export const defaultImage = Avatar