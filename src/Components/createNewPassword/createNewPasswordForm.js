
import VerficationIcon from '../../assest/Image/createpasswordform.svg'
import Image from 'next/image'
import { InputComp } from '../../Components'
import Button from '../../Components/Ui/Button'
export const CreateNewPasswordForm =({handleOnChange,handleSubmit,isLoading})=> {
  return (
    <div>
        <div className='container d-flex justify-content-center mb-5'>
    <div className='col-10 col-lg-8 col-xl-4'>
<div className='text-center mb-3'>
    <Image src={VerficationIcon} alt=''/>
</div>
<div className='text-center mb-3'>
    <h5 className='mb-3'>CREATE NEW PASSWORD</h5>
    <div className='text-type2 mb-5 h10'>Your new password must be different from your
pervious  used password.
password.</div>
</div>
<div className=''>
  <div className='mb-0'>  <InputComp label={"Email"}  typeComp={"email"} name='email' labelSize='17px' bordercolor='#C4C4C4' className={"py-2"} onChange={handleOnChange}/></div>
  <div className='mb-0'>  <InputComp label={"Enter New Password"} icon typeComp={"password"} name='new_password' labelSize='17px' bordercolor='#C4C4C4' className={"py-2"} onChange={handleOnChange}/></div>
  <div className='mb-5'>  <InputComp label={"Re-enter New Password"} icon typeComp={"password"} name='confirm_new_password' labelSize='17px' bordercolor='#C4C4C4' className={"py-2"} onChange={handleOnChange}/></div>
  <div className='mt-5 mb-3'><Button bntText={isLoading ?"Loading...":'Reset password'} onClick={handleSubmit} className='bg-type1 text-white h5 fw-bold py-3  w-100 px-3 rounded'/></div>
 
 
</div>
</div>
</div>
    </div>
  )
}
