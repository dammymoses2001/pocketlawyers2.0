import  { useEffect, useState } from "react";
import { LayoutAuth } from "../../../../Layout";
import {
  CardData,
  retailPackage1,
  selectedPlan,
  subcriptionPlan,
  subscriptionSideBar,
} from "../../../../utils";
import Image from "next/image";
import { MdOutlineArrowForwardIos } from "react-icons/md";
import { BsThreeDotsVertical } from "react-icons/bs";
import Button from "../../../../Components/Ui/Button";
import AddCardIcon from "../../../../assest/Image/addCardIcon.svg";
import MoneyIcon from "../../../../assest/Image/moneyicon.svg";
import MoneyIcon1 from "../../../../assest/Image/moneyicon1.svg";
import {
  CheckBoxx,
  InputComp,
  InputComp2,
  RadioComp2,
  RadioComp4,
  RadioComp5,
  RetailPackage,
} from "../../../../Components";
import { useAuth } from "../../../../hooks/useContext";

export default function Index() {
  const [selectMenu, setSelectMenu] = useState(0);
  const [cardToggle, setCardToggle] = useState(0);
  const { state,subscribeRetainerPackageFunc,getClientSubServiceFunc,userProfile } = useAuth();
  const [subscriptionToggle, setSubscriptionToggle] = useState(0);
  const [plan, setPlan] = useState([]);

  //console.log(state?.client_Sub_Service, "statesub");
  useEffect(() => {
    getClientSubServiceFunc()
  }, [getClientSubServiceFunc]);

  useEffect(() => {
    if(!state?.client_Sub_Service){
      setSubscriptionToggle(1)
    }
  }, [state?.client_Sub_Service]);

  const sideBar = (index,item) => {
    return (
      <div key={index}>
      <div
        className="py-3 px-3 rounded bg-white mb-3 d-flex align-items-center justify-content-between pointer"
        onClick={() => setSelectMenu(index)}
      >
        <div className="d-flex align-items-cente">
          <div className="me-3">
            <Image
              src={selectMenu === index ? item?.icon : item?.icon1}
              alt=""
            />
          </div>
          <h6
            className={`${selectMenu === index ? "text-type1" : ""}`}
          >
            {item?.name}
          </h6>
        </div>
        <div className={selectMenu === index ? "text-type1" : ""}>
          <MdOutlineArrowForwardIos size={20} />
        </div>
      </div>
    </div>
    )
  }
  const getDetails = selectedPlan.find((item)=>item?.name ===state?.client_Sub_Service?.name)
  //  console.log(getDetails,selectedPlan,state?.client_Sub_Service?.name,'state?.client_Sub_Service?.name')

  const showCurrentSubscriptionside = (state)=>{
    return(
      <div className=" py-5" >
        {/* {console.log(state?.client_Sub_Service)} */}
      <div>
        <h5 className="mb-2 fw-2 px-3 px-lg-5">
          <span className="me-3">Subcription Plan:</span>{" "}
          <span className="fw-0">
            {state?.client_Sub_Service?.name?state?.client_Sub_Service?.name:'No Subscription Yet'}
          </span>
        </h5>{" "}
        <hr />
      </div>
      <div>
        <h5 className="py-3 fw-1 px-3 px-lg-5">
          <span className="">Status:</span>{" "}
          <span className="fw-0">{state?.client_Sub_Service?.payment_status ==="payment_received"?'Active':'Not Active'}</span>
        </h5>{" "}
        <hr />
      </div>
      <div className="mb-3">
        <h5 className="px-3 px-lg-5 mb-5">Summary</h5>
        <div className="col-lg-8 px-3 px-lg-5">
          <table className="table table table-bordered">
            <tbody className="">
              {getDetails?.Summary?.map((item, index) => (
                
                  <tr key={index}>
                    <td className="text-start h7">
                      {item?.name}
                    </td>
                    <td className=" h7">{item?.assets}</td>
                  </tr>
                
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <div className="mb-5">
        <h6 className="px-3 px-lg-5 fw-2 mb-3">
          Legal Services (per month)
        </h6>
        <div className="col-lg-8 px-3 px-lg-5">
          <table className="table table table-bordered">
            <tbody className="">
              {/* {console.log('hello',getDetails?.legalService)} */}
              {getDetails?.legalService?.map((item, index) => (
                
                  <tr key={index}>
                    <td className="text-start h7">
                      {item?.legal}
                    </td>
                   {item?.available ?
                  <td colSpan={2} className=" h7">
                  {item?.available && "/"}
                </td>
                :
                <td colSpan={2} className=" h7">
                      {item?.no ? item?.no : "/"}
                    </td> 
                  } 
                  </tr>
                
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <div className="px-5">
        <Button
          className={
            "py-3 px-5 h5 fw-bolder bg-type1 text-white"
          }
          bntText={state?.client_Sub_Service?.name ?"Change Plan":'Subscribe Now'}
          onClick={() => setSubscriptionToggle(1)}
        />
      </div>
    </div>
    )
  }

  const ListPlan = subcriptionPlan.find((item) => item?.name == plan);
 //console.log(ListPlan,'sub')
  return (
    <LayoutAuth Title={"Subcription"}>
      <div className="mb-3"></div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-3">
            <h5 className="fw-2 mb-5">Subscription</h5>
            {subscriptionSideBar.map((item, index) => (
              sideBar(index,item)
            ))}
          </div>
          <div className="col-lg-9">
            {selectMenu === 0 ? (
              <div className="bg-white">
                {subscriptionToggle === 0 ? (
                  <div>
                    {/* {selectedPlan.map((item, index) => ( */}
                     {showCurrentSubscriptionside(state)}
                    {/* ))} */}
                  </div>
                ) : subscriptionToggle === 1 ? (
                  <div>
                    <RetailPackage
                      retailPackage1={retailPackage1}
                      off
                      setSubscriptionToggle={setSubscriptionToggle}
                      setPlan={setPlan}
                    />
                    <div className="px-5 text-center pb-5">
                      <Button
                        className={"py-3 px-5 h5 fw-bolder bg-type1 text-white"}
                        bntText={"Back"}
                        onClick={() => setSubscriptionToggle(0)}
                      />
                    </div>
                  </div>
                ) : (
                  <div>
                    <div className="pt-7 pb-5 d-flex justify-content-end font-1 px-3">
                      <div className="col-12 col-lg-10 ">
                      

                        <div>
                          <div>
                            <h5 className="fw-bold">{ListPlan?.name}</h5>
                            <p>Select subcription period</p>
                          </div>
                          <div>
                            <table className="">
    
                             
                             <tbody>
                                {ListPlan?.plan?.map((item, index) => (
                                  <tr key={index}>
                                    <td className="px-3 px-lg-5 py-1 bg-6 text-end fw-2">
                                      <h6>{item?.name}</h6>
                                    </td>
                                    <td className="ps-3 py-1 ">
                                      {" "}
                                      <RadioComp5
                                        className={"fw-2"}
                                        label={item?.price}
                                        labelClassName='text-end fw-2'
                                        
                                      />
                                    </td>
                                  </tr>
                                ))}
                                <tr >
                                    <td className="">
                                   
                                    </td>
                                    <td className="ps-3  pt-3 ">
                                    <h6 className="fw-0">Bill:  <span className="text-type1 fw-2">{ListPlan?.tota}</span></h6>
                                    </td>
                                  </tr>
                              </tbody> 
                            </table>

                 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div  className="d-flex justify-content-start">
                    <div className="bg-white container position-relative col-12 col-md-8">
         
         <div className="d-flex align-items-center mb-4 px-md-3 ">
           {/* <span
             onClick={() => setSwitchFromPackageToPay(0)}
             className="pointer"
           >
             <MdOutlineArrowBackIosNew size={30} className="text-type1" />
           </span> */}
           <div className="w-100 text-center">
             <h5 className="text-type2 fw-1 mb-0 mt-5">
               Select Payment method
             </h5>
           </div>
         </div>

         <div className="pb-5  position-relative">
           <div className="row justify-content-start">
             <div className="col-12  mb-4 ">
               <div>
                 <div className="d-flex align-items-center rounded w-100 bg-type3 py-3 px-3">
                   <span className="me-2">
                     {" "}
                     <Image src={MoneyIcon} alt="" />
                   </span>
                   <div className="w-100">
                     {" "}
                     <RadioComp2
                       className={"h5 fw-2"}
                       label={"Bank Transfer"}
                       id="payment"
                     />
                   </div>
                 </div>
               </div>
             </div>

             <div className="col-12  mb-7 ">
               <div>
                 <div className="d-flex align-items-center rounded w-100 bg-type3 p-3">
                   <span className="me-2">
                     {" "}
                     <Image src={MoneyIcon1} alt="" />
                   </span>
                   <div className="w-100">
                     {" "}
                     <RadioComp2
                       className={"h5 fw-2"}
                       label={"Credit/Debit Card"}
                       id="payment"
                     />
                   </div>
                 </div>
               </div>
             </div>
             <div className="col-12  text-center">
               <div className="text-center">
                 <Button
                 loading={state?.isLoading}
                   bntText={"Proceed"}
                   onClick={() => {
                    //  //console.log(retailerServiceData, "retailerServiceData");
                     subscribeRetainerPackageFunc(4)
                    //  subscribeRetainerPackageFunc(retailerServiceData?.id);
                     //setSwitchFromPackageToPay(3)
                   }}
                   className="h5 bg-type1 w-100 py-3 fw-bold rounded text-white"
                 />
                 <Button
               
                   bntText={"Cancel"}
                   onClick={() => setSubscriptionToggle(1)}
                   className="h5 bg-7 w-100 py-3 fw-bold rounded text-black"
                 />
               </div>
             </div>
           </div>
         </div>
       </div>
                    </div>
                    
                  </div>
                )}
              </div>
            ) : (
              <div>
                {cardToggle === 0 ? (
                  <div
                    className="bg-white  py-5 px-3 px-lg-5 d-flex flex-column justify-content-between"
                    style={{ height: "100vh" }}
                  >
                    <div>
                      <h5 className="fw-bold mb-3">Saved card</h5>
                      {CardData.map((item, index) => (
                        <div
                          key={index}
                          className="bg-6 py-4 px-3 d-flex justify-content-between  align-items-center"
                        >
                          <div className="d-flex  align-items-center">
                            <div className="me-4">
                              {" "}
                              <Image
                                src={item?.icon}
                                width={120}
                                height={40}
                                alt=""
                              />
                            </div>
                            <div>
                              <h4 className="fw-2 mb-0">Visa {item?.id}</h4>
                              <h5 className="text-muted">
                                {" "}
                                Expires {item?.expires}
                              </h5>
                            </div>
                          </div>
                          <div>
                            <BsThreeDotsVertical size={30} />
                          </div>
                        </div>
                      ))}
                    </div>
                    <div>
                      <Button
                        bntText={"Add Card"}
                        onClick={() => setCardToggle(1)}
                        className="text-white h5 bg-type1 px-5 rounded py-3 fw-bold"
                      />
                    </div>
                  </div>
                ) : (
                  <div
                    className="bg-white  py-5 px-3 px-lg-5 d-flex flex-column justify-content-between"
                    style={{ height: "100vh" }}
                  >
                    <div>
                      <h5 className="fw-bold mb-3">Add card</h5>
                      <div>
                        <div className="text-center">
                          <Image src={AddCardIcon} alt="" />
                        </div>
                      </div>
                      <div className="d-flex justify-content-center">
                        <div className="col-8">
                          <div className="col-12">
                            <InputComp2
                              placeholder="Card holder name"
                              bg={"#F2F2F2"}
                            />
                          </div>
                          <div className="col-12">
                            <InputComp2
                              placeholder="14 digit card number"
                              bg={"#F2F2F2"}
                            />
                          </div>
                          <div className="row">
                            <div className="col-8">
                              <InputComp2
                                placeholder={"Expiration Date"}
                                bg={"#F2F2F2"}
                              />
                            </div>
                            <div className="col-4">
                              <InputComp2
                                placeholder="CCV"
                                typeComp={"text"}
                                bg={"#F2F2F2"}
                              />
                            </div>
                            <div className="text-center">
                              <Button
                                bntText={"Save Card"}
                                onClick={() => setCardToggle(1)}
                                className="text-white h5 bg-type1 px-5 rounded py-3 fw-bold"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </LayoutAuth>
  );
}
