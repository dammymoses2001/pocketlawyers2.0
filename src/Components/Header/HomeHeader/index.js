
import styled from "styled-components";
import Link from 'next/link'
import Button from "../../Ui/Button";
import {HiArrowRight} from 'react-icons/hi'
import Image from "next/image";
import { useRouter } from "next/router";

const Style = styled.div`
  min-height: 100vh;
  // background: url("./Pic2.png");
  // background-size: cover;
   background-color: #fff;

  @media (max-width: 540px) {
    min-height: 80vh;
  }
`;
export const HomeHeader = () => {
  const router = useRouter()
  return (
    <Style>
      <div className=" container h-100">
        <div className="row align-items-center">
          <div className="col-lg-7 mb-5" data-aos="fade-right">
            <div className='py-3 py-lg-5 my-5 '></div>
            <h1 className="col-lg-10 text-center text-lg-start font-4 ">Lawyers in your <span className="text-9">Pocket</span></h1>
            <h5 className="text-justify col-lg-11 fw-0  mb-5 font-1">
            PocketLawyers is Africa’s first fully integrative legal tech startup that offers access to affordable premium legal services and solutions to SMEs and Startups. We offer reliable, cost-effective solutions that give peace of mind and help scale your business.
            </h5>
            <Button onClick={()=>router.push('/login')} bntText={<span className="fw-bold mb-0 d-flex font-1 h6">Get Started <span className=" mb-0 ms-3 animate__animated  animate__infinite	infinite animate__headShake"><HiArrowRight/></span></span>} className={"text-white fw-bold bg-type1 px-4 py-3"} data-aos="fade-down-right"/>
          </div>
          <div className="col-lg-5  mb-3" data-aos="fade-left">
            <div className="py-3"></div>
            <div>
            <div className="text-center px-5">
           <picture>
           <source srcSet={"/justic.png"} type="image/png" />
           <img src={"/justic.png"}  alt=''  className="w-100" layout=""/>
            </picture>
            </div>
            </div>
          </div>
        </div>
        <div className="py-3 py-lg-4"></div>
      </div>
    </Style>
  );
};
