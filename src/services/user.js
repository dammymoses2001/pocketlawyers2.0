import { getAuthToken, request } from "../utils";

export const loginUser = async (value) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.post("/auth/signin", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);
    
    const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;
    
    throw new Error(err);
  }
};

export const getUserProfile = async () => {
  try {
      const res = await request.get("/get_user_profile");
      // //console.log(res, "product user");
      return res?.data;
  } catch (error) {
      const err = error?.response?.data?.message || error?.message;
      throw new Error(err);
  }
};

export const updateUserProfile = async (value) => {
  try {
      const res = await request.post("/profile_update",value);
      // //console.log(res, "product user");
      return res?.data;
  } catch (error) {
      const err = error?.response?.data?.message || error?.message;
      throw new Error(err);
  }
};

export const RegisterClient = async (value) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.post("/auth/client_signup", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);
    
    const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;
    
    throw new Error(err);
  }
};
export const RegisterLawyer = async (value) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.post("/auth/lawyer_signup", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);
    
    const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;
    
    throw new Error(err);
  }
};

//to get the details of the user having conversation with
export const getOtherUserProfile = async (id) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.get(`/get_user_profile/${id}`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);
    
    const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;
    
    throw new Error(err);
  }
};



export const subscribeNewLetter = async (email) => {
  try {
    const res = await request.post(`/newsletter`,email);
    return res?.data;
  } catch (error) {
    console.log(error.response);
    
    const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;
    
    throw new Error(err);
  }
};


export const contactUs = async (value) => {
  try {
    const res = await request.post(`/newsletter`,value);
    return res?.data;
  } catch (error) {
    console.log(error.response);
    
    const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;
    
    throw new Error(err);
  }
};

