import styled from "styled-components";
import Logo from "../../../assest/Image/loginImage.svg";
import LawyerIcon from "../../../assest/Image/lawyericon.svg";
import Image from "next/image";
import { useRouter } from 'next/router'
import Button from "../../Ui/Button";
import Link from "next/link";
import { useState } from "react";

//
import SmeIcon from "../../../assest/Image/smeicon.svg";

const Style = styled.section`
  padding: 5rem 0 15rem 0;
  .form-width {
    width: 40%;
  }
  @media (max-width: 992px) {
    .form-width {
      width: 60% !important;
    }
  }
  @media (max-width: 786px) {
    .form-width {
      width: 80% !important;
    }
    h3 {
      font-size: 26px !important;
    }
  }
  @media (max-width: 540px) h3 {
    font-size: 26px !important;
  }
`;
export const ProfileForm = ({ setSignupLevel }) => {
  const [toggle,setToggle] =useState("")
  const router = useRouter()


  const handleChoose = (toogleid) =>{
    if(toggle===1){
      router.push('/signup/client_signup')
    }
    else{
      router.push('/signup/lawyer_signup')
    }

  }
  return (
    <Style>
      <div className="container  text-black">
        <h5 className="fw-2 mb-4 text-center">Sign Up To</h5>
        <div className="text-center mb-5 px-5 px-md-0">
          <Image src={Logo} alt="" />
        </div>
        <div className="d-flex justify-content-center">
          <div className="form-width">
            <div className="">
              <h6 className="fw-bold mb-5">SELECT PROFILE</h6>
            </div>
           
            <div className={`py-4 ps-3 ps-md-5 d-flex flex-wrap align-items-center pointer ${toggle ===1?'bg-type2 text-white' :'bg-5'}  mb-5`}
             onClick={()=>setToggle(1)}
            >
              <span className="me-2 me-md-4">
                <Image src={SmeIcon} alt="" />
              </span>
              <h4 className="  fw-2">
                SME/STARTUP
              </h4>
            </div>
           
         
            <div className={`py-4 ps-3 ps-md-5 pointer d-flex flex-wrap align-items-center ${toggle ===2?'bg-type2 text-white' :'bg-5'}  mb-5`}
            onClick={()=>setToggle(2)}
            >
              <span className="me-2 me-md-4 ">
                <Image src={LawyerIcon} alt="" />
              </span>
              <h4 className="fw-2 ">
                LAWYER
              </h4>
            </div>
           
            <div className="text-center">
              <Button
              disabled={toggle?false:true}
                bntText={"Next"}
                className={`w-100 py-3 h4 fw-bolder ${!toggle?'bg-type11':'bg-type1'}  text-white mb-3`}
                onClick={() => handleChoose(toggle)}
              />
              <h6>
                I have an account yet,{" "}
                <Link href="/login">
                  <a className="text-type1 fw-1">Sign In?</a>
                </Link>
              </h6>
            </div>
          </div>
        </div>
      </div>
    </Style>
  );
};
