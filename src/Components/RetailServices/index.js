/* eslint-disable @next/next/no-img-element */
import { Card } from "react-bootstrap"
import styled from "styled-components"
import { RetailCard1 } from "../RetailCard"
import Link from "next/link"
import { retailSolutionData } from "../../utils"

const Styled = styled.section`

`

export const RetailServices = ({retailserviceData2})=> {
  return (
    <Styled>
        <div className="row justify-content-center">
        <div className="col-10 col-lg-7">
        <div className="py-5 py-lg-1 my-3"></div>
            <div className="mb-5">
            <h5 className=" fw-1 ">What we Offer</h5>
            <h3 className="h3 font-4 text-type1">Retail Solutions</h3>
            </div>
            
           <div className="row">
            {retailSolutionData.map((item,index)=>
             <div key={index} className="col-lg-6 col-xl-4 mb-4" data-aos="zoom-out-down">
             <div className="bg-13 p-3 h-100" style={{borderRadius:'15px'}}>
               <div className=" mb-3" style={{width:'48px'}}><img src={item?.icon} className="w-100"/></div>
               <div className="h51 font-4 text-7 mb-3">{item?.title}</div>
               <div className="h7 text-justify text-capitalize">{item?.desc}</div>
               <div className="py-3"></div>
             </div>
           
           </div>
            )}
           

           </div>
        </div>
        </div>
    </Styled>
  )
}
