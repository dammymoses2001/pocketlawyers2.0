/* eslint-disable @next/next/no-img-element */
import Image from "next/image";
import React from "react";
import styled from "styled-components";
import Pic3 from "../../../public/Pic3.svg";

const Style = styled.div`
  background: ${props=>props.blockBg ? '':'#eef1f1'};
  overflow: hidden;

  h2 {
    font-weight: 800 !important;
  }
  h5 {
    font-weight: 700;
  }
  h6 {
    text-align: justify;
    font-weight: 500;
    line-height: 30px;
  }
  img {
    height: 100%;
  }

  @media (max-width: 991px) {
    img {
      // width: 60% !important;
    }
  }
  @media (max-width: 540px) {
    img {
      width: 100% !important;
    }
  }
`;

export const TwoSide = ({ switchSide, sideOne, blockBg, sideTwo, displayImage }) => {
  return (
    <Style blockBg={blockBg}>
      <div className="px-2  ">
        <div className="row g-3">
          <div
            className={`col-lg-6 space  ps-6  ${
              switchSide ? "order-0" : "order-1"
            } `}
          >
            {sideOne}
          </div>
          <div className={`col-lg-6  ${switchSide ? "order-1" : "order-0"} `}>
            <div className="h-100 d-flex justify-content-center">
              <Image src={displayImage} alt=""  blurDataURL="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.explicit.bing.net%2Fth%3Fid%3DOIP.6oalPwIHHy8Nm_hgM4_AvgHaEV%26pid%3DApi&f=1"/>
            </div>
          </div>
        </div>
      </div>
    </Style>
  );
};
