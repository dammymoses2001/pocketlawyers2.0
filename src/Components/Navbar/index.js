/* eslint-disable @next/next/no-img-element */

import Image from "next/image";
import React from "react";
import { Dropdown, Nav, Navbar, NavDropdown } from "react-bootstrap";
import Logo from "../../../public/Logo1.svg";
import styled from "styled-components";
import Link from "next/link";
import {MdOutlineKeyboardArrowDown} from 'react-icons/md'
import { HiMenuAlt2 } from "react-icons/hi";
import { AiOutlineMenu } from "react-icons/ai";

const Style = styled.div`
  .nav-link {
    font-weight: 700;
    color: black;
    font-size: 16px;
  }
  .dropdown-item{
    font-weight: 700;
    color: #218698 !important;
    margin-bottom:10px;
  }
  .dropdown-menu{
    background:#EAF8FA;
    border: none;
  }
  .bg-light {
    background: white !important;
  }
  .container-fluid {
    padding: 10px 5rem !important;
  }
  @media (max-width: 540px) {
    .container-fluid {
      padding: 8px 1rem !important;
    }
  }
`;

export const NavBar = () => {
  return (
    <Style>
     <div className="">
     <Navbar collapseOnSelect expand="xl" bg="light" variant="light" fixed="top" className="">
        <div className="container-fluid ">
          <Link href="/">
            <a className="navbar-brand">
              {" "}
              <Image src={Logo} alt="" />
            </a>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto">
            {/* text-center dropdown-item */}
            <NavDropdown className="text-7  me-4 text-uppercase border-0" title={<span className="">Company <span><MdOutlineKeyboardArrowDown size={30}/></span></span>} id="basic-nav-dropdown">
            <Link  href="/about" className="me-4  text-7 ">
              <a className="dropdown-item me-4  text-7  text-uppercase">ABOUT US</a>
              </Link>
            
              <Link  href="/our-story" className="me-4  text-7 ">
              <a className="dropdown-item me-4  text-7  text-uppercase">   OUR STORY</a>
              </Link>
              <Link  href="/team" className="me-4  text-7 ">
              <a className="dropdown-item me-4  text-7  text-uppercase">OUR TEAM</a>
              </Link>
              {/* <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item> */}
            </NavDropdown>
              <Link href={"/retailservices"}>
                <a className="nav-link me-4  text-7  text-uppercase">Services</a>
              </Link>

              <Link  href="/retailpackages" className="me-4  text-7 ">
              <a className="nav-link me-4  text-7  text-uppercase">pricing</a>
              </Link>
              <NavDropdown className="text-7  me-4 text-uppercase" title={<span>Explore <span><MdOutlineKeyboardArrowDown size={30}/></span></span>} id="basic-nav-dropdown">
              <Link  href="/team" className="me-4  text-7 ">
              <a className="dropdown-item me-4  text-7  text-uppercase">Free templates</a>
              </Link>
              <Link  href="/team" className="me-4  text-7 ">
              <a className="dropdown-item me-4  text-7  text-uppercase">blog</a>
              </Link>
              <Link  href="/press" className="me-4  text-7 ">
              <a className="dropdown-item me-4  text-7  text-uppercase">media</a>
              </Link>
             
             
            </NavDropdown>
          
              <Link href={"/faq"}>
                <a className="nav-link me-4  text-7  text-uppercase">FAQ</a>
              </Link>
              <Link href={"/contact"}>
                <a className="nav-link me-4 text-7  text-uppercase">Contact</a>
              </Link>

              <Link href={"/login"}>
                <a className="nav-link btn bg-type1 fw-bold text-white px-4 h3">
                  {" "}
                  LOG IN
                </a>
              </Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
     </div>
     <div className="d-none">
      <div className="container py-3">
      <div className="d-flex justify-content-between">
      <Link href="/">
            <a className="navbar-brand">
              {" "}
              <Image src={Logo} alt="" />
            </a>
          </Link>
          <div>
          <Dropdown>
      <Dropdown.Toggle variant="" id="dropdown-basic">
      <span className=" p-2"><AiOutlineMenu size={30}/></span>
      </Dropdown.Toggle>

      <Dropdown.Menu>
      <Link href={"/about"}>
                <a className="dropdown-item btn  fw-bold text-white px-4 h3">
                  {" "}
                  About Us
                </a>
              </Link>
              <Link href={"/our-story"}>
                <a className="dropdown-item btn  fw-bold text-white px-4 h3">
                  {" "}
                  Our Story
                </a>
              </Link>
              <Link href={"/team"}>
                <a className="dropdown-item btn  fw-bold text-white px-4 h3">
                  {" "}
                  Our Team
                </a>
              </Link>
              <Link href={"/retailservices"}>
                <a className="dropdown-item btn  fw-bold text-white px-4 h3">
                  {" "}
                  Services
                </a>
              </Link>
              <Dropdown.Divider className="mx-3 mb-3" />
              <Link href={"/login"}>
              <a className="nav-link btn bg-type1 fw-bold text-white px-4 h3 mx-3">
                  {" "}
                  LOG IN
                </a>
                </Link>
      </Dropdown.Menu>
    </Dropdown>
          
          </div>
      </div>
      </div>
     </div>
    </Style>
  );
};
