import React from 'react'
import Image from 'next/image'
import styledComponents from 'styled-components'
import WelcomeImage from '../../assest/Image/Welcome.svg'
import Button from '../../Components/Ui/Button'
import {MdArrowForwardIos} from 'react-icons/md'
import { useRouter } from 'next/router'

const Style = styledComponents.div`
height:100vh;
display: flex;
justify-content: center;
align-items: center;
text-align:center;
.form-width{
    width:40%;
}
@media (max-width: 992px) {
    .form-width{
        width:60% !important;
    }
}
@media (max-width: 786px) {
    .form-width{
        width:80% !important;
    }
}
`
export default function Index() {
    const router = useRouter()
  return (
    <Style>
       <div className='d-flex justify-content-center align-items-center container'>
           <div className='col-lg-10'>
           <div>
            <Image src={WelcomeImage} alt=''/>
        </div>
        <h2 className='mb-3'>WELCOME TO POCKET LAWYERS</h2>
        {/* <h6 className='mb-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h6> */}
        <div>
            <Button bntText={'GO TO HOME'} className='h3 w-100 py-4 px-10 bg-type1 text-white fw-bold d-flex align-items-center justify-content-center' onClick={()=> router.push('/verify-page')} icon={<MdArrowForwardIos/>}/>
        </div>
           </div>
       </div>
    </Style>
  )
}
