/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import { GeneralUrl, request } from "../../utils";
import { GoPrimitiveDot } from "react-icons/go";

export const ListUsers = ({
  message,
  conversation,
  userProfile,
  getOtherUserProfile,
  setGetSelectedMessages,
  otheruser,
  setMessage,
  activeUser,
}) => {
  const [user, setUser] = useState();

  useEffect(() => {
    if (JSON.parse(conversation?.members)[1]) {
      const getOtherUser = async () => {
        // //console.log( "stttststsst");
        try {
          const res = await request.get(
            `/get_user_profile/${JSON.parse(conversation?.members)[1]}`
          );

          setUser(res?.data?.user);
        } catch (error) {
          //console.log(error.response);

          const err = error?.response?.data?.errors
            ? error?.response?.data?.errors[0]
            : error?.response?.data?.message;
        }
      };
      getOtherUser();
    }
  }, [conversation, otheruser]);

  return (
    <div>
      <div
        className="px-3 py-3 bg-white rounded mb-3 pointer"
        onClick={() => {
          setMessage({ user, conversation });
          setGetSelectedMessages(conversation?.messages);
        }}
      >
        <div className="d-flex">
          <div className="imageWrapperMessage me-3">
            {/* {console.log(user,'hello')} */}
            <img
              src={
                user?.pic_name ?(GeneralUrl+user?.pic_name): "https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg"              }
              alt=""
            />
          </div>
          <div className="">
            <div className="h6 fw-bold">
              {user?.first_name} {user?.last_name}{" "}
              {activeUser?.map((item, index) =>
                item?.userId === user?.id ? (
                  <GoPrimitiveDot key={index} color="green" size={20} />
                ) : null
              )}
            </div>
            <div className="h6 fw-0">
              {conversation?.serviceName.substring(0, 10) + "..."}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
