import { useEffect } from "react";
import { useState } from "react";
import styled from "styled-components";
import { useAuth } from "../../hooks/useContext";
import { InputComp, TextAreaComp } from "../Ui";
import Button from "../Ui/Button";

const Style = styled.section`
  h5 {
    font-size: 20px !important;
  }
  @media (max-width: 540px) {
    h5 {
      font-size: 16px !important;
    }
  }
`;

export const ContactForm = () => {
  const { ContactUs,state:{isLoading,defaultMessage} } = useAuth();
  const initialState = {
    first_name: "",
    last_name: "",
    email: "",
    phone: "",
    message: "",
  };
  const [ userData, setUserData ] = useState(initialState);
  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setUserData({...userData,[name]:value})
  };
  const handleSubmit = (e) => {
    e.preventDefault()
    ContactUs(userData)
  }
  
  useEffect(() => {
    if(defaultMessage){
      setUserData(initialState)
    }
  }, [defaultMessage])
  
  
   const {first_name,last_name,email,phone,message}=userData;
  return (
    <Style>
      <div className="container">
      <div className="row justify-content-center">
        <div className="py-5 py-lg-5"></div>
      <div className="text-type1 col-10 col-lg-6">
        <h3 className="fw-2 font-4">Contact Us</h3>
      </div>
      </div>
       <div className="row justify-content-center">
       
       <div className="col-10 col-lg-7 bg-13 p-4  mb-5" style={{borderRadius:'10px'}}>
        <form className="">
                <div className="row">
                  <div className="col-lg-6">
                    <InputComp label={"First Name"} required placeholder='enter first name' labelClassName={"h7 font-6"}  name="first_name" onChange={handleOnChange} value={first_name}/>
                  </div>
                  <div className="col-lg-6">
                    <InputComp label={"Last Name"} required placeholder='enter last name' labelClassName={"h7 font-6"}  name="last_name" onChange={handleOnChange} value={last_name}/>
                  </div>
                </div>
                <div>
                  <InputComp label={"Email"} required placeholder='enter email' labelClassName={"h7 font-6"}  name="email" onChange={handleOnChange} value={email}/>
                </div>
                <div>
                  <InputComp label={"Phone Number"} required placeholder='+ 234' labelClassName={"h7 font-6"}  name="phone" onChange={handleOnChange} value={phone}/>
                </div>
                <div>
                  <TextAreaComp required label={"Send your Message"} placeholder='enter message' labelClassName={"h7 font-6"}  rows="5" name="message" onChange={handleOnChange} value={message} />
                </div>
                <div className="text-end">
                  <Button
                  onClick={handleSubmit}
                    bntText={isLoading?"Loading...":"SUBMIT"}
                    className="py-2 px-5 fw-bold bg-type1 h5 text-white font-3"
                  />
                </div>
              </form>
        </div>
        <div className="col-10 col-lg-6">
          <div className="bg-15 p-4" style={{borderRadius:'10px'}}>
            <h5 className="text-uppercase font-4 text-type1">get in touch</h5>
            <div className="font-3">
              <h5>www.pocketlawyers.io

</h5>
<h5>
hello@pocketlawyers.io 
</h5>
<h5>
+234 916 803 6060
</h5>
<h5>
Suite 1, 319, Borno Way off Herbert Macaulay Way, Yaba, Lagos, Nigeria

</h5>
            </div>
          </div>
        </div>
       </div>
      </div>
    </Style>
  );
};
