import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { LayoutAuth, LayoutAuthLawyer } from "../../../Layout";
import HomeTopIcon from "../../../assest/Image/homeTop.svg";

import { Card, Form } from "react-bootstrap";
import LawyerServiceIcon from "../../../assest/Image/lawyerserviceicon.svg";
import LawyerReciptIcon from "../../../assest/Image/lawyerrecipt.svg";
import LawyerShedularIcon from "../../../assest/Image/lawyerscheduler.svg";
import BackgroundImage from "../../../assest/Image/dasboardBack.png";
import CaseIcon from "../../../assest/Image/caseIcon.svg";
import { HiPlusCircle } from "react-icons/hi";
import { io } from "socket.io-client";
import AddCardIcon from "../../../assest/Image/addCardIcon.svg";
import moment from "moment";
import Empty from "../../../assest/Image/empty.svg";

import {
  InputComp2,
  LawyerReceiptComp,
  LawyerServiceComp,
  LawyerShedularComp,
  Loader,
  SelectComp,
  SwitchButton,
  UpcomingEvent,
} from "../../../Components";
import {
  dashBoardServices,
  GeneralUrl,
  handleRetailServices,
  lawyerHomeData,
  LawyerSchedularData,
  ReceiptData,
  recentServicesData,
  retailPackage2,
} from "../../../utils";
import { useAuth } from "../../../hooks/useContext";
import { ModalComp } from "../../../Components/Modal";
import { MdOutlineArrowBackIosNew } from "react-icons/md";
import Button from "../../../Components/Ui/Button";
import styledComponents from "styled-components";

export default function Index() {
  const [requesting, setRequesting] = useState(false);
  const [value, onChange] = useState(new Date());
  const [lawyersTool, setLawyerTools] = useState("");
  const [conversations, setConversations] = useState([]);
  const [lawyerAvailablity, setLawyerAvailablity] = useState(false);


  
  const { userProfile, state, getallConversationFunc, LawyerAvailablityFunc,GetNewsFeeds } =
    useAuth();

    useEffect(() => {
      GetNewsFeeds()
    }, [GetNewsFeeds])
  const socket = useRef();

  useEffect(() => {
    socket.current = io(GeneralUrl);
  }, []);

  useEffect(() => {
    socket?.current?.emit("addUser", userProfile?.user?.id);
    // socket.current.on("getOnlineUsers", (users) => {
    //   //console.log(users, "");
    // });
  }, [userProfile?.user?.id]);

  useEffect(() => {
    setLawyerAvailablity(userProfile?.user?.isLawyerAvailable);
  }, [userProfile?.user?.isLawyerAvailable]);
  //console.log(userProfile?.user?.lawyerBankAccountDetail?.id,'userProfile')
  useEffect(() => {
    // window.location.href = "/login";
    // clean up controller
    let isSubscribed = true;

    const GetallConversation = async () => {
      // //console.log('state' ,'find')
      getallConversationFunc(userProfile?.user?.id);
    };

    GetallConversation();
    // cancel subscription to useEffect
    return () => (isSubscribed = false);
  }, [getallConversationFunc, userProfile?.user?.id]);

  useEffect(() => {
    setConversations(state?.conversations);
  }, [state?.conversations]);

  

  const HandleLawyerServices = (id, conversations) => {
    return (
      <div className="position-relative px-3">
        <div className="back">
          <Image src={BackgroundImage} alt="" />
        </div>
        <hr />
        <div className="bg-white py-4 px-md-4 ">
          <div className="mb-5">
            <span onClick={() => setLawyerTools(0)}>
              <MdOutlineArrowBackIosNew
                size={35}
                className="text-type1 pointer"
              />
            </span>
            <div className="w-100 text-center d-flex justify-content-center align-items-center position-relative">
              <span className="me-3">
                <Image src={LawyerServiceIcon} alt="" />
              </span>
              <h5 className="text-type2 fw-2">Services</h5>
            </div>
          </div>
          <div
            className="px-md-4 position-relative "
            style={{ minHeight: conversations?.length === 0 ? "50vh" : "80vh" }}
          >
            <h5 className="fw-0 mb-3 ">Recent Services</h5>

            {conversations?.length === 0 && (
              <div className="noConversation2">
                <h5 className="text-center py-5">No Services Available Yet</h5>
              </div>
            )}

            {conversations?.map((item, index) => (
              <span className="mb-5" key={index}>
                <LawyerServiceComp
                  btnClassNamee={"px-5"}
                  bg={"bg-type3"}
                  btnText="Chat"
                  data={item}
                />
              </span>
            ))}
          </div>
        </div>{" "}
      </div>
    );
  };

  const HandleLAwyerReciept = (lawyersTool, setLawyerTools) => {
    return (
      <div className="position-relative">
        <div className="back">
          <Image src={BackgroundImage} alt="" />
        </div>
        <hr />
        <div className="bg-white py-4 px-md-4 ">
          <div className="mb-5">
            <span onClick={() => setLawyerTools(0)}>
              <MdOutlineArrowBackIosNew
                size={35}
                className="text-type1 pointer"
              />
            </span>
            <div className="w-100 text-center d-flex justify-content-center align-items-center">
              <span className="me-3">
                <Image src={LawyerReciptIcon} alt="" />
              </span>
              <h5 className="text-type2 fw-2">Receipts</h5>
            </div>
          </div>
          <div className="px-2 px-md-4 " style={{ minHeight: "80vh" }}>
            <div className=" d-flex justify-content-center">
              <div className="col-md-8">
                <div className="fw-0 mb-3 h7">Recent Receipts</div>
                <div>
                  {state?.lawyerPayment?.length === 0 && (
                    <div className="noConversation2 py-5">
                      <h5 className="text-center">
                      <div>
                <Image src={Empty} alt='' layout='' />
                <h5>Nothings here</h5>
              </div>
                      </h5>
                    </div>
                  )}
                </div>
                {state?.lawyerPayment?.map((item, index) => (
                  <div key={index}>
                    <LawyerReceiptComp data={item} />
                  </div>
                ))}

{state?.lawyerPayment?.length>0 &&    <div className="mt-5">
                  {" "}
                  <Button
                    bntText={"Proceed"}
                    className="bg-type1 text-white py-3 px-5 w-100 h4"
                  />
                </div>
  }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const HandleSchdeular = (setLawyerTools, LawyerSchedularData) => {
    return (
      <div>
        <hr />
        <div className="bg-white py-4 px-md-4 position-relative">
          <div className="back">
            <Image src={BackgroundImage} alt="" />
          </div>
          <div className="mb-5 position-relative">
            <span onClick={() => setLawyerTools(0)}>
              <MdOutlineArrowBackIosNew
                size={35}
                className="text-type1 pointer"
              />
            </span>
            <div className="w-100 text-center d-flex justify-content-center align-items-center">
              <span className="me-3">
                <Image src={LawyerShedularIcon} alt="" />
              </span>
              <h5 className="text-type2 fw-2">Scheduler</h5>
            </div>
          </div>
          <div className="px-2 px-md-4 " style={{ minHeight: "80vh" }}>
            <div className=" mb-5 d-flex justify-content-center position-relative">
              <div className="col-md-12">
                <div className="fw-0 mt-3 mb-5 d-flex flex-wrap justify-content-between align-items-center">
                  <h6>Upcoming Schedule</h6>
                  <div>
                    <Button
                      onClick={() => setLawyerTools(4)}
                      className={
                        "bg-type1 py-2 py-md-3 rounded px-md-3 text-white"
                      }
                      bntText={
                        <span className="">
                          <span className="me-2">
                            <HiPlusCircle size={30} />
                          </span>
                          Create New
                        </span>
                      }
                    />
                  </div>
                </div>
                {LawyerSchedularData?.map((item, index) => (
                  <div key={index}>
                    <LawyerShedularComp data={item} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const HandleAddSchedular = () => {
    return (
      <div>
        <hr />
        <div className="bg-white py-4 px-md-4 ">
          <div className="mb-5">
            <span onClick={() => setLawyerTools(0)}>
              <MdOutlineArrowBackIosNew
                size={35}
                className="text-type1 pointer"
              />
            </span>
            <div className="w-100 text-center d-flex justify-content-center align-items-center">
              <span className="me-3">
                <Image src={LawyerShedularIcon} alt="" />
              </span>
              <h5 className="text-type2 fw-2">Scheduler</h5>
            </div>
          </div>
          <div className="px-2 px-md-4 " style={{ minHeight: "80vh" }}>
            <div className=" mb-5 ps-lg-5">
              <div className="row">
                <div className="col-md-12">
                  <InputComp2
                    label={"Schedule name"}
                    required
                    bordercolor={"#EEEEEE"}
                    placeholder="Enter schedule name"
                  />
                </div>
              </div>
              <div className="col-md-12">
                <SelectComp label={"Add Recipient "} required />
              </div>
              <div className="row mb-7">
                <div className="col-lg-6">
                  <InputComp2
                    label={"Select Date"}
                    required
                    bordercolor={"#EEEEEE"}
                    placeholder="Enter schedule name"
                    typeComp={"date"}
                  />
                </div>
                <div className="col-lg-6">
                  <InputComp2
                    label={"Select Time"}
                    required
                    bordercolor={"#EEEEEE"}
                    placeholder="Enter schedule name"
                    typeComp={"time"}
                  />
                </div>
              </div>
              <div className="row justify-content-center">
                <div className="col-md-6">
                  <div className="text-center">
                    <Button
                      bntText={"Saved"}
                      className="py-3 w-100 px-5 h5 fw-bold bg-type1 text-white"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const handleAddBankDetails = () => {
    return (
      <div
        className="bg-white  py-5 px-3 px-lg-5 d-flex flex-column justify-content-between"
        style={{ height: "100vh" }}
      >
        <div className="mb-5">
          <span onClick={() => setLawyerTools(0)}>
            <MdOutlineArrowBackIosNew
              size={35}
              className="text-type1 pointer"
            />
          </span>
        </div>
        <div>
          <div>
            <div className="text-center">
              <Image src={AddCardIcon} alt="" />
            </div>
          </div>
          <h5 className="fw-bold mb-5 text-center">Add Account</h5>
          <div className="d-flex justify-content-center">
            <div className="col-8">
              <div className="col-12 mb-5">
                <InputComp2 placeholder=" Bank Name" bg={"#F2F2F2"} />
              </div>
              <div className="col-12 mb-5">
                <InputComp2 placeholder="Account Name" bg={"#F2F2F2"} />
              </div>
              <div className="row">
                <div className="col-12 mb-5">
                  <InputComp2
                    placeholder={"Account Number"}
                    type="number"
                    bg={"#F2F2F2"}
                  />
                </div>

                <div className="text-center">
                  <Button
                    bntText={"Save Card"}
                    onClick={() => setCardToggle(null)}
                    className="text-white h5 bg-type1 px-5 rounded py-3 fw-bold"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
 

  return (
    <LayoutAuthLawyer Title={"DashBoard | Home"}>
      <div className="mt-4 container-fluid ">
        <div className="row mb-4">
          <div className="col-xl-12">
            <div className="ps-1 ps-md-3 mb-0 pt-2 bg-white mb-4">
              <div className="row px-3">
                <div className="col-3 ">
                  <Image src={HomeTopIcon} alt="" />
                </div>
                <div className="col-9 py-3 ">
                  <div className="pe-4">
                    <h5 className="text-type1 fw-bold">
                      Welcome to your dashboard.{" "}
                    </h5>
                    <p className="h6 text-type2 fw-0">
                      Thanks! Enter your payment information now. You can <br />
                      always change it in settings.
                    </p>
                    {!userProfile?.user?.lawyerBankAccountDetail?.id &&
                      state?.isLoading && (
                        <div className="text-end">
                          <Button
                            onClick={() => setLawyerTools(5)}
                            bntText={"Click Here"}
                            className="bg-8 fw-bold text-type1"
                          />
                        </div>
                      )}
                  </div>
                </div>
              </div>

              {lawyersTool === 1
                ? HandleLawyerServices(lawyersTool, conversations)
                : lawyersTool === 2
                ? HandleLAwyerReciept(lawyersTool, setLawyerTools)
                : lawyersTool === 3
                ? HandleSchdeular(setLawyerTools, LawyerSchedularData)
                : lawyersTool === 4
                ? HandleAddSchedular()
                : lawyersTool === 5
                ? handleAddBankDetails()
                : null}

              {/*  */}
            </div>

            {/*  */}
            {!lawyersTool && (
              <div>
                <div className="mb-3">
                  <div className="row  g-4">
                    {lawyerHomeData.map((item, index) => (
                      <div
                        onClick={() => setLawyerTools(1 + index)}
                        key={index}
                        className="col-md-6 col-lg-6 col-xl-4  pointer   rounded  mb-2"
                      >
                        <Card body className="py-4 px-2 h-100">
                          <div className="d-flex flex-wrap align-items-center">
                            <div className="me-3">
                              <Image src={item?.icon} alt="" />
                            </div>
                            <h5 className="text-type2 fw-2">{item?.name}</h5>
                          </div>
                        </Card>
                      </div>
                    ))}
                  </div>
                </div>
                {/*  */}

                <div className="mb-5">
                  {/* {state?.isLoading ?<Loader height={"40vh"}/> :
                  
                  } */}
                  <div>
                    {state?.conversations?.length === 0 && (
                      <>
                        <h5 className="text-type1 fw-1 mb-3">
                          Service Request
                        </h5>
                        <div className="noConversation2 py-5 bg-white">
                          <div className="noConversation2 text-center ">
                          <div>
                <Image src={Empty} alt='' layout='' />
                <h5>Nothings here</h5>
              </div>
                          </div>
                        </div>
                      </>
                    )}

                    {state?.conversations &&
                      conversations?.slice(0, 1)?.map((item, index) => (
                        //  //console.log(item,'item')
                        <LawyerServiceComp
                          data={item}
                          btnText="Connect"
                          key={index}
                        />
                      ))}
                  </div>
                </div>
                {/*  */}
              {state?.newFeedsLawyer?.length>0 && <div className="mb-5 mb-lg-0">
                  <div className="text-type1 fw-bolder h8 my-3">News Feed</div>
                  <div>
                    {state?.newFeedsLawyer?.map((item,index)=>
                    <div key={index} className=" g-3">
                    <span>
                      {item?.body}
                    </span>
                  </div>
                    
                    )}
                    
                  </div>
                </div>
                    }
              </div>
            )}
          </div>
          {/* <div className="col-xl-3">
            <div>
              <div>
                <div className=" font-1 mb-3">
                  <Calendar onChange={onChange} value={value} />
                </div>
                <div className="bg-white p-2 d-flex justify-content-between">
                  <h6 className="mb-0"> Availability</h6>
                  <div>
                    <SwitchButton
                      checked={lawyerAvailablity}
                      onClick={(e) => {
                        setLawyerAvailablity(!lawyerAvailablity);
                        LawyerAvailablityFunc(true);
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="font-1">
                <div className="text-type1 fw-bolder  mt-4 mb-4">
                  <h5 className="text-5 font-1 mb-0">Upcoming Events</h5>
                  <span className="h7 fw-0 font-1 text-5">
                    Today, {moment().format("MMM Do YY")}
                  </span>
                </div>
                <div>
                  <UpcomingEvent title={"No Message"} />
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </LayoutAuthLawyer>
  );
}
