/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useRef, useState } from "react";
import styledComponents from "styled-components";
import { LayoutAuth } from "../../../Layout";

import { GoPrimitiveDot } from "react-icons/go";
import { FiSend } from "react-icons/fi";
import { MdAttachFile } from "react-icons/md";
import { GeneralUrl, MessageData, request } from "../../../utils";
import { io } from "socket.io-client";
import { useAuth } from "../../../hooks/useContext";
import { getOtherUserProfile } from "../../../services";
import { ListUsers, Loader, Messager } from "../../../Components";
import { useQuery } from "react-query";


const Style = styledComponents.div`
.imageWrapperMessage{
  border-radius: 50%;
  width: 50px;
  height: 50px;
  overflow: hidden;
}
.h-1{
  min-height:100vh;
}

`;

export default function Index() {
  const [message, setMessage] = useState(null); //currentconversation Chat
  const [getSelectedMessages, setGetSelectedMessages] = useState();
  const [convsersation, setConversation] = useState();
  const [sendMessage, setSendMessage] = useState();
  const [arrivalMessage, setArrivalMessage] = useState(null);
  const [activeUser, setActiveUser] = useState();
  //
  const [pic, setPic] = useState();
  const [showPic, setShowPic] = useState();
  //
  const {
    getOtherProfileFunc,
    userProfile,
    getallConversationFunc,
    state,
    state: { otheruser },
    sendMessageFunc,
  } = useAuth();
  const [otherUserId, setOtherUserId] = useState(14);
  // const socket = useRef(io("ws://pocketlawyers.herokuapp.com/"));
  const scrollRef = useRef();
  const socket = useRef();
  const scrollTo = () =>
    scrollRef?.current?.scrollIntoView({ behavior: "smooth" });
  //const socket = io("https://pocketlawyers.herokuapp.com/api/messages")

  const startSocket = () => {
    socket.current = io(GeneralUrl);
    // socket.on("getMessage", (data) => {
    //   //console.log(data,'socketTop')
    // })
    socket.current.on("getMessage", (data) => {
      //console.log(data, "socketTop");
      scrollTo();
      setArrivalMessage({
        conversationId: data?.conversationId,
        sender: data?.senderId,
        text: data?.message,
        media:data?.media,
        createdAt: Date.now(),
      });

      // scrollTo();
    });
  };

  useEffect(() => {
    startSocket();
  }, []);

  // //console.log(message?.conversation?.messages[0],arrivalMessage?.conversationId,getSelectedMessages,'socket')
  useEffect(() => {
    //  //console.log(arrivalMessage?.sender,message?.conversation,'arrivalMessage?.sender')
    //   message?.conversation?.senderId===(arrivalMessage?.sender)&&
    message?.conversation?.messages[0]?.conversationId ===
      arrivalMessage?.conversationId &&
      arrivalMessage &&
      setGetSelectedMessages([...getSelectedMessages, arrivalMessage]);
  }, [arrivalMessage]);

  useEffect(() => {
    socket.current.on("hello", (msg) => {
      // console.log(msg, "socketTop");
    });
  }, [socket]);

  // //console.log(socket,'userProfile?.user?.id')

  useEffect(() => {
    socket.current.emit("addUser", userProfile?.user?.id);
    socket.current.on("getOnlineUsers", (users) => {
      setActiveUser(users?.lawyers);
    });
  }, [userProfile?.user?.id]);



  useEffect(() => {
    // window.location.href = "/login";
    // clean up controller
    let isSubscribed = true;
    if (userProfile?.user?.id) {
      const GetallConversation = async () => {
        getallConversationFunc(userProfile?.user?.id);
      };
      GetallConversation();
    }

    // cancel subscription to useEffect
    return () => (isSubscribed = false);
  }, [getallConversationFunc, userProfile?.user?.id]);
  //https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg

  //console.log(message, "checkMessage");

  useEffect(() => {
    if (state?.isLoading) {
      setSendMessage("");
      scrollTo();
    }
  }, [state?.isLoading]);



  const getConversation =async () => {
    try {
      const res = await request.get(`/messages/${message?.conversation?.id}`);
      //console.log(res?.data, "socket1");
      setGetSelectedMessages(res?.data);
      //  setGetSelectedMessages([...getSelectedMessages,res?.data])
       scrollTo();
      //  setSendMessage("")
      return res?.data;
    } catch (error) {
      const err = error?.response?.data?.message || error?.message;
      // throw new Error(err);
    }
  }

  //getCurrentMessage
  const { data,isLoading,isError,error,isFetching } = useQuery("getSingleProduct",getConversation,{
    // enabled:router?.isReady,
    // onSuccess:CheckCat,
    
    // onError: (error) =>
    //     toast.error(`Oops, something went wrong : ${error.message}`),
  })
  //console.log(data,'helloss')

  //getCurrentMessage
  useEffect(() => {
    const getCurrentMessage = async () => {
      try {
        const res = await request.get(`/messages/${message?.conversation?.id}`);
        //console.log(res?.data, "socket");
        setGetSelectedMessages(res?.data);
        //  setGetSelectedMessages([...getSelectedMessages,res?.data])
        //  scrollTo();
        //  setSendMessage("")
        return res?.data;
      } catch (error) {
        const err = error?.response?.data?.message || error?.message;
        // throw new Error(err);
      }
    };
    getCurrentMessage();
  }, [message?.conversation?.id]);

  // console.log(state?.conversations);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if (
      message?.conversation?.id &&
      message?.conversation?.id &&
      message?.conversation?.receiverId
    ) {
      // const value = {
      //   conversationId: message?.conversation?.id,
      //   text: sendMessage,
      //   media: pic,
      //   mediaPath:pic
      // };
      const Form = new FormData();
      Form.append("conversationId", message?.conversation?.id);
      Form.append("text", sendMessage);

      Form.append("mediaFile", showPic);

      const checkout = {

        conversationId: message?.conversation?.id,
        senderId: userProfile?.user?.id,
        receiverId: message?.conversation?.receiverId,
        message: sendMessage,
        media: pic,
        mediaPath:pic
      }

       //console.log(checkout,message?.conversation,userProfile?.user?.id)

      socket.current.emit("sendMessage", {
        conversationId: message?.conversation?.id,
        senderId: userProfile?.user?.id,
        receiverId: message?.conversation?.receiverId,
        message: sendMessage,
        media: pic,
      });
      //  console.log(message?.conversation, "picture");

      const getOtherUser = async () => {
        try {
          const res = await request.post(`/messages`, Form);
          //console.log(res?.data, "getAllConversation");
          setPic(null);
          setShowPic(null)
          setGetSelectedMessages([...getSelectedMessages, res?.data]);
          scrollTo();
          setSendMessage("");
          return res?.data;
        } catch (error) {
          const err = error?.response?.data?.message || error?.message;
          // window.location.reload()
          // throw new Error(err);
        }
      };
      getOtherUser();
      // startSocket()
    }

    // startSocket()
  };
  //
  const hiddenFileInput = React.useRef(null);
  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    const reader = new FileReader();
    setShowPic(fileUploaded);
    reader?.readAsDataURL(fileUploaded);
    reader.onload = function () {
      setPic(reader?.result);
      // console.log(reader?.result)
    };
    //console.log(fileUploaded,'picture')

    const form = new FormData();

    form.append("profile_pic", fileUploaded);

    // if (fileUploaded) {
    //   UpdateProfilePicFunc(form);
    // }

    // setShowPic(URL.createObjectURL(fileUploaded));
  };

  useEffect(() => {
    scrollTo();
  }, [getSelectedMessages]);

  if (state?.isLoading) {
    <Loader />;
  }
  return (
    <LayoutAuth Title={"Message"}>
      <Style className="row">
        <div className="col-lg-3">
          <div>
            <h5 className="fw-2 mb-3"> Message </h5>
            <div className="messagebox1">
              {state?.conversations?.map((conversation, index) => (
                <ListUsers
                  key={index}
                  state={state}
                  message={MessageData}
                  setMessage={setMessage}
                  conversation={conversation}
                  userProfile={userProfile}
                  otheruser={otheruser}
                  getSelectedMessages={getSelectedMessages}
                  getOtherUserProfile={getOtherProfileFunc}
                  setGetSelectedMessages={setGetSelectedMessages}
                  activeUser={activeUser}
                />
              ))}
            </div>
          </div>
        </div>
        <div className="col-lg-9 mb-5 mb-md-0 position-relative">
          {/* {console.log(message)} */}
          {message?.conversation ? (
            <div className="bg-white container pb-5 h-100">
              <div className="row h-100">
                <div className="col-md-12 border-end h-100">
                  <div className=" border-bottom d-flex align-items-center justify-content-between py-4 topHeader">
                    <div className=" d-flex align-items-center">
                      <div className="imageWrapperMessage me-3">
                        <img
                          src={
                            message?.user?.pic_name
                              ? GeneralUrl +
                                message?.user?.pic_name
                              : "https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg"
                          }
                          alt=""
                        />
                      </div>
                      <h5 className="me-5 mb-0">{message?.user?.first_name}</h5>
                      {activeUser?.map((item, index) =>
                        item?.userId === message?.user?.id ? (
                          <div key={index} className="d-flex align-items-center">
                            <GoPrimitiveDot color="green" size={20} />
                            <h6 className="mb-0">Active</h6>
                          </div>
                        ) : (
                          <div className="d-flex align-items-center">
                            <h6 className="mb-0">Offline</h6>
                            {/* <GoPrimitiveDot color="green" size={20} />
                    <h6 className="mb-0">Offline</h6> */}
                          </div>
                        )
                      )}
                      <h6 className="mb-0 ps-3">
                        {message?.conversation?.serviceName}
                      </h6>
                    </div>
                    <div className={!message?.conversation?.closed?"text-danger fw-bolder":''}>
                    {message?.conversation?.closed?'Chat closed':''}
                    </div>
                  </div>

                  <div className="hello">
                    <div className="pt-8  messagebox">
                      {getSelectedMessages?.map((item, index) => (
                        <div className="" key={index} ref={scrollRef}>
                       
                          <Messager
                            item={item}
                            userProfile={userProfile}
                            message={message}
                          />
                        </div>
                      ))}
                    </div>

                    <div className="h9 text-6">{pic ? "File Added" : ""}</div>
                    
                    <div className="d-flex align-items-center w-100 bg-10 ">
                      <form
                        onSubmit={handleOnSubmit}
                        className="d-flex align-items-center w-100 bg-10 my-2 py-0 px-2"
                      >
                    <div className="d-flex align-items-center w-100 bg-10 ">
                      
                      <div className="w-100 bg-10 d-flex">
                        <span>
                        {message?.conversation?.closed?
                          <MdAttachFile className="text-muted "  size={35} />
                          :
                          <MdAttachFile className="text-muted pointer" onClick={handleClick} size={35} />
                        }
                          <input
                            type="file"
                            ref={hiddenFileInput}
                            onChange={handleChange}
                            style={{ display: "none" }}
                            accept="image/*,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.pdf"
                          />
                        </span>
                        <input
                          className="w-100 bg-10"
                          value={sendMessage}
                          onChange={(e) => setSendMessage(e.target.value)}
                        />
                      </div>
                      <div>
                      {message?.conversation?.closed?
                        <FiSend
                        className="text-muted "
                        size={30}
                        // onClick={}
                      />:
                      <FiSend
                          className="text-muted pointer"
                          size={30}
                          onClick={handleOnSubmit}
                        />}
                      </div>
                    </div>
                    </form>
                    </div>
                  </div>
                </div>
                {/* <div className="col-md-2 d-none d-md-block">hello</div> */}
              </div>
            </div>
          ) : (
            <div>
              {state?.conversations?.length>0 ?
             <span className="noConversation">
             Open a Conversation to <br /> start a chat
           </span>
           :
           <span className="noConversation">
          No Message Available  Yet<br /> Request A Service
         </span>  
            }
             
            </div>
          )}
        </div>
      </Style>
    </LayoutAuth>
  );
}
