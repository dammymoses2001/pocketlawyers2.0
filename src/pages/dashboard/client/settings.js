/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import { LayoutAuth } from "../../../Layout";
import Image from "next/image";
import { FaqData, GeneralUrl, notificationSettings, SettingSideBar } from "../../../utils";
import { MdOutlineArrowForwardIos } from "react-icons/md";
import UserAvater from "../../../assest/Image/userImage.svg";
import uploadicon from "../../../assest/Image/addImageIcon.svg";
import Toggle from "../../../assest/Image/toggle.svg";
import ToggleOne from "../../../assest/Image/toggle1.svg";
import styledComponents from "styled-components";
import Cookies from 'js-cookie'
import {
  Accordiance,
  InputComp,
  InputComp2,
  ResuableImageContainer,
  TextAreaComp,
} from "../../../Components";
import Button from "../../../Components/Ui/Button";
import { useAuth } from "../../../hooks/useContext";
import { FiMinus } from "react-icons/fi";



const Style2 = styledComponents.div`
h6{
  display:contents !important;
}
`;

export default function Index() {
  const {
    userProfile: { user },
    UpdateProfileFunc,
    state,
    getProfile,
    state: { isLoading, },
    UpdateProfilePicFunc,
    UpdateKycPost,
    getKYCFunc,
    PostKYCFunc,
    GetNewsFeeds
  } = useAuth();
  const initialState = {
    ...user,
    country: user?.country?.name,
  };

  const initialSupport = {
    subject: "",
    message: "",
  };
  const [userData, setUserData] = useState(initialState);
  const [supportData, setSupportData] = useState(initialSupport);
  const [selectMenu, setSelectMenu] = useState(0);
  const [editProfile, setEditProfile] = useState(false);
  const [pic, setPic] = useState();
  const [showPic, setShowPic] = useState();
  const [business,setBusiness] =useState(initialState?.business)

  //display image selected
  // const []
  //setImg(URL.createObjectURL(file))

  //
  //console.log(user)

  useEffect(() => {
    getKYCFunc();
  }, [getKYCFunc]);
  useEffect(() => {
    getProfile();
  }, [getProfile]);

  const hiddenFileInput = React.useRef(null);
  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    setPic(fileUploaded);

    const form = new FormData();

    form.append("profile_pic", fileUploaded);

    if (fileUploaded) {
      UpdateProfilePicFunc(form);
    }

    setShowPic(URL.createObjectURL(fileUploaded));
  };
  const handleUpdateKYC =()=>{
    UpdateKycPost(business)
  }
 

  const sideBar = () => {
    return (
      <Style2>
        <h5 className="fw-2 mb-5">Setting</h5>
        {SettingSideBar.map((item, index) => (
          <div key={index}>
            <div
              className="py-3 ps-4 pe-4 rounded bg-white mb-3 d-flex align-items-center justify-content-between pointer"
              onClick={() => setSelectMenu(index)}
            >
              <div className="d-flex align-items-cente">
                <div className="me-3">
                  <Image
                    src={selectMenu === index ? item?.icon : item?.icon2}
                    alt=""
                  />
                </div>
                <h5
                  className={`h51 ${
                    selectMenu === index ? "text-7 " : "text-muted"
                  }`}
                >
                  {item?.name}
                </h5>
              </div>
              <div className={selectMenu === index ? "text-7" : "text-muted"}>
                <MdOutlineArrowForwardIos size={20} />
              </div>
            </div>
          </div>
        ))}
      </Style2>
    );
  };

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };
  const handleOnChange1 = (e) => {
    const { name, value } = e.target;
    setBusiness({ ...business, [name]: value });
  };
  const handleOnChangeSupport = (e) => {
    const { name, value } = e.target;
    setSupportData({ ...supportData, [name]: value });
  };

  useEffect(() => {
    // window.location.href = "/login";
    if (false) {
      // window.location.href = "/login";
      return router.push("/login");
    }
  }, []);

 
  
  // //console.log(
  //   userData,'userData'
  // )

  const handleUpdateUserProfile = () => {
    const value = {
      ...userData,
      first_name: userData?.first_name,
      last_name: userData?.last_name,
      profile_pic: pic,
    };
    UpdateProfileFunc(value);
    //console.log(value, "value");
  };

 // console.log(userData, "state");
  const handleUserProfile = () => {
    return (
      <Style>
        <div className=" ">
          <div className=" text-center mb-5">
            <div className="profileImage mx-auto position-relative">
            <ResuableImageContainer Width={"100%"} Height={"100%"} picture={GeneralUrl+userData?.pic_name } newlySlectedImage={showPic}/>

              <div className="uploadImage pointer" onClick={handleClick}>
                <Image src={uploadicon} alt="" />
              </div>
              <input
                type="file"
                ref={hiddenFileInput}
                onChange={handleChange}
                style={{ display: "none" }}
              />
            </div>
          </div>
          <div className="container">
            <div className=" ">
              <Accordiance
                id={0}
                Accordiontitle={
                  <h5 className="fw-2 mb-5">Account Information</h5>
                }
                AccordionBody={
                  <div>
                    {!editProfile ? (
                      <div>
                        <div className="row mb-5">
                          <div className="col-lg-5 col-xl-2">
                            <h6 className="fw-bold">Name:</h6>
                          </div>
                          <div className="col">
                            <h6 className="fw-0">
                              {userData?.first_name} {userData?.last_name}
                            </h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                        <div className="col-md-5 col-xl-2">
                                                      <h6 className="fw-bold">Email:</h6>
                          </div>
                          <div className="col-8">
                            <h6 className="fw-0 text-wrap d-md-none">{userData?.email?.substring(0,20)} <br/> {userData?.email?.substring(21,userData?.email?.length)}</h6>
                            <h6 className="d-none d-md-block">{userData?.email}</h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                        <div className="col-md-5 col-xl-2">
                                                      <h6 className="fw-bold">Phone:</h6>
                          </div>
                          <div className="col-8">
                            <h6 className="fw-0">{userData?.phone}</h6>
                          </div>
                        </div>
                        <div className="row mb-7">
                        <div className="col-md-5 col-xl-2">
                                                      <h6 className="fw-bold">Address:</h6>
                          </div>
                          <div className="col-8">
                            <h6 className="fw-0">
                              {userData?.home_address}
                              <br />
                              <br /> {userData?.stateOfOrigin?.name} ,{" "}
                              {userData?.country}
                            </h6>
                          </div>
                        </div>
                        <div className="text-center">
                          <Button
                            bntText={"Edit Profile"}
                            onClick={() => setEditProfile(true)}
                            className="bg-type1 text-white h4 py-3 rounded px-5"
                          />
                        </div>
                      </div>
                    ) : (
                      <div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-3 col-lg-3 col-xl-2"></div>
                          <div className="row ">
                            <div className="col-md-3 col-lg-3 col-xl-2">
                              <h6 className="fw-bold">Name:</h6>
                            </div>
                            <div className="col">
                              <div className="row">
                                <div className="col-md-6 mb-3">
                                  <InputComp2
                                    bordercolor={"#E0E0E0"}
                                    value={userData?.first_name}
                                    name={"first_name"}
                                    onChange={handleOnChange}
                                    bg="#E0E0E0"
                                  />
                                </div>
                                <div className="col-md-6">
                                  <InputComp2
                                    bordercolor={"#E0E0E0"}
                                    value={userData?.last_name}
                                    name={"last_name"}
                                    onChange={handleOnChange}
                                    bg="#E0E0E0"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-3 col-lg-3 col-xl-2">
                            <h6 className="fw-bold">Email:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              bg="#E0E0E0"
                              value={userData?.email}
                              name={"email"}
                              onChange={handleOnChange}
                            />
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-3 col-lg-3 col-xl-2">
                            <h6 className="fw-bold">Phone:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              bg="#E0E0E0"
                              value={userData?.phone}
                              name={"phone"}
                              onChange={handleOnChange}
                            />
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-3 col-lg-3 col-xl-2">
                            <h6 className="fw-bold">Address:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              value={userData?.home_address}
                              name={"home_address"}
                              onChange={handleOnChange}
                              bg="#E0E0E0"
                              disabled={true}
                            />
                          </div>
                        </div>
                        <div className="row mb-7">
                          <div className="col-md-3 col-lg-3 col-xl-2"></div>
                          <div className="col">
                            <div className="row">
                              <div className="col-md-6 mb-3">
                                <InputComp2
                                  bordercolor={"#E0E0E0"}
                                  value={userData?.stateOfOrigin?.name}
                                  name={"stateOfOrigin"}
                                  onChange={handleOnChange}
                                  bg="#E0E0E0"
                                  disabled={true}
                                />
                              </div>
                              <div className="col-md-6">
                                <InputComp2
                                  bordercolor={"#E0E0E0"}
                                  value={userData?.country}
                                  name={"country"}
                                  onChange={handleOnChange}
                                  bg="#E0E0E0"
                                  disabled={true}
                                />
                              </div>
                              <div>
                              <div className="col mt-3">
                  <div className="row">
                    <div className="text-center col-lg-6 justify-content-center">
                      <Button
                        bntText={editProfile ? "Save Profile" : "Edit Profile"}
                        className="bg-type1 text-white h5 py-2 px-3  me-3"
                        onClick={
                          editProfile
                            ? handleUpdateUserProfile
                            : () => setEditProfile(!editProfile)
                        }
                        loading={isLoading}
                      />
                                          </div>

                      <div className="text-center col-lg-6">
                      {editProfile && (
                        <Button
                          bntText={"Back"}
                          className="bg-white h4 py-2 px-3"
                          onClick={() => setEditProfile(!editProfile)}
                          // loading={isLoading}
                        />
                      )}
                    </div>
                  </div>
                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    )}
                  </div>
                }
              />
              <Accordiance
                id={1}
                Accordiontitle={
                  <h5 className="fw-2 mb-5">Business Information</h5>
                }
                AccordionBody={
                  <div>
                    {!editProfile ? (
                      <div>
                        <div className="row mb-5">
                          <div className=" col-md-5 col-xl-5">
                            <h6 className="fw-bold">Designation:</h6>
                          </div>
                          <div className="col">
                            <h6 className="fw-0">
                               {userData?.business?.designation}
                            </h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="col-md-5 col-xl-5">
                            <h6 className="fw-bold">Business Name:</h6>
                          </div>
                          <div className="col-5">
                            <h6 className="fw-0">{userData?.business?.businessName }</h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className=" col-md-5 col-xl-5">
                            <h6 className="fw-bold">Business Description:</h6>
                          </div>
                          <div className="col-5">
                            <h6 className="fw-0">{userData?.business?.businessDescription}</h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="col-md-5 col-xl-5">
                            <h6 className="fw-bold">Business Sector:</h6>
                          </div>
                          <div className="col-5">
                            <h6 className="fw-0">
                              {userData?.business?.businessSector}
                              
                            </h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="col-md-5 col-xl-5">
                            <h6 className="fw-bold">Business Address:</h6>
                          </div>
                          <div className="col-5">
                            <h6 className="fw-0">{userData?.business?.businessAddress}</h6>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="col-md-5 col-xl-5">
                            <h6 className="fw-bold">
                              Business Establishment Year:
                            </h6>
                          </div>
                          <div className="col-5">
                            <h6 className="fw-0">{userData?.business?.businessEstablishmentYear}</h6>
                          </div>
                        </div>
                        <div className="text-center">
                          <Button
                            bntText={"Edit Profile"}
                            onClick={() => setEditProfile(true)}
                            className="bg-type1 text-white h4 py-3 rounded px-5"
                          />
                        </div>
                      </div>
                    ) : (
                      <div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-3 col-lg-3 col-xl-2"></div>
                          <div className="row ">
                            <div className="col-md-4">
                              <h6 className="fw-bold">Designation:</h6>
                            </div>
                            <div className="col">
                              <div className="row">
                                <div className="col-md-12 mb-3">
                                  <InputComp2
                                    bordercolor={"#E0E0E0"}
                                    value={business?.designation}
                                    name={"designation"}
                                    onChange={handleOnChange}
                                    bg="#E0E0E0"
                                  />
                                </div>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-4">
                            <h6 className="fw-bold">Business Name:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              bg="#E0E0E0"
                              value={business?.businessName}
                              name={"businessName"}
                              onChange={handleOnChange1}
                            />
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-4">
                            <h6 className="fw-bold">Business Description:
</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              bg="#E0E0E0"
                              value={business?.businessDescription}
                              name={"businessDescription"}
                              onChange={handleOnChange1}
                            />
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-4">
                            <h6 className="fw-bold">Business Sector:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              value={business?.businessDescription}
                              name={"businessDescription"}
                              onChange={handleOnChange1}
                              bg="#E0E0E0"
                             
                            />
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-4">
                            <h6 className="fw-bold">Business Address:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              value={business?.businessAddress}
                              name={"businessAddress"}
                              onChange={handleOnChange1}
                              bg="#E0E0E0"
                             
                            />
                          </div>
                        </div>
                        <div className="row align-items-center mb-3">
                          <div className="col-md-4">
                            <h6 className="fw-bold">Business Establishment Year:</h6>
                          </div>
                          <div className="col">
                            <InputComp2
                              bordercolor={"#E0E0E0"}
                              value={business?.businessEstablishmentYear}
                              name={"businessEstablishmentYear"}
                              onChange={handleOnChange1}
                              bg="#E0E0E0"
                              
                            />
                          </div>
                        </div>
                        <div className="row mb-7">
                          <div className="col-md-3 col-lg-3 col-xl-2"></div>
                       
                        </div>
                        <div className="row">
                    <div className="text-center col-lg-6 justify-content-center">
                      <Button
                        bntText={editProfile ? "Save KYC" : "Edit Profile"}
                        className="bg-type1 text-white h5 py-2 px-3  me-3"
                        onClick={
                          editProfile
                            ? handleUpdateKYC
                            : () => setEditProfile(!editProfile)
                        }
                        loading={isLoading}
                      />
                                          </div>

                      <div className="text-center col-lg-6">
                      {editProfile && (
                        <Button
                          bntText={"Back"}
                          className="bg-white h4 py-2 px-3"
                          onClick={() => setEditProfile(!editProfile)}
                          // loading={isLoading}
                        />
                      )}
                    </div>
                  </div>
                      </div>
                    )}
                  </div>
                }
              />
              <div className="row ">
                <div className="col-md-3 col-lg-3 col-xl-2"></div>
                
              </div>
            </div>
          </div>
        </div>
      </Style>
    );
  };

 
  const handleNotification = () => {
    return (
      <div>
        <div>
          <h5 className="fw-bold">Notification:</h5>
          <hr />
          <div className="mt-5">
            {notificationSettings.map((item, index) => (
              <div
                key={index}
                className="d-flex justify-content-between mb-3 align-item-center"
              >
                <h6 className="fw-0">{item?.name}</h6>
                <div className="pointer">
                  <Image src={item?.status ? ToggleOne : Toggle} alt="" />
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  };
  //
  const handleSubmitSupoort = () => {
    PostSupportFunc(supportData);
    // "subject":"Subject.",
    // "message": "Message"
  };
  useEffect(() => {
    if (state?.supportMessage) {
      setSupportData(initialSupport);
    }
  }, [initialSupport, state?.supportMessage]);

  const handleSupport = () => {
    return (
      <div>
        <div className="pe-lg-5">
          <h5 className="fw-bold">Support:</h5>
          <hr />
          <div className="mt-5">
            <div className="mb-3">
              <InputComp2
                labelClassName={"text-muted"}
                bordercolor={"#E0E0E0"}
                label={"What is the support about?"}
                name="subject"
                onChange={handleOnChangeSupport}
                value={supportData?.subject}
              />
            </div>
            <div>
              {" "}
              <TextAreaComp
                rows="5"
                label={"Message"}
                labelClassName="text-muted"
                name="message"
                onChange={handleOnChangeSupport}
                value={supportData?.message}
              />
            </div>
            <div className="text-end">
              <Button
                bntText={"Send Message"}
                className="bg-type1 text-white h4 py-3 px-5"
                onClick={handleSubmitSupoort}
                loading={state?.isLoading}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  const handleFAQ = () => {
    return(
      <div className="">
            <h5 className="fw-bold">FAQ:</h5>
          <hr />
      <div></div>
      {FaqData?.map((item,index)=>
       <div key={index} className="font-3">
       <Accordiance
       id={index}
       display={"none"}
       Accordiontitle={<h6 className="d-flex align-items-end fw-2 font-1"><span className="dot me-3"><FiMinus color="#fff"/></span>{item?.title}</h6>}
       AccordionBody={
           <div className="d-flex align-item-center ms-3">
               <div className="me-2 px-1 rounded py-4 my-2 bg-type1"></div>
               <div className="w-100 lh-base fw-3" dangerouslySetInnerHTML={{__html: item?.desc}}>
               </div>
           </div>
       }
       />
       <hr className="my-2"/>
      
      
   


       
   </div>
      )}
     
</div>
    )
  }
  return (
    <LayoutAuth Title={"Settings"}>
      <div className="mb-4"></div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-4">{sideBar()}</div>
          <div className="col-lg-8">
            <div
              className="bg-white py-5 px-3 px-lg-5"
              style={{ minHeight: "100vh" }}
            >
              {selectMenu === 0 ? (
                handleUserProfile()
              ) : selectMenu === 1 ? (
                handleNotification()
              ) : selectMenu === 2 ? (
                handleSupport()
              ) : (
                handleFAQ()
              )}
            </div>
          </div>
        </div>
      </div>
    </LayoutAuth>
  );
}

const Style = styledComponents.div`
.profileImage{
  width: 300px;
    height: 300px;
    overflow: hidden;
    border-radius: 50%;
}
.uploadImage{
  position: absolute;
    bottom: 34px;
    right: 20px;
    z-index: 900;
    width:40%;
}
@media (max-width: 786px) {
  .profileImage{
    width: 200px;
      height: 200px;
      overflow: hidden;
      border-radius: 50%;
  }
  .uploadImage{
    // position: absolute;
    //   bottom: 34px;
    //   right: 20px;
    //   z-index: 900;
      width:20%;
  }
}

`;