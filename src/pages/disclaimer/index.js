/* eslint-disable react/no-unescaped-entities */
import styled from "styled-components";
import { LayoutNoAuth } from "../../Layout";
import Link from "next/link";

export default function index() {
  return (
    <LayoutNoAuth>
      <DivStyle>
        <section className="container">
          <div className="mb-5">
            <h4 className="text-7 mb-5 fw-bold">Disclaimer</h4>
            <p className="mb-3 text-muted">Last updated: March 22nd, 2022</p>

            <p className="mb-1">
              This Privacy Policy describes Our policies and procedures on the
              collection, use and disclosure of Your information when You use
              the Service and tells You about Your privacy rights and how the
              law protects You.
              <p>
                We use Your Personal data to provide and improve the Service. By
                using the Service, You agree to the collection and use of
                information in accordance with this Privacy Policy.
              </p>
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">Interpretation and Definitions</h5>
            <hr />
            <div className="fw-2 mb-2 text-7 h51">Interpretation</div>
            <p className=" mb-3">
              The words of which the initial letter is capitalized have meanings
              defined under the following conditions. The following definitions
              shall have the same meaning regardless of whether they appear in
              singular or in plural.
            </p>
            <div className="fw-2 mb-2 text-7 h51">Definitions</div>
            <p className=" mb-3">For the purposes of this Disclaimer:</p>
            <ul>
              <li>
                <span className="fw-bold">Company</span> (referred to as either
                "the Company", "We", "Us" or "Our" in this Agreement) refers to
                Pocket Lawyers & Co, Suite 1, 319 Borno Way off Herbert Macaulay
                Way Alagomeji Yaba, Lagos State, Nigeria.
              </li>
              <li>
                <span className="fw-bold">Service</span> refers to the Website.
              </li>
              <li>
                <span className="fw-bold">You</span> means the individual
                accessing or using the Service, or the company, or other legal
                entity on behalf of which such individual is accessing or using
                the Service, as applicable.
              </li>
              <li>
                <span className="fw-bold">Website</span> refers to
                PocketLawyers, accessible from www.pocketlawyers.io
              </li>
            </ul>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              The information contained on the Service is for general
              information purposes only.
            </p>
            <p className=" mb-3">
              The Company assumes no responsibility for errors or omissions in
              the contents of the Service.
            </p>
            <p className=" mb-3">
              The Company assumes no responsibility for any business interaction
              with any personnel associated with the Company that occurs outside
              the Website or outside our knowledge.
            </p>
            <p className=" mb-3">
              The Company shall not be liable for any special, direct, indirect,
              consequential, or incidental damages/errors/delays or any damages
              whatsoever, whether in an action of contract, negligence or other
              tort, arising out of or in connection with the services offered on
              the website in relation to a third-party service provider.
            </p>
            <p className=" mb-3">
              In no event shall the Company be liable for any special, direct,
              indirect, consequential, or incidental damages or any damages
              whatsoever, whether in an action of contract, negligence or other
              tort, arising out of or in connection with the use of the Service
              or the contents of the Service. The Company reserves the right to
              make additions, deletions, or modifications to the contents on the
              Service at any time without prior notice.
            </p>
            <p className=" mb-3">
              The Company does not warrant that the Service is free of viruses
              or other harmful components.
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">External Links Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              The Service may contain links to external websites that are not
              provided or maintained by or in any way affiliated with the
              Company.
            </p>
            <p className=" mb-3">
              Please note that the Company does not guarantee the accuracy,
              relevance, timeliness, or completeness of any information on these
              external websites and we are not responsible or liable for the
              content on those linked websites, or any loss you may suffer on
              those linked sites, and have no control over or rights in those
              linked websites.
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">Errors and Omissions Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              The information given by the Service is for general guidance on
              matters of interest only. Even if the Company takes every
              precaution to ensure that the content of the Service is both
              current and accurate, errors can occur. Plus, given the changing
              nature of laws, rules and regulations, there may be delays,
              omissions or inaccuracies in the information contained on the
              Service.
            </p>
            <p className=" mb-3">
              The Company is not responsible for any errors or omissions, or for
              the results obtained from the use of this information.
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">Fair Dealing Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              The Company may use copyrighted material which has not always been
              specifically authorized by the copyright owner. The Company is
              making such material available for criticism, comment, news
              reporting, teaching, scholarship, or research.
            </p>
            <p className=" mb-3">
              The Company believes this constitutes a "fair dealing" of any such
              copyrighted material as provided for in the Second Schedule of the
              Copyright Act, Cap T 13, Laws of the Federation of Nigeria 2004.
            </p>
            <p className=" mb-3">
              If You wish to use copyrighted material from the Service for your
              own purposes that go beyond fair dealing, You must obtain
              permission from the copyright owner.
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">Views Expressed Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              The Service may contain views and opinions which are those of the
              authors and do not necessarily reflect the official policy or
              position of any other author, agency, organization, employer or
              company, including the Company.
            </p>
            <p className=" mb-3">
              Comments published by users are their sole responsibility and the
              users will take full responsibility, liability and blame for any
              libel or litigation that results from something written in or as a
              direct result of something written in a comment. The Company is
              not liable for any comment published by users and reserves the
              right to delete any comment for any reason whatsoever.
            </p>
            <p className=" mb-3"></p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">No Responsibility Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              All content contained on this website or otherwise provided to you
              as part of the online services is intended to provide general
              information in summary form on legal and other topics, current at
              the time of publication. The content does not constitute legal (or
              other) advice and should not be relied upon as such. You should
              obtain specific legal or other professional advice before relying
              on any content contained in this website.
            </p>
            <p className=" mb-3">
              The information on the Service is provided with the understanding
              that the Company is not herein engaged in rendering legal,
              accounting, tax, or other professional advice and services. As
              such, it should not be used as a substitute for consultation with
              professional accounting, tax, legal or other competent advisers.
            </p>
            <p className=" mb-3">
              In no event shall the Company or its suppliers be liable for any
              special, incidental, indirect, or consequential damages whatsoever
              arising out of or in connection with your access or use or
              inability to access or use the Service.
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3">"Use at Your Own Risk" Disclaimer</h5>
            <hr />

            <p className=" mb-3">
              All information in the Service is provided "as is", with no
              guarantee of completeness, accuracy, timeliness or of the results
              obtained from the use of this information, and without warranty of
              any kind, express or implied, including, but not limited to
              warranties of performance, merchantability and fitness for a
              particular purpose.
            </p>
            <p className=" mb-3">
              The Company will not be liable to You or anyone else for any
              decision made or action taken in reliance on the information given
              by the Service or for any consequential, special or similar
              damages, even if advised of the possibility of such damages.
            </p>
          </div>
          <div className="mb-5">
            <h5 className="text-7 mb-3"></h5>
            <hr />

            <p className=" mb-4">
              If you have any questions about this Privacy Policy, You can
              contact us:
            </p>
            <p className=" mb-1">
              By email:
              <a
                href="mailto:office@pocketlawyers.io"
                target={"_blank"}
                rel="noreferrer"
              >
                office@pocketlawyers.io
              </a>
            </p>
            <p className=" mb-1">
              By visiting this page on our website:{" "}
              <Link
                href="https://www.pocketlawyers.io/contact"
                target={"_blank"}
                rel="noreferrer"
              >
                <a>www.pocketlawyers.io/contact</a>
              </Link>
            </p>
            <p className=" mb-1">
              By phone number: <a href="tel:+2348166864154">+2348166864154</a>
            </p>
            <p className=" mb-1">
              By mail: Suite 1, 319 Borno Way off Hebert Macaulay Way Alagomeji
              Yaba, Lagos State, Nigeria.
            </p>
          </div>
        </section>
      </DivStyle>
    </LayoutNoAuth>
  );
}

const DivStyle = styled.div`
  @media (min-width: 1200px) {
    .container,
    .container-lg,
    .container-md,
    .container-sm,
    .container-xl {
      max-width: 1006px !important;
    }
  }

  @media (min-width: 1200px) {
    .container,
    .container-lg,
    .container-md,
    .container-sm,
    .container-xl {
      max-width: 1031px;
    }
  }
`;
