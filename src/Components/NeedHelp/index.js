import React from "react";
import styled from "styled-components";
import { AppleButton, GoogleButton } from "..";
import Link from 'next/link'
import Button from "../Ui/Button";

const Style = styled.div`
 
`;

export const NeedHelp = ({ bodyText, switchBg }) => {
  return (
    <Style className={`${switchBg ? "bg-type3" : "bg-type1"}`}>
      <div className="container py-4">
        <div className="d-flex flex-wrap justify-content-between align-items-center">
         <div className="mb-3 mb-lg-0 "> {bodyText}</div>
          <div className="">
           <Link href={'/signup'}><a className="h5 text-white bg-9 fw-bold py-3 px-4 rounded">Get Started</a></Link>
          </div>
        </div>
      </div>
    </Style>
  );
};
