import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect } from 'react'
import { useAuth } from '../../hooks/useContext'
import { LayoutNoAuth } from '../../Layout'
import { GeneralUrl } from '../../utils'

export default function Index() {
  const {GetPress,state:{getPress}} =useAuth()
//console.log(state,'GetPress')
  useEffect(() => {
    GetPress()
  }, [GetPress])
  

  return (
    <LayoutNoAuth Title={"Press"}>
        <div className="row justify-content-center">
          <div className="col-10 col-lg-10">
            <div className="py-5 py-lg-5 "></div>
            <div className="mb-5">
              <h5 className=" fw-1 text-capitalize">media</h5>
              <h3 className="h3 font-4 text-type1">Our Press</h3>
            </div>
            <div>
              {getPress?.map((item,index)=>
                 <div key={index} className='row mb-5' data-aos="flip-left">
                 <div className='col-lg-4 mb-4 mb-lg-0' >
                     <div>
                     <Image src={GeneralUrl+item?.imagePath} width={"438px"} height={"434px"} alt=''/>
                     </div>
                 </div>
                 <div className='col-lg-8'>
                     <h5 className='font-3 fw-bold mb-3 text-justify'>{item?.title} </h5>
                     <div className='font-3 h51 mb-3 text-justify'>
                    {item?.body}

                     </div>
                     <div className='text-end text-type1'>
                     <Link href={item?.url}><a className='text-type1 h51'>Read more</a></Link>
                     </div>
                 </div>
             </div>
              )}
             
                {/* <div className='row mb-5'>
                    <div className='col-lg-4 mb-4 mb-lg-0'>
                        <div>
                        <Image src={'/press.svg'} width={"438px"} height={"434px"} alt='' layout='responsive'/>
                        </div>
                    </div>
                    <div className='col-lg-8'>
                        <h5 className='font-3 fw-bold mb-3 text-justify'>PocketLawyers is Africa’s first fully integrative legal tech startup that offers access to affordable premium legal services and solutions to SMEs and Startups. </h5>
                        <div className='font-3 h51 mb-3 text-justify'>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Donec massa sapien faucibus et. Velit dignissim sodales ut eu sem integer. Mollis nunc sed id semper risus. Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Diam quam nulla porttitor massa id. Quisque sagittis purus sit amet volutpat. Blandit cursus risus at ultrices mi tempus. Faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. In ante metus dictum at tempor commodo ullamcorper. Ullamcorper eget nulla facilisi etiam dignissim diam quis enim. Leo integer malesuada nunc vel risus commodo viverra.


                        </div>
                        <div className='text-end text-type1'>
                        <h5 className='fw-2'>
                          <Link href={item?.url}><a>Read more</a></Link>
                        </h5>
                        </div>
                    </div>
                </div> */}
            </div>
            <div className="py-3 py-lg-5 "></div>
            </div>
            </div>
    </LayoutNoAuth>
  )
}
