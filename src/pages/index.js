import Head from "next/head";
import {LayoutNoAuth} from '../Layout'
import {
  
  HomeHeader,
  RetailSolution,
  TwoSide,
  NeedHelp,
  ClientFeedBackComp,
  Footer,
  OurClient,
  WhatWeCanOffer,
  Law,
  RetailerPackages,
  FAQs,
} from "../Components";
import { retailserviceData, retailPackage, ClientFeedBackData } from "../utils";
import AboutImage from '../assest/Image/pic33.png'
import VisionImage from '../assest/Image/Pic11.png'

export default function Home() {
  return (
    <LayoutNoAuth Title={"Home"}>
    <div className="Home overflow-hidden">
    
     
      <HomeHeader />
      <OurClient/>
      <WhatWeCanOffer/>
     <Law/>
     <RetailerPackages/>
     <FAQs/>
     {/* <RetailerPackages/> */}

      <ClientFeedBackComp data={ClientFeedBackData} />
      
    </div>
    </LayoutNoAuth>
  );
}
