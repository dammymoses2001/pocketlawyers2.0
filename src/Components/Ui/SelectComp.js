/* eslint-disable react/display-name */
/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react'
import { Dropdown, Form, FormControl } from 'react-bootstrap'
import styledComponents from 'styled-components'
import {BiSearch} from 'react-icons/bi'

const Style =styledComponents.div`
.form-label {
  font-size: ${props=>props.labelSize ?props.labelSize  :'18px'} !important;
}

`

export  const SelectComp =({label,selectText,labelClassName,labelSize,arrayData=[],input,required,...props})=> {
  return (
    <Style labelSize={labelSize}>
         <Form.Group className="mb-3">
     {label && <Form.Label  className={`fw-2 mb-2 ${labelClassName}`}>{label} {required && <span className="text-danger">*</span>}</Form.Label> }
      <Form.Select className='py-3 border-2 fw-2' {...props}>
      <option className=''>{selectText?selectText:'Select'}</option>
   
        {arrayData.map((item,index)=>
         <option id={item?.id}   
         key={index} value={item?.id}>{item[input]}</option>)}
       
   
      </Form.Select>
     {!props?.value && props?.showError &&<small className='text-danger text-capitalize fw-2'>{`${label} is required....` }</small>} 
    </Form.Group>
    </Style>
  )
}

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
    &#x25bc;
  </a>
));

// forwardRef again here!
// Dropdown needs access to the DOM of the Menu to measure it
const CustomMenu = React.forwardRef(
  ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
    const [value, setValue] = useState('');

    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <FormControl
          autoFocus
          className="mx-3 my-2 w-auto"
          placeholder="Type to filter..."
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <ul className="list-unstyled">
          {React.Children.toArray(children).filter(
            (child) =>
              !value || child.props.children.toLowerCase().startsWith(value),
          )}
        </ul>
      </div>
    );
  },
);
export  const DropdDownComp =({label,arrayData=[],input,required,...props})=> {
  return (
    <div className='w-100 border-1'>

  <Dropdown>
    <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
     <BiSearch size={30}/>
    </Dropdown.Toggle>

    <Dropdown.Menu as={CustomMenu}>
      {/* <Dropdown.Item eventKey="1">Red</Dropdown.Item>
      <Dropdown.Item eventKey="2">Blue</Dropdown.Item>
      <Dropdown.Item eventKey="3" active>
        Orange
      </Dropdown.Item>
      <Dropdown.Item eventKey="1">Red-Orange</Dropdown.Item> */}
    </Dropdown.Menu>
  </Dropdown>
  </div>
  
);
    
}

