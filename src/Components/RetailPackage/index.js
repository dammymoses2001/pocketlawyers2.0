import { Card, Table } from "react-bootstrap";
import styled from "styled-components";
import { RetailCard2 } from "../RetailCard";
import { GrCheckmark } from "react-icons/gr";
import { TiTimes } from "react-icons/ti";
import Link from "next/link";
import Button from "../Ui/Button";
import { subscriptiondetails } from "../../utils";
import { useState } from "react";
import { useRouter } from 'next/router'

const Styled = styled.section`
  .card {
    background: #ffffff;
    border: 2px solid #0d4447;
    box-sizing: border-box;
    box-shadow: -1.41011px -1.41011px 2.82022px rgba(0, 0, 0, 0.15),
      1.41011px 1.41011px 2.82022px rgba(0, 0, 0, 0.15);
    border-radius: 7.05056px 7.05056px 0px 0px;
    margin: 0 10px;
    min-height: ${(props) => (props.SizeComp ? props.SizeComp : "15rem")};
  }
`;

export const RetailPackage = ({
  retailPackage1,
  off,
  setSubscriptionToggle,
  setPlan,
}) => {
  const router = useRouter()
  const [type, setType] = useState(1);
  return (
    <Styled>
      <div className="row justify-content-center">
        <div className="col-10 col-lg-7">
        <div className="py-5 py-lg-1 "></div>
            <div className="mb-5">
            <h5 className=" fw-1 ">How we charge</h5>
            <h3 className="h3 font-4 text-type1">Our Retainer Packages</h3>
            </div>
        </div>
        <div className="col-10">
       

        <div>
          <div className="d-flex g-4 mb-5 justify-content-center">
            <div className="d-flex align-items-center  px-2 py-3 rounded bg-13">
              <div>
                <Button
                  bntText={"Small Scale Business"}
                  onClick={() => setType(1)}
                  className={`py-3 h1 mb-0 fw-bold px-4  me-2 h4 border-0 ${
                    type == 1 ? "bg-type4 text-white animate__animated animate__bounce" :"bg-7 text-11"
                  }  `}
                />
              </div>
              <div>
                <Button
                  bntText={"Medium Scale Business"}
                  onClick={() => setType(2)}
                  className={`py-3 h1 mb-0  fw-bold border-0 px-4 h4 ${type == 2 ? "bg-type4 text-white animate__animated animate__bounce":"bg-7 text-11"}  `}
                />
              </div>
            </div>
          </div>
          <div className="mb-8">
            {type === 2 && (
              <h6 className="fw-0 text-center font-1">
                This service is advised for Businesses/Corporations that want to
                <br /> outsource their Legal Departments to PocketLawyers.{" "}
              </h6>
            )}
          </div>
          <div></div>
          <div className="d-flex justify-content-center ">
            <div className="col-10 mb-5 font-1">
              {subscriptiondetails.length > 0 && (
                <div className="table-responsive">
                  <Table className="table table-borderless">
                    <tbody>
                      <tr>
                        <td colSpan={2}></td>
                        <td className="text-center">
                          {" "}
                          {type === 1 ? (
                            <div >
                              <div className="text-type1 fw-bold h61 mb-3">
                                SS Business
                              </div>
                              {/* {console.log(subscriptiondetails,' id:4,')} */}
                              <Link href={`/dashboard/client/subcription/SS Business`}>
                              <a   className="bg-type1 text-white px-5 text-nowrap h6 py-2 mb-3">
                              Select Plan
                              </a>
                              </Link>
                              {/* <Button
                                bntText={"Select Plan"}
                                className="bg-type1 text-white px-5 text-nowrap h6 py-2 mb-3"
                                onClick={() => {
                                  setSubscriptionToggle &&
                                    setSubscriptionToggle(3);
                                  setPlan && setPlan("SS Business");
                                }}
                              /> */}
                            </div>
                          ) : (
                            <div>
                              <h6 className="text-type1 fw-bold h61 mb-3"> MS Business</h6>
                              <Link href={`/dashboard/client/subcription/${364}`}>
                              <a  className="bg-type1 text-white px-5 text-nowrap h6  py-2 mb-3"
>         
Select Plan
                              </a>
                              </Link>
                              {/* <Button
                                bntText={"Select Plan"}
                                onClick={() => {
                                  setSubscriptionToggle &&
                                    setSubscriptionToggle(3);
                                  setPlan && setPlan("MS Business");
                                }}
                                className="bg-type1 text-white px-5 text-nowrap h6  py-2 mb-3"
                              /> */}
                            </div>
                          )}
                        </td>
                        <td className="text-center">
                          {type === 1 ? (
                            <div>
                              <div className="text-7 fw-bold h61 mb-3">SS Business +</div>
                              <Link href={`/dashboard/client/subcription/${314}`}>
                              <a    className="bg-type4 text-white px-5 text-nowrap h6  py-2 mb-3"
>         
Select Plan
                              </a>
                              </Link>
                              {/* <Button
                                bntText={"Select Plan"}
                                className="bg-type4 text-white px-5 text-nowrap h6  py-2 mb-3"
                                onClick={() => {
                                  setSubscriptionToggle &&
                                    setSubscriptionToggle(3);
                                  setPlan && setPlan("SS Business +");
                                }}
                              /> */}
                            </div>
                          ) : (
                            <div>
                              <div className="text-7 fw-bold h61 mb-3"> Business +</div>
                              <Button
                                bntText={"Select Plan"}
                                className="bg-type4 text-white px-5 text-nowrap h6  py-2 mb-3"
                                onClick={() => {
                                  router.push("/dashboard/client/subcription/344")
                                  // setSubscriptionToggle &&
                                  //   setSubscriptionToggle(3);
                                  // setPlan && setPlan("MS Business +");
                                }}
                              />
                            </div>
                          )}
                        </td>
                        <td className="text-center">
                          {type == 1 ? (
                            <div>
                              {" "}
                              <div className="text-6 fw-bold h61 mb-2">SS Business Premium</div>
                              <Button
                                bntText={"Select Plan"}
                                className="bg-12 text-white px-5 text-nowrap h6  py-2 mb-3"
                                onClick={() => {
                                  router.push(`/dashboard/client/subcription/SS Business`)
                                  // setSubscriptionToggle &&
                                  //   setSubscriptionToggle(3);
                                  // setPlan && setPlan("SS Business Premium");
                                }}
                              />
                            </div>
                          ) : (
                            <div>
                              {" "}
                              <h6 className="text-6 fw-bold h61 mb-3">
                                Business Premium
                              </h6>
                              <Button
                                bntText={"Select Plan"}
                                className="bg-12 text-white px-5 text-nowrap h6   py-2 mb-3"
                                onClick={() => {
                                  router.push("/dashboard/client/subcription/334")
                                  // setSubscriptionToggle &&
                                  //   setSubscriptionToggle(3);
                                  // setPlan && setPlan("MS Business Premium");
                                }}
                              />
                            </div>
                          )}
                        </td>
                      </tr>

                      {subscriptiondetails.map((item, index) => {
                        if (item?.type === type || item?.style) {
                          return (
                            <tr key={index} className="pb-5 ">
                              <td colSpan={2}>
                                {" "}
                                <div
                                  className={`h7  py-1 pe-2 ${item?.style ? "fw-0" : "fw-2"} text-end ${item?.name && 'bg-13'}` }
                                >
                                  {item?.name}
                                </div>
                              </td>

                              <td
                                className={` text-center ${
                                  item?.style ? "fw-0" : "fw-bolder text-type1"
                                } `}
                              >
                                {item?.one}{" "}
                              </td>
                              <td
                                className={` text-center ${
                                  item?.style ? "fw-0" : "fw-bolder text-7"
                                }`}
                              >
                                {item?.two}
                              </td>
                              <td
                                className={` text-center ${
                                  item?.style ? "fw-0" : "fw-bolder text-6"
                                }`}
                              >
                                {item?.three}{" "}
                              </td>
                            </tr>
                          );
                        }
                      })}
                      <tr className="pt-5">
                        <td></td>
                        <td colSpan={3} className="h-7">
                          * Terms and Conditions apply. For Small Scale
                          Enterprises, whose Legal needs and structure cannot be
                          accommodated in the above structures, please contact
                          us to discuss your legal needs in order to enable us
                          to specifically determine applicable rates.
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </div>
              )}
            </div>
          </div>
        </div>
        <div></div>
        <div></div>
        {!off && (
          <div className="text-center">
            <h5 className="fw-2 mb-5 text-capitalize">
              Reach Out to us to Discuss your <br/> Legal solutions
            </h5>
            <div className="">
              <Link href="/contact">
                <a className="h5 bg-type1 py-3 px-8 text-white rounded">
                  Get in touch with us{" "}
                </a>
              </Link>
            </div>
          </div>
        )}
      </div>
      </div>
    </Styled>
  );
};
