import Image from "next/image";
import React from "react";
import { BsInstagram, BsLinkedin } from "react-icons/bs";
import { LayoutNoAuth } from "../../Layout";

export default function index() {
  return (
    <LayoutNoAuth Title={"Team"}>
      <div className="Team">
        <div className="row justify-content-center">
          <div className="col-10 col-lg-7">
            <div className="py-5 py-lg-5 my-3"></div>
            <div className="mb-5">
              <h5 className=" fw-1 text-capitalize">our team</h5>
              <h3 className="h3 font-4 text-type1">Our People</h3>
            </div>
            <div className="bg-13 p-3 p-lg-4 " style={{ borderRadius: "10px" }} data-aos="zoom-in-down">
              {/* <div className='row'>
                    <div className=''></div>
                </div> */}
              <div>
                <div
                  className="me-3 ourteam mb-3 mb-lg-0"
                  style={{ float: "left" }}
                >
                  <Image
                    src={"/ceo.svg"}
                    width={"195px"}
                    height="250px"
                    alt=""
                    className="w-100"
                  />
                </div>
                <div>
                  <h4 className="font-4">Ngozi Nwabueze, Esq.</h4>
                  <h5 className="font-3 fw-bold">Co-Founder</h5>
                  <div className="mb-3">
                    <span className="me-3">
                      <Image
                        src={"/link.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                    <span className="me-3">
                      <Image
                        src={"/insta.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                    <span className="me-3">
                      <Image
                        src={"/twitter.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    Ngozi Nwabueze, Esq. fondly known as the ‘Mother-In-LAW’ of
                    businesses, is a Legal Practitioner who was called to the
                    Nigerian Bar in 2008. She has over 13 years’ experience
                    advising SMEs and Startups. She honed her legal skills at
                    Rocheba Solicitors between 2009 and 2012 before setting up
                    her fashion brand which ran between 2012 – 2017.
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    Whilst running her business, she also engaged in private
                    Legal Practise. Between 2018 and 2019, she dabbled into new
                    waters when she became the COO at TroggeUrban Ltd, a
                    technology development company, (now FirstFounders.cc, a
                    Startup Studio).
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    She currently is the CEO/CoFounder of Africa’s first fully
                    integrative Virtual Law Firm that offers affordable premium
                    Legal services and solutions to SMEs and StartUps;
                    PocketLawyers. Poised to become Africa&lsquo;s largest
                    Virtual Law Firm, the Firm currently has a growing number of
                    SMEs and Startups in its portfolio.
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    She has experience in advising businesses on Legal Business
                    Structure, Funding/Fundraising, Intellectual Property
                    Protection, Contract and Correspondence matters, Business
                    policy, Statutory/Regulatory Compliance, Staff Onboarding
                    and Outboarding, Growth Advisory and Taxation and more. She
                    also specializes in Property Law, advising clients on real
                    estate matters. She is currently building what will be the
                    future of how legal services and solutions will be offered
                    on the African continent.
                  </div>
                </div>
              </div>
            </div>
            <div className="py-5"></div>
            <div className="bg-13 p-3 p-lg-4 " style={{ borderRadius: "10px" }} data-aos="zoom-in-left">
              <div>
                <div
                  className="ms-3 ourteam mb-3 mb-lg-0"
                  style={{ float: "right" }}
                >
                  <Image
                    src={"/co-founder1.svg"}
                    width={"195px"}
                    height="250px"
                    alt=""
                  />
                </div>
                <div>
                  <h4 className="font-4">EMEKA EBENIRO</h4>
                  <h5 className="font-3 fw-bold">Co-Founder</h5>
                  <div className="mb-3">
                    <span className="me-3">
                      <Image
                        src={"/link.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                    <span className="me-3">
                      <Image
                        src={"/insta.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                    <span className="me-3">
                      <Image
                        src={"/twitter.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    Emeka Ebeniro is a Business and Brand Consultant, Personal
                    Branding and Transformation specialist, motivational
                    speaker, and exceptional content creator. He is an
                    activator, igniting the potential and gifts in people so
                    they can do and be much more. He is a certified Integrated
                    Marketing Communications Professional and Business
                    Consultant with over 13 years of experience in Brand
                    Management, Corporate Communications, and Organisational
                    Strategy.{" "}
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    Emeka has trained over 1000 creators, coaches, and leaders
                    across 5 continents and helped them build powerful and
                    profitable brands. Emeka is the convener of Personal
                    Branding Summit, Africa&lsquo;s No. 1 Global Summit on
                    Personal Branding.
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    Emeka is a Tech Entrepreneur and founder of KARAKURI, an
                    idea and innovation company, the brains behind
                    www.seatsandtickets.com- Nigeria&lsquo;s fastest-growing
                    ticketing and access platform. He has appeared on Inside
                    Business Africa, Connect Nigeria, Hot FM, The Nation
                    Newspaper, The Sun Newspaper, and other brands/ companies.
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    His mission is to help 1,000,000 individuals and businesses
                    optimise potential, increase visibility and grow their
                    income.
                  </div>
                </div>
              </div>
            </div>
            <div className="py-5"></div>
            <div className="bg-13 p-3 p-lg-4 " style={{ borderRadius: "10px" }} data-aos="zoom-in-right">
              <div>
                <div
                  className="me-3 ourteam mb-3 mb-lg-0"
                  style={{  float: "left" }}
                >
                  <Image
                    src={"/co-founder2.svg"}
                    width={"195px"}
                    height="250px"
                    alt=""
                  />
                </div>
                <div>
                  <h4 className="font-4">SEYI OMOLE</h4>
                  <h5 className="font-3 fw-bold">Co-Founder</h5>
                  <div className="mb-3">
                    <span className="me-3">
                      <Image
                        src={"/link.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                    <span className="me-3">
                      <Image
                        src={"/insta.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                    <span className="me-3">
                      <Image
                        src={"/twitter.svg"}
                        width="24px"
                        height={"24px"}
                        alt=""
                      />
                    </span>
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    Oluseyi Omole is a Brand and Marketing Strategist of
                    International repute with years of experience in helping
                    businesses start, level up, and scale in tech. He’s
                    passionate about technology, human psychology,
                    entrepreneurship, brand positioning, and taking ideas from
                    raw state to fruition.
                  </div>
                  <div className="h51 font-1 text-justify mb-3">
                    He has worked across diverse industries. Oluseyi was the
                    first African to hit 62 award medals (36 Golds, 16 Silvers,
                    and 10 Bronzes) in the international design contest working
                    on projects from top organisations like: UPS USA, Cambridge
                    University, Vitaliv Norway, City University Hong Kong, etc.
                    He has also consulted for Nigerian brands like Kraks TV,
                    Mamalette (CChub), and many more. Oluseyi has a BSc. Estate
                    Management from the University of Lagos.
                  </div>
                </div>
              </div>
            </div>
            <div className="py-5"></div>
          </div>
        </div>
      </div>
    </LayoutNoAuth>
  );
}
