/* eslint-disable @next/next/no-img-element */
import Image from "next/image";
import { Card } from "react-bootstrap";
import { HiArrowRight } from "react-icons/hi";
import styled from "styled-components";
import { CheckBoxx } from "../Ui";
import Link from 'next/link'
import CaseIcon from "../../assest/Image/caseIcon.svg";
import DropIcon from "../../assest/Image/droplet.svg";
import BackgroundImage from "../../assest/Image/dasboardBack.png";
import Button from "../Ui/Button";
import { MdArrowBackIos } from "react-icons/md";
import { dashBoardServices, request } from "../../utils";
import toast from "react-hot-toast";
import { useEffect, useState } from "react";
import RequestServiceImage from '../../assest/Image/Capture.PNG'
import moment from "moment";

const Style = styled.div`
  h3 {
    font-size: 30px !important;
    font-weight: 700 !important;
  }
  span {
    font-size: ${(props) =>
      props.SizeComp ? `${props.SizeComp}` : "24px"} !important;
  }

  p {
    font-size: 20px !important;
  }
  h4 {
    font-weight: 700 !important;
    font-size: 20px !important;
    color: #0d4447;
  }
  button {
    font-size: 20px !important;
  }
`;

const Style1 = styled.div`
  h3 {
    font-size: 30px !important;
    font-weight: 700 !important;
  }
  .subtitle {
    font-size: 20px !important;
    font-weight: 400 !important;
  }
  h3 {
    font-weight: 700;
  }
  h4 {
    font-weight: 600 !important;
    font-size: ${(props) =>
      props.SizeComp ? props.SizeComp : "24px"} !important;
    color: #0d4447;
  }
`;
export const RetailCard1 = ({ item, size }) => {
  return (
    <Style1 SizeComp={size}>
      <Card.Body>
        <div className="text-center py-3">
          <div className="mb-3">
            <Image src={item?.icon} alt="" height={80} width={80} />
          </div>
          <h4 className="text-type2">{item?.name}</h4>
        </div>
      </Card.Body>
    </Style1>
  );
};

export const RetailCard2 = ({ item, size }) => {
  return (
    <Style SizeComp={size}>
      <Card.Body>
        <div className="text-center py-3">
          <div className="mb-3">
            <div className=" mb-3 text-type2">{item?.title}</div>
            <p className="fw-normal h7 text-start text-type2">{item?.desc}</p>
            <Button
              className="px-3  bg-type1 text-white rounded"
              bntText={
                <div className="d-flex align-items-center justify-content-center">
                  <div className="small me-2">see more</div>{" "}
                  <i>
                    <HiArrowRight size={15} />
                  </i>
                </div>
              }
            />
          </div>
        </div>
      </Card.Body>
    </Style>
  );
};
export const RCard3 = ({
  item,
  index,
  size,
  setRetailServices,
  clientSubRetainSolution,
}) => {
  // if(item?.id === 4){
  const bola = [4, 14];
  //to find if client service exit with current services
  const check = clientSubRetainSolution?.find(
    (item2, index) => item2 === item?.id
  );
// //console.log(item,clientSubRetainSolution,'clientSubRetainSolution')
  return (
    // <div className="h-100 pointer" onClick={() => setRetailServices({index:index,...item})}>
    <div
      className="h-100 pointer"
      onClick={() =>
        item?.id === check
          ? setRetailServices({ index: index, ...item,status:true })
          :setRetailServices({ index: index, ...item,status:false })
          // : toast.error("You have not Subscribe for this service Yet")
      }
    >
      {/* {console.log(item,'item2')} */}
      <Card className={`rounded py-2 h-100  ${item?.id === check ? "" : "opacity-50"}`}>
        <Card.Body className="">
          <div>
            <div className="text-center mb-3">
              {/* {//console.log(item, "stateretainer")} */}
              <Image src={dashBoardServices[index]?.icon} alt="" />
            </div>
            <div className=" text-center fw-2">
              <span className="text-type2 h7">{item?.title}</span>
            </div>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
  //}
};
export const RCard4 = ({ item, size }) => {
  return (
    <div className="h-100 pointer">
      <Card className="rounded py-2 h-100">
        <Card.Body className="">
          <div>
            <div className="text-center mb-3"></div>
            <div className=" text-center fw-2">
              <div className="text-type2 h5 fw-bold">
                {/* {//console.log(item, "retailPackageMain")} */}
                {item?.title || item?.name?.split(" ")[0]}
              </div>
              <span className="text-type2 fw-0 h7">
                {" "}
                {item?.desc || item?.name?.split(" ")[1]}
              </span>
              <span className="text-type2 fw-0 h7">
                {" "}
                {item?.desc || item?.name?.split(" ")[2]}
              </span>
            </div>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
};

const Styled = styled.div`
  .profileImage {
    width: 100px;
    height: 100px;
    overflow: hidden;
    border-radius: 50%;
  }
  .profileImage img {
    height: 100%;
  }
`;

export const LawyerServiceComp = ({ data, bg, btnClassNamee, btnText }) => {

  const [user, setUser] = useState();

  useEffect(() => {
    if ((data)) {
      const getOtherUser = async () => {
        // //console.log( "stttststsst");
        try {
          const res = await request.get(
            `/get_user_profile/${data?.senderId}`
          );

          setUser(res?.data?.user);
        } catch (error) {
          // //console.log(error.response);

          const err = error?.response?.data?.errors
            ? error?.response?.data?.errors[0]
            : error?.response?.data?.message;
        }
      };
      getOtherUser();
    }
  }, [data]);
  
 
 
 // //console.log(user, data ,data?.subjectMatters ,JSON.parse(data?.members),'state')
 console.log(user,'state')
 


  return (
    <Styled className={`py-4 px-5 mb-4 rounded ${bg ? bg : "bg-white"}`}>
      <div>
      <div className=" flex-wrap justify-content-between align-items-center ">
      <div className="d-flex flex-wrap justify-content-between">
      <div className="profileImage me-5 mb-3 mb-md-0">
          {/* pic_name */}
            <img src={GeneralUrl+user?.pic_name} alt="" />
          </div>
          <div className="me-4 me-5 mb-3 mb-md-3">
            <h5 className="mb-3">{user?.first_name} {user?.last_name}</h5>
            <ul className="mx-0 p-0">
              {/* {//console.log((JSON.parse("hello")),'state22')} */}
              <li>{data?.serviceName}</li>
              {/* {//console.log(JSON.parse(data?.subjectMatter),'JSON.parse(data?.subjectMatter)')} */}
              {JSON.parse(data?.subjectMatter)?.map((item, index) => (
                <li key={index} className="h9">
                  {item}
                
                </li>
              ))}
            </ul>
          </div>
         <div className="d-flex align-items-end justify-content-end">
         <div>
          <Link href='/dashboard/lawyer/message'>
            <a className="px-8 py-2 py-md-2 fw-bold text-white bg-type1 rounded h5 ">{btnText}
              </a></Link>
          
        </div>
         </div>
      </div>
      </div>
      </div>
    </Styled>
  );
};

const Styled2 = styled.div`
  .left {
    border-left: 4px solid
      ${(props) => (props.status == 1 ? "#4CAF50" : "#ED6710")};
  }
`;

export const LawyerReceiptComp = ({ data }) => {
  return (
    <Styled2 status={data?.status}>
      <div className="border ps-1 py-1 rounded mb-3">
        <div className=" left py-3 px-3">
          <div className="d-flex justify-content-between">
            <span
              className={`h9 fw-2 ${data?.payment_status === "Complete"  ? "text-6" : "text-9"} `}
            >
              {data?.payment_status === "Complete"  ? "Collected" : "Pending"}
            </span>
            <span className="h9">{ moment(data?.createdAt).format('L')}</span>
          </div>
          <span className="h9  fw-2 lh-1">{data?.service?.title} </span>
          <div className="d-flex justify-content-between mt-1">
            <span className="h9 fw-1 text-muted">
              Receipt Invoice {data?.id} 
            </span>
            <span
              className={`h6 ${
                data?.status === 1 ? "text-6" : "text-9"
              } fw-bold`}
            >
              ₦{data?.amount}
            </span>
          </div>
        </div>
      </div>
    </Styled2>
  );
};

const Styled3 = styled.div`
  .profileWrapper {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    overflow: hidden;
  }
  img {
    height: 100%;
  }
  .border {
    border-left: 8px solid
      ${(props) => (props.status == 1 ? "#0D4447" : "#ED6710")}!important;
  }
`;

export const LawyerShedularComp = ({ data }) => {
  return (
    <Styled3 status={data?.status}>
      <div className="d-flex border py-4 px-3 mb-4 rounded justify-content-between bg-type3 ">
        <div>
          <h6 className="text-type1 fw-2">{data?.name}</h6>
          <div className="h6 text-muted mb-2">{data?.time}</div>
          <div className="profileWrapper">
            <img src={data?.image} alt="" />
          </div>
        </div>
        <div>
          <Image src={data?.status === 1 ? DropIcon : CaseIcon} alt="" />
        </div>
      </div>
    </Styled3>
  );
};

export const RCard5 = ({
  items = [],
  HandleClose,
  size,
  setRequesting,
  setShowLawyer,
  fetchLawyerForSpecializedServicefunc,
  state,
  setRetailSolutionID
}) => {
  return (
    <div className="h-100" >
      {/* {//console.log(items,'itemsitemsitems')} */}
      {/* {console.log(items,'retainerPackage')} */}
      {items?.status ? 
      <div className="bg-white py-4 px-3  position-relative" style={{overflow:'hidden'}}>
      <div className="back">
        <Image src={BackgroundImage} alt="" />
      </div>
      <div className="d-flex ">
        <MdArrowBackIos
          size={35}
          className="text-type1 pointer"
          onClick={HandleClose}
        />

        {/* {//console.log(items?.id, dashBoardServices[items?.id],'socketItem')} */}
        <div className="w-100 text-center">
         {/* {items?.id && <Image src={dashBoardServices[items?.id]?.icon1} alt="" /> }  */}
        </div>
      </div>
      <div className=" position-relative">
        <div className="mb-5">
          <h4 className="text-center text-type2 fw-2">{items?.title}</h4>
        </div>
        <div>
          <div className="mb-5">
            {/* {//console.log(
              JSON.parse(items?.subjectMatters),
              "items?.subjectMatters"
            )} */}
            {JSON.parse(items?.subjectMatters)?.map((item, index) => (
              <div key={index} className="row justify-content-center">
                <div className="col-12 col-lg-10 col-xl-8">
                  {" "}
                  <CheckBoxx label={item} />
                </div>
              </div>
            ))}

            <div className="text-center">
              <u>
                <a className="text-type1 fw-1">Learn More</a>
              </u>
            </div>
          </div>
          <div className="text-center">
            {items?.status ? 
            <Button
            bntText={"Request  Service"}
            loading={state?.isLoading}
            disabled={state?.isLoading}
            onClick={() => {
             // items?.status 
              setRetailSolutionID(items)
              // setShowLawyer(true);
              fetchLawyerForSpecializedServicefunc(items?.id)
            }}
            className="py-3 h4 px-5 bg-type2 text-white rounded"
          />
        :
        <Button
              bntText={"Request  Service"}
              loading={state?.isLoading}
              disabled={state?.isLoading}
              onClick={() => {
              
              }}
              className="py-3 h4 px-5 bg-type2 text-white rounded"
            />}
            
          </div>
        </div>
      </div>
    </div>
    :
    <div className="bg-white py-4 px-3  position-relative">
    <div className="back">
      <Image src={BackgroundImage} alt="" />
    </div>
    <div className="d-flex ">
      <MdArrowBackIos
        size={35}
        className="text-type1 pointer"
        onClick={HandleClose}
      />

      <div className="w-100 text-center">
      
      </div>
    </div>
    <div className=" position-relative">
      <div className="mb-5">
        <h4 className="text-center text-type2 fw-2">{items?.name}</h4>
      </div>
      <div>
        
        <div className="mb-5">
         
            <h3 className="fw-2 text-center text-type2">Retail solution not <br/> available.</h3>
<p className="text-center mb-8">
Ah dang, it looks like you do not have access to this service. You can get access by going to subscription and upgrading to package that includes the service.
<br/>
or
<br/>
Request service for the retainer solution only.
        
</p>
<div>
<Image src={RequestServiceImage} alt=""/>
</div>
          <div className="text-center">
            <u>
              <Link href={"/dashboard/client/subcription/1"}>
              <a className="text-black fw-1">Go to subscription</a>
              </Link>
            </u>
          </div>
        </div>
       
      </div>
    </div>
  </div>  
    }
    </div>
  );
};
