import React, { useState } from 'react'
import { Form } from 'react-bootstrap';
import { MultiSelect } from 'react-multi-select-component'
import styledComponents from 'styled-components';

const Style =styledComponents.div`

span{

    font-size: 18px;
    font-weight: 600;
}
.rmsc .dropdown-heading{
    padding-top: 2rem!important;
    padding-bottom: 2rem!important;
}

`
const options = [
    { label: "Grapes 🍇", value: "grapes" },
    { label: "Mango 🥭", value: "mango" },
    { label: "Strawberry 🍓", value: "strawberry", disabled: true },
  ];
export  const MutipleSelect =({setSelected,selected=[],handleOnChange,label,required,labelClassName,arrayData=[],showError,name}) =>{
    

    // const handleOnChange = () => {
    //     MutipleSelectArray(selected)
    // }
  return (
    <Style>
        <div>
        {label && <Form.Label  className={`fw-2 mb-2 ${labelClassName}`}>{label} {required && <span className="text-danger">*</span>}</Form.Label> }
       <div>
       <MultiSelect
        options={arrayData}
        value={selected}
        onChange={setSelected}
        labelledBy="Select"
        className=''
      />

       </div>
       {selected?.length===0 && showError &&<small className='text-danger text-capitalize fw-2'>{`${name} is required....` }</small>} 

      </div>
    </Style>
  )
}
