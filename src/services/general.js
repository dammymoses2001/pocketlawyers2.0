import { request } from "../utils";

export const getAllCountries = async () => {
  try {
    const res = await request.get("/countries");
    // //console.log(res, "product user");
    return res?.data;
  } catch (error) {
    const err = error?.response?.data?.message || error?.message;
    throw new Error(err);
  }
};

export const getAllState = async () => {
  try {
    const res = await request.get("/states");
    // //console.log(res, "product user");
    return res?.data;
  } catch (error) {
    const err = error?.response?.data?.message || error?.message;
    throw new Error(err);
  }
};

export const getRetainerPackage = async () => {
  try {
    const res = await request.get("/retainer_packages");
    // //console.log(res, "product user");
    return res?.data;
  } catch (error) {
    const err = error?.response?.data?.message || error?.message;
    throw new Error(err);
  }
};

export const getRetainerPackageSubscription = async (id) => {
  try {
    const res = await request.get(`/retainer_packages_plan/${id}`);
    // //console.log(res, "product user");
    return res?.data;
  } catch (error) {
    const err = error?.response?.data?.message || error?.message;
    throw new Error(err);
  }
};

export const getCampus = async () => {
  try {
    const res = await request.get(`/law_school_campuses`);
    // //console.log(res, "product user");
    return res?.data;
  } catch (error) {
    const err = error?.response?.data?.message || error?.message;
    throw new Error(err);
  }
};

export const getLawyerSpecialization = async () => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.get("/area_of_specializations");
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

export const subscriptionToRetainerPackage = async (id) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.post(`/subscribe/${id}`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

export const getRetailSolution = async () => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.get(`/retail_solutions`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//SPECIALIZED
export const fetchLawyerForSpecializedService = async (serviceId) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.get(`/get_service_lawyers/${serviceId}`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

// GET CLIENT SUBSCRIPTION SERVICE

export const getClientSubService = async (serviceId) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.get(`/client_services`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//UPDATE USER PROFILE
export const updateUserProfilePic = async (pic) => {
  ////console.log.log(emailAddress, password, "servicelogin");
  try {
    const res = await request.post(`/update_profile_pic`, pic);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//INITIATE CONVERSATION WITH LAWYER ->initiate_conversation/lawyerId/retailsolutionid
export const initiateConversation = async (ids, subjectMatters) => {
  //console.log(ids,subjectMatters, "servicelogin");
  try {
    const res = await request.post(
      `/initiate_conversation/${ids?.lawyerId}/${ids?.retailsolutionId}`,
      subjectMatters
    );
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};
//CLIENT GET- CONFIRM THAT KYC FORM WAS COMPLETED
export const kyc = async () => {
  try {
    const res = await request.get("/kyc");
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//CLIENT - POST CONFIRM THAT KYC FORM WAS COMPLETED
export const kycpost = async (value) => {
  try {
    const res = await request.post("/kyc", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//CLIENT - POST CONFIRM THAT KYC FORM WAS COMPLETED
export const updateKycPost = async (value) => {
  try {
    const res = await request.put("/kyc", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};
//CREATE SUPPORT
export const support = async (value) => {
  try {
    const res = await request.post("/support", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//Update BankDetails
export const updateBankDetails = async (value) => {
  try {
    const res = await request.put("/bank", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};
//Update BankDetails
export const AddBankDetails = async (value) => {
  try {
    const res = await request.post("/bank", value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//CREATE SUPPORT
export const verifyAccount = async (email, code) => {
  try {
    const res = await request.get(`auth/verify/${email}/${code}`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

//Request Reset
export const restPassword = async (value) => {
  try {
    const res = await request.post(`auth/password_reset`, value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};
export const changePassword = async (value) => {
  try {
    const res = await request.post(`auth/reset_password`, value);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

export const getLawyerPayment = async () => {
  try {
    const res = await request.get(`lawyerPaymentRecord`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

export const lawyerAvailablity = async () => {
  try {
    const res = await request.put(`isLawyerAvailable`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

// export const resendPassword= async (value) => {

//   try {
//     const res = await request.post(`auth/password_reset`,value);
//     return res?.data;
//   } catch (error) {
//     //console.log(error.response);

//     const err = error?.response?.data?.errors ?error?.response?.data?.errors[0] : error?.response?.data?.message;

//     throw new Error(err);
//   }
// };

//CLIENT - GET ALL ONE TIME RETAIL SOLUTIONS HISTORY
export const getUserRetainSolutionSubscription = async () => {
  try {
    const res = await request.get(`onetimeRetainerPackageHistory/All/`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};
export const getPress = async () => {
  try {
    const res = await request.get(`press`);
    return res?.data;
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

export const getNewsFeeds = async () => {
  try {
    const res = await request.get(`newsfeeds`);
    return res?.data;
    // console.log('getNewsFeeds',res?.data)
  } catch (error) {
    //console.log(error.response);

    const err = error?.response?.data?.errors
      ? error?.response?.data?.errors[0]
      : error?.response?.data?.message;

    throw new Error(err);
  }
};

