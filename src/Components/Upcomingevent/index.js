import Image from "next/image";
import styledComponents from "styled-components";

import UserImage from "../../assest/Image/icon.svg";
const Style = styledComponents.div`
border-left: 6px solid red; 
.imageWrapper{
    width: 50px;
    height: 50px;
    border-radius: 50%;
    overflow: hidden;
}
`;
export const UpcomingEvent = ({ title, time, image }) => {
  return (
    <Style>
      <div className=" py-3 bg-white mb-3">
        <div className="row">
          <div className="col-8">
            <div className="ms-4">
              {title && <h6 className="fw-0 text-type1 mb-0">{title}</h6>}
              {time && <span className="h9">{time}</span>}
            </div>
          </div>
          {image && (
            <div className="col-4">
              <div className="imageWrapper">
                <Image layout="responsive" src={UserImage} alt="" />
              </div>
            </div>
          )}
        </div>
      </div>
    </Style>
  );
};
