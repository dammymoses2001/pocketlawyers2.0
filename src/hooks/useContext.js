import { useContext } from "react";
import {AuthContext } from "../Context/auth/provider";

export const useAuth = () => {
  return useContext(AuthContext);
};
