/* eslint-disable @next/next/no-img-element */
import  { useState } from 'react'
import styled from 'styled-components'

const ResuableImageContainerStyle =styled.div`
width:${props=>props?.Width?props?.Width:"100px"} !important;
height:${props=>props?.Height?props?.Width:"100px"} !important;

img{
  object-fit:cover;
}
`

export const ResuableImageContainer =({picture,Width,Height,newlySlectedImage})=> {
    const [src,setSrc] =useState(picture)
  return (
    <ResuableImageContainerStyle  Width={Width} Height={Height}>
         <img src={newlySlectedImage ||src} alt="" onError={(e)=>setSrc("https://res.cloudinary.com/dammymoses/image/upload/v1638464947/avatars/avatar_ekgiia.jpg")} />
    </ResuableImageContainerStyle>
  )
}
