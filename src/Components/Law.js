/* eslint-disable @next/next/no-img-element */
import React from 'react'

export const Law =()=> {
  return (
    <div className='container'>
        <div className='row align-items-center g-5'>
            <div className='col-lg-5'>
                <div data-aos="flip-left">
                    <picture>
                    <img src='/law.svg' className='w-100 rounded-3' alt='' />
                    </picture>
                </div>
            </div>
            <div className='col-lg-7 ' data-aos="fade-up" data-aos-delay="500">
                <h5 className='text-9 text-uppercase fw-2 font-3'>Who we are</h5>
                <h5 className=' col-10 col-lg-9 font-3 pe-lg-4 mb-3' style={{fontWeight:'900'}}>
                PocketLawyers is a virtual law firm poised to democratize access to premium legal solutions for SMEs and Startups. 
                </h5>
                <h6 className='fw-0 text-decoration-underline'>Read More</h6>
            </div>
        </div>
        <div className='py-3 py-lg-4'></div>
    </div>
  )
}
