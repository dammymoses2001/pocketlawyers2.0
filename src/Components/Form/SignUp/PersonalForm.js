import styled from "styled-components";
import Logo from "../../../assest/Image/loginImage.svg";
import Image from "next/image";
import { InputComp, RadioComp, SelectComp } from "../../Ui";
import Button from "../../Ui/Button";
import Link from "next/link";
import { useEffect, useState } from "react";
import { TitleData } from "../../../utils";
import {useRouter} from 'next/router'
import toast from "react-hot-toast";

const Style = styled.section`
  padding: 5rem 0 15rem 0;
  .form-width {
    width: 70%;
  }
  .d-inline {
    border-bottom: 2px solid;
  }
  @media (max-width: 992px) {
    .form-width {
      width: 90% !important;
    }
  }
  @media (max-width: 786px) {
    .form-width {
      width: 90% !important;
    }
  }
`;
export const PersonalForm = ({ setSignupLevel, handleOnChange,setgetStateid, userdata,getStateFunc,state,showError,setShowError }) => {
  useEffect(() => {
    getStateFunc()
  }, [getStateFunc])
  const router = useRouter()

  const [level, setLevel] = useState(0);
  const {title,first_name,last_name,email,phone_number,dob,city,state_of_origin,gender} =userdata;
  const checkFill =()=>{
    return  !title ||!first_name||!last_name||!email||!phone_number||!dob||!city||!state_of_origin||!gender
  }

 
  return (
    <Style>
      <div className="container  text-black">
        <div className="d-flex justify-content-center">
          <form className="form-width">
            <h5 className=" d-inline ">Personal Information</h5>

            <div className="mt-5">
              <div>
                <div className="row">
                  <div className="col-4 col-lg-2">
                    <SelectComp
                      label={"Title"}
                      arrayData={TitleData}
                      input="name"
                      name="title"
                      value={userdata?.title}
                      onChange={handleOnChange}
                      showError={showError}
                    />
                  </div>
                  <div className="col-12 col-lg-5">
                    <InputComp
                      label={"First Name"}
                      placeholder="First Name"
                      name="first_name"
                      className={"py-3"}
                      value={userdata?.first_name}
                      onChange={handleOnChange}
                      showError={showError}
                    />
                  </div>
                  <div className="col-lg-5">
                    <InputComp
                      label={"Last Name"}
                      placeholder="Last Name"
                      name="last_name"
                      className={"py-3"}
                      value={userdata?.last_name}
                      onChange={handleOnChange}
                      showError={showError}
                    />
                  </div>
                </div>
              </div>
              <div>
                <div className="row">
                  <div className=" col-lg-6">
                    <InputComp
                      label={"Email Address"}
                      placeholder="Enter  email address"
                      name='email' value={userdata?.email} onChange={handleOnChange}
                      className={"py-3"}
                      showError={showError}
                    />
                  </div>
                  <div className="col-lg-6">
                    <InputComp
                      label={"Phone Number"}
                      placeholder="e.g +234 123 456 7890"
                      name='phone_number' value={userdata?.phone_number} onChange={handleOnChange}
                      className={"py-3"}
                      showError={showError}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className=" col-lg-6">
                    <div className="pe-lg-5">
                      <InputComp
                        label={"Date of Birth"}
                        typeComp="date"
                        className={"text-uppercase py-3"}
                        name='dob'
                        value={userdata?.dob} onChange={handleOnChange}
                        showError={showError}
                      />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <label className="form-label  text-capitalize mb-4 fw-2">
                      Gender
                    </label>
                    <div className="row">
                      <div className="col-6 col-lg-5">
                        <RadioComp label={"Male"} id="gender"  name='gender'
                        value={"M"} checked={userdata?.gender === 'M'} onChange={handleOnChange}/>
                      </div>

                      {/* <input className="form-check-input" type="radio" value={"hello"} name="gender" id="flexRadioDefault1" onChange={(e)=>//console.log(e.target.value)}/> */}
                      <div className="col-6 col-lg-5">
                        <RadioComp label={"Female"} id="gender" name='gender'
                        value={"F"} onChange={handleOnChange} checked={userdata?.gender === 'F'}/>
                      </div>
                    </div>
                    {!gender && showError &&<small className='text-danger text-capitalize fw-2'>{`${"Gender"} is required....` }</small>} 

                  </div>
                </div>
                <div className="row">
                  <div className=" col-lg-12">
                    <InputComp
                      label={"Permanent Home Address"}
                      placeholder=" address"
                      name='home_address'
                      value={userdata?.home_address} onChange={handleOnChange}
                      className={"py-3"}
                      showError={showError}
                    />
                  </div>
                </div>
                <div className="row mb-5">
                  <div className=" col-lg-6">
                    <InputComp label={"City"} placeholder="City" 
                         name='city'
                         value={userdata?.city} onChange={handleOnChange}
                         className={"py-3"}
                         showError={showError}
                    />
                    
                  </div>
                  <div className="col-lg-6">
                  <SelectComp
                      label={"State"}
                      arrayData={state}
                      input="name"
                      name="state_of_origin"
                      value={userdata?.state_of_origin}
                      selectText='Select State'
                      onChange={(e)=>{
                        //console.log(e.target.selectedIndex)
                        handleOnChange(e)
                        setgetStateid(e.target.selectedIndex)
                       
                      }
                      }
                      showError={showError}
                    />
                    
                  </div>
                </div>
              </div>
            </div>
            <div className="text-center px-10">
              <Button
                bntText={"Proceed"}
                className="w-100 py-3 h4 fw-bolder bg-type2 text-white mb-3"
                onClick={(e) => {
                  e.preventDefault()
                  if(checkFill()){
                    setShowError(true)
                    return toast.error('All field are Mandatory ')
                  }
                  setSignupLevel(1)}}
              />

            </div>
            <div className="text-center px-10">
            <Button  onClick={()=>router.push('/signup')}   bntText={'Go back'} className='w-100 py-3 h4 fw-bolder bg-8 text-black border mb-3'/>

            </div>
            

          </form>
        </div>
      </div>
    </Style>
  );
};
