/* eslint-disable @next/next/no-img-element */
import  { useEffect, useState } from 'react'
import {AiTwotoneStar} from 'react-icons/ai'
import { defaultImage, endPoint, GeneralUrl, request } from '../../utils';
import { Loader } from '../Loader';
import Button from '../Ui/Button'
import Image from 'next/image'

export  const GetLawyerProfile =({LawyerId,retailSolutionId,state,IntiateLawyerFunc})=> {
    const [user, setUser] = useState();

    

    useEffect(() => {
        if(LawyerId){
            //console.log('LawyerId2',LawyerId)
          const getOtherUser = async () => {
            //console.log('LawyerId3',LawyerId)
            // //console.log( "stttststsst");
            try {
                //console.log('LawyerId4',LawyerId)
              const res = await request.get(`/lawyerProfile/${LawyerId}`);
              //console.log(res?.data ,'LawyerId')
              setUser(res?.data?.lawyer);
            } catch (error) {
              //console.log(error.response);
    
              const err = error?.response?.data?.errors
                ? error?.response?.data?.errors[0]
                : error?.response?.data?.message;
            }
          };
          getOtherUser();
        }
        
      }, [LawyerId]);

      //console.log(user,LawyerId ,retailSolutionId,'LawyerId')

      const handleAreaofSpeclization = (user,retailSolutionId) => {
         const AOS = user?.area_of_specializations.find((item)=>item?.id === retailSolutionId?.id)
         return AOS?.name
        //  //console.log(AOS,'LawyerID')
      }

      const handleInitialConversation = () => {
        const ids = {
          lawyerId:user?.id,
          retailsolutionId:retailSolutionId?.id
        }
        const subjectMatters ={subjectMatters:retailSolutionId && JSON?.parse(retailSolutionId?.subjectMatters)};
        //console.log(ids,subjectMatters,'ids,subjectMatters')

        IntiateLawyerFunc(ids,subjectMatters)
      }
  return (
    <div>
        <div>
          <div className="py-3 bg-type2 d-flex flex-wrap px-3 mb-3 rounded">
         <div className="lawyerWrapper  me-3">
           {/* {console.log(user,'lawyerpic')} */}
         <img src={user?.pic_name ? `${GeneralUrl}${user?.pic_name}`:defaultImage} alt=''/>
         </div>
         {user?.first_name ? 
         
         <div className="text-white me-3 mb-3 mb-sm-0">
         <h6>{user?.first_name} {user?.last_name}</h6>
         <p className="mb-1">{handleAreaofSpeclization(user,retailSolutionId)}</p>
         <p className="mb-0">10 Yrs of Experience</p>
         <div><AiTwotoneStar className="text-type1"/> <AiTwotoneStar className="text-type1"/></div>
       </div>:
     <div className='text-center col-4'>  <Loader height={'5vh'} color='white'/></div>
        }
         
         <div className="d-flex align-items-end"><Button bntText={"Connect"} onClick={handleInitialConversation} className={"bg-type1 text-white px-4 py-1"} /></div>
          </div>

          {/* <div className="py-3 bg-type2 d-flex px-3 mb-3">
         <div className="lawyerWrapper  me-3">
         <img src={"https://3.bp.blogspot.com/-Tj9no9Jt84U/UPGUmN3z54I/AAAAAAAAGYM/skaNHlAPPvw/s1600/156975.jpg"} alt=''/>
         </div>
         <div className="text-white">
           <h6>Bola Ogunde</h6>
           <p className="mb-1">Business Law</p>
           <p className="mb-0">10 Yrs of Experience</p>
           <div><AiTwotoneStar className="text-type1"/> <AiTwotoneStar className="text-type1"/></div>
         </div>
          </div> */}
        </div>
    </div>
  )
}
