/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import { GeneralUrl, request } from "../../utils";
import { GoPrimitiveDot } from "react-icons/go";
import { ResuableImageContainer } from "../Ui";

export const ListUsersLawyer = ({
  message,
  conversation,
  userProfile,
  getOtherUserProfile,
  setGetSelectedMessages,
  getSelectedMessages,
  state,
  otheruser,
  setMessage,
  setSendMessage,
  activeUser,
  arrivalMessage
}) => {
  const [user, setUser] = useState();

  useEffect(() => {
    if (JSON.parse(conversation?.members)[0]) {
      const getOtherUser = async () => {
        //console.log("stttststsst");
        try {
          const res = await request.get(
            `/get_user_profile/${JSON.parse(conversation?.members)[0]}`
          );

          setUser(res?.data?.user);
        } catch (error) {
          //console.log(error.response);

          const err = error?.response?.data?.errors
            ? error?.response?.data?.errors[0]
            : error?.response?.data?.message;
        }
      };
      getOtherUser();
    }
  }, [conversation, otheruser]);

  //  console.log(getSelectedMessages[getSelectedMessages.length-1]?.text ===arrivalMessage?.text,'checking')
  //   console.log(conversation)

  return (
    <div>
      <div
        className="px-3 py-3 bg-white rounded mb-3 pointer"
        onClick={() => {
          setMessage({ user, conversation });
          setGetSelectedMessages(conversation?.messages);
          setSendMessage("");
        }}
      >
        <div className="d-flex">
          <div className="imageWrapperMessage me-3">
            
           {user?.pic_name && <ResuableImageContainer Width={"100%"} Height={"100%"} picture={(GeneralUrl+user?.pic_name)}/>
            
          }
          </div>
          <div className="">
            <div className="h6 fw-bold">
              {user?.first_name} {user?.last_name}{" "}
              {activeUser?.map((item, index) =>
                item?.userId === user?.id ? (
                  <GoPrimitiveDot  color="green" size={20} key={index}/>
                ) : null
              )}
            </div>
            <div className="h6 fw-0">
              {conversation?.serviceName.substring(0, 10) + "..."}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
