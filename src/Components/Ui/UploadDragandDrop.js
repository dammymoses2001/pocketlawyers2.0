import  {useCallback, useState} from 'react'
import {useDropzone} from 'react-dropzone'
import UploadImage from '../../assest/Image/Upload.svg'
import Image from 'next/image'
import styledComponents from 'styled-components'


const Style = styledComponents.div`
.upload{
    border:2px dotted #C4C4C4;
}
`

export const DrapandDropUpload =({label,required,fileUpload,fileName,showError,name})=> {
  const [picName,setPicName]=useState('')
  const onDrop = useCallback(acceptedFiles => {
      //console.log(acceptedFiles,'isDragActive')
      fileUpload(acceptedFiles[0])
      setPicName(acceptedFiles[0]?.name)
    // Do something with the files
  }, [fileUpload])
  // console.log(fileName)

  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <Style className='mb-3' {...getRootProps()}>
          <label className="form-label  text-capitalize mb-2 fw-2">
            {label} {required && <span className="text-danger">*</span>}
          </label>
        <div className='upload py-3'>
      <input {...getInputProps()} 
      accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.pdf" />
     
      {
          
        isDragActive ? 
          <p className='text-center text-success'>{picName ||fileName?.name}</p> : <p className='text-center text-success'> {picName ||fileName?.name}</p>
          
      }
      <div className='text-center'>
          <Image src={UploadImage} alt=''/>
          <p>Drag and Drop Files Here</p>
      </div>
      {!fileName?.name && showError &&<small className='text-danger text-capitalize fw-2'>{`${name} is required....` }</small>} 

      </div>
    </Style>
  )
}