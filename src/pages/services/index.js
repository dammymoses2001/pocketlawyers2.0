import React from 'react'
import { ServicesComp } from '../../Components'
import { LayoutNoAuth } from '../../Layout'


export default function index() {
  return (
    <LayoutNoAuth Title={"Services"}>
     <ServicesComp/>
    </LayoutNoAuth>
  )
}
