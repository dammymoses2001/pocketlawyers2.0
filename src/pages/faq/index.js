/* eslint-disable react/no-unescaped-entities */

import { useState } from "react";
import { FiMinus, FiPlus } from "react-icons/fi";
import styled from "styled-components";
import { Accordiance } from "../../Components";
import { LayoutNoAuth } from "../../Layout";
import { FaqData } from "../../utils";

export default function Index() {
const [select,setSelect] =useState()
  return (
    <LayoutNoAuth Title={"FAQs"}>
        <DivStyle>
        <div className="row justify-content-center">
          <div className="col-9">
          <div className="my-3 py-5 py-lg-5  my-lg-5"></div>
            <div className="mb-5">
            <h5 className=" fw-1 ">Frequently Asked Questions?</h5>
            <h3 className="h3 font-4 text-type1">PocketLawyers FAQ</h3>
            </div>
          </div>
        <div className="col-lg-8">
      
            
      <div className="container">
            <div></div>
            {FaqData?.map((item,index)=>
             <div key={index} className="font-3">
             <Accordiance
             id={index}
             display={"none"}
             Accordiontitle={<div className="d-flex align-items-end fw-2 font-1 h51"><span className="dot me-3 p-1"><FiPlus color="#fff"/></span>{item?.title}</div>}
             AccordionBody={
                 <div className="d-flex align-item-center ms-3">
                     <div className="me-2 px-1 rounded py-4 my-2 bg-type1"></div>
                     <div className="w-100 lh-base fw-3 text-justify" dangerouslySetInnerHTML={{__html: item?.desc}}>
                     </div>
                 </div>
             }
             />
             <hr className="my-2 bg-type4"/>
            
            
         


             
         </div>
            )}
           <div className="py-5 my-5"></div>
      </div>
      </div>
      </div>
      </DivStyle>
    </LayoutNoAuth>
  );
}

const DivStyle = styled.div`
 
`;
