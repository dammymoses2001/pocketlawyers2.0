import { Accordion } from "react-bootstrap";
import styled from "styled-components";

const AccordianceStyle = styled.div`
  .accordion-button:not(.collapsed) {
    color: #000;
    background-color: transparent;
  }
  .accordion-button::after{
display:${props=>props?.Display?props?.Display:""} !important;
  }
  .accordion-item,.accordion,.accordion-header button{
    background:transparent !important;
  }
  p{
    text-align: justify;
    font-weight: 600;
    line-height: 25px;
  }
  hr:not([size]) {
    height: 0.2px;
}
.accordion-button{
  align-items:start !important;
}
`;

export const Accordiance = ({ Accordiontitle, AccordionBody, id,display }) => {
  return (
    <AccordianceStyle defaultActiveKey={id} Display={display}>
      <Accordion defaultActiveKey="0" className="border-0" style={{background:'transparent'}}>
        {Accordiontitle && (
          <Accordion.Item eventKey={id} className="border-0">
            <Accordion.Header>{Accordiontitle}</Accordion.Header>
            <Accordion.Body className='py-0 my-0'>{AccordionBody}</Accordion.Body>
          </Accordion.Item>
        )}
      </Accordion>
    </AccordianceStyle>
  );
};
