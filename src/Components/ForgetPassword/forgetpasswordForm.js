import VerficationIcon from "../../assest/Image/forgetpassword.svg";
import Image from "next/image";
import { InputComp2 } from "../../Components";
import Button from "../../Components/Ui/Button";
export const ForgetpasswordForm = ({ setToogle,handleOnchange,handleSubmitEmail,isLoading }) => {



  return (
    <div>
      <div className="container d-flex justify-content-center mb-5">
        <div className="col-10 col-lg-8 col-xl-4">
          <div className="text-center mb-3">
            <Image src={VerficationIcon} alt="" />
          </div>
          <div className="text-center mb-3">
            <h5 className="mb-3">FORGET PASSWORD</h5>
            <div className="text-type2 mb-5 h10">
              Enter your registered email address below & we will send an email
              with instructions to reset your password.
            </div>
          </div>
          <div className="">
            <div className="mb-5">
              {" "}
              <InputComp2
                label={"Email Address"}
                labelSize="17px"
                bordercolor="#C4C4C4"
                className={"py-2"}
                name='email'
                onChange={handleOnchange}
              />
            </div>
            <div className="mt-5 mb-3">
              <Button
                bntText={isLoading?"Loading...":"Reset password"}
                onClick={handleSubmitEmail}
                className="bg-type1 text-white h5 fw-bold py-3  w-100 px-3 rounded"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
