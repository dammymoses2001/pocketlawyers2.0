
import { useEffect, useState } from 'react'
import toast from 'react-hot-toast'
import { initializedState } from 'react-slick/lib/utils/innerSliderUtils'
import styledComponents from 'styled-components'
import { LawyerProfileForm,AdditionalInformation,LawyerEducationalForm, CreatePassword, LawyerCreatePassword } from '../../Components'
import { useAuth } from '../../hooks/useContext'
import { LayoutNoAuth } from '../../Layout'
import { HowManyYears, retailserviceData2, Years } from '../../utils'


const Style = styledComponents.div`
padding:3rem 0 0 0;
.one::before{

}
.inline,.wrapper{
  border: 1px solid;
    border-radius: 50%;
    height: 25px;
    width: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
    
    color:white;
}
 span{
  font-size: 13px;
  z-index: 90;
}
.inline{
  z-index: 10;
}
.switch1{
  background: #218698;
}
.switch2{
  background: #C4C4C4;
}
.two::before{
  
   border-top: 2px dotted #C4C4C4;
  content:"";
  margin: 0 auto; /* this centers the line to the full width specified */
  position: absolute; /* positioning must be absolute here, and relative positioning must be applied to the parent */
  top: 50%; left: 0; right: 0; bottom: 0;
  width: 95%;
  z-index: 0;

}
.mx-50{
  // width:70%;
}

@media (max-width: 540px) {
  padding:1rem 0 0 0  !important;
  margin: 0rem 10px !important;
}
`

export default function Lawyer_signup() {
  const {getState,getStateFunc,getCampusFunc,state,getlawyerSpecalizationFunc,getCountriesFunc,getCountries,RegisterLawyerComp} =useAuth()
    const [signupLevel,setSignupLevel]=useState(0)
    const [showError,setShowError] =useState(false)
    const [getLawyerSpecialization,setLawyerSpecialization]=useState()
    const [getCountriesData,setCountries]=useState()
    const [professionalBody,setProfessionalBody]=useState([])
    const [otherdegree,setOthersDegree]=useState([])


    useEffect(() => {
      getStateFunc();
      getCampusFunc()
      getlawyerSpecalizationFunc()
      getCountriesFunc()
    }, [getStateFunc, getCampusFunc, getlawyerSpecalizationFunc, getCountriesFunc])


    
    useEffect(() => {
      const array = [];
      state?.lawyerspecialization?.map((item,index)=>{
        array.push({
          label:item?.name, value:item?.id 
        })
      })
      setLawyerSpecialization(array)
    }, [setLawyerSpecialization, state?.lawyerspecialization])
    

    useEffect(() => {
      const array = [];
      getCountries?.countries?.map((item,index)=>{
        array.push({
          label:item?.name, value:item?.name 
        })
      })
      setCountries(array)
    }, [getCountries?.countries, setCountries])
    //User Saving Details
    const intialState ={
      title:'',
      email:'',
      password:'',
      confirm_password:'',
      first_name:'',
      last_name:'',
      gender:'',
      dob:'',
      city:'',
      state_of_origin:'',
      learning_institution:'',
      grad_year:'',
      law_school_campus:'',
      phone_number:'',
      home_address:'',
      grad_year:'',
      professional_bodies:'',
      other_degree:'',
      year_of_bar:'',
      self_employed:'',
      CAC_accredition_year:'',
      full_time_job:'',
      years_of_practice:'',

    }
    const [userData,setUserData]=useState(intialState);
    const [practiceCountries,setPraticeCountries]=useState([])
    const [areaofSpec,setAreaOfSpec] =useState([])
    const [cv,setCv] =useState([])
    const [bar_cert,setBar_cert] =useState([])

const handleOnChange = (e) => {
  const {value,name,selectedIndex}=e.target
  if(name === 'state_of_origin' || name === 'law_school_campus' || name === 'residential_state'){
    setUserData({...userData,[name]:value})
  }
  else{
    setUserData({...userData,[name]:value})
  }

}

const handleCountryofOrigin = (state) => {
  //  //console.log(state,getState?.state,'handleCountryofOrigin')
  const countryid =getState?.state.find((item,index)=>state==index)
  // //console.log(countryid,'handleCountryofOrigin')
  return countryid?.country_id
 }
 const handleStateid = (state) => {
  // //console.log(state,getState?.state,'handleCountryofOrigin')
 const stateid =getState?.state.find((item,index)=>state==index)
 //console.log(stateid,'handleCountryofOrigin')
 return stateid?.id
}

 const handleFilter =(value)=>{
   const array =[]
   //console.log(value)
   value?.map((item,index)=>
   array.push(item?.value)
   )
   return array;
 }

 const FindStateSelected = (id) =>{
   const StateName =getState?.state?.find((item,index)=>item?.id ===Number(id))
   return StateName?.name
 }

const handelSubmit =async (e) => {
  e.preventDefault()
  ////console.log('there',getState?.state[getStateid]?.id)
  if(userData?.password !== userData?.confirm_password){
   return toast.error('You have entered different Password')
  }
  if(!userData?.password || !userData?.confirm_password){
    return toast.error('Enter your Passwords')
   }
   const handleCountryofOrigin = (state) => {
    //  console.log((state,'Stateid'))
    //  //console.log(state,getState?.state,'handleCountryofOrigin')
    const countryid =getState?.state.find((item,index)=>state==item?.id)
    // //console.log(countryid,'handleCountryofOrigin')
    return countryid?.country_id
   }
   const handleStateid = (state) => {
    // //console.log(state,getState?.state,'handleCountryofOrigin')
   const stateid =getState?.state.find((item,index)=>state==index)
   //console.log(stateid,'handleCountryofOrigin')
   return stateid?.id
  }
  
   const handleFilter =(value)=>{
     const array =[]
     //console.log(value)
     value?.map((item,index)=>
     array.push(item?.value)
     )
     return array;
   }


   
   const NewAreaOfSpe =handleFilter(areaofSpec)

  const Form = new FormData()
  Form.append("cv",cv)
  Form.append("title",userData?.title)
  Form.append("email",userData?.email)
  Form.append("first_name",userData?.first_name)
  Form.append("last_name",userData?.last_name)
  Form.append("gender",userData?.gender)
  Form.append("city",userData?.city)
  Form.append("dob",userData?.dob)
  Form.append("learning_institution",userData?.learning_institution)
  Form.append("grad_year",userData?.grad_year)
  Form.append("law_school_campus",userData?.law_school_campus)
  Form.append("phone_number",userData?.phone_number)
  Form.append("home_address",userData?.home_address)
  Form.append("year_of_bar",userData?.year_of_bar)
  Form.append("CAC_accredition_year",userData?.CAC_accredition_year==='No'?false:true)
  Form.append("password",userData?.password)
  Form.append("bar_cert",bar_cert)
  Form.append("other_degree",otherdegree?.length>0 ?true:false)
  Form.append("full_time_job",userData?.full_time_job ==='Yes'?true:false)
  Form.append("self_employed",userData?.self_employed ==='Yes'?true:false)
  //other_degree
 
  otherdegree?.length>0 ?
  otherdegree?.map((item,index)=>  
  Form.append(`degrees[${index}]`,item))
  :Form.append(`degrees[]`,'')
  
  // if(userData?.other_degree === 'Yes'){
  //   otherdegree?.map((item,index)=>  
  //   Form.append(`degrees[${index}]`,item))
  // }
  professionalBody?.map((item,index)=>  
    Form.append(`professional_bodies[${index}]`,item))

  // otherdegree?.map((item,index)=>  
  // Form.append(`degrees[${index}]`,item))
  NewAreaOfSpe?.map((item,index)=>  
  Form.append(`areas_of_specialization[${index}]`,item))

  practiceCountries?.length>0 ? handleFilter(practiceCountries)?.map((item,index)=>  
  Form.append(`qualifed_practice_countries[${index}]`,item)) :
  Form.append(`qualifed_practice_countries[]`,``)
  // //console.log(areaofSpec,'ehell')

  Form.append("residential_state",(userData?.residential_state))
  Form.append("country_of_origin",handleCountryofOrigin(userData?.state_of_origin))
  // Form.append("qualifed_practice_countries",handleFilter(practiceCountries))
  // Form.append("areas_of_specialization",handleFilter(areaofSpec))
  Form.append("state_of_origin",(userData?.state_of_origin))
  Form.append("years_of_practice",userData?.years_of_practice)
 
   RegisterLawyerComp(Form)
   console?.log(userData)
  
}

  return (
    <LayoutNoAuth footer Title={'Lawyer Sign up'}>
    
      <Style className='container'>
      <h4 className="text-center fw-bold mb-lg-5">Sign up</h4>
      
      <div className='d-none d-md-inline'>
      <div className='d-flex justify-content-center'>
        <div className=' d-flex position-relative '>
          <span className=' '>
            <div onClick={()=>setSignupLevel(0)} className='pointer inline switch1 position-relative'>1</div>
            
          </span>
          
          <div onClick={()=>setSignupLevel(1)} className='mx-10 pointer two inline switch1'><span>2</span></div>
          <div onClick={()=>setSignupLevel(2)} className='inline pointer switch1'>3</div>
        </div>
      </div>
      <div className='d-flex justify-content-center'>
        <div className=' d-flex justify-content-center text-center position-relative '>
        <span className='text-end text-sm-center'>Personal Information</span>
            <span className='mx-10sub '>Educational Information</span>
           
          <span className=' text-start text-sm-center'> Additional  Information</span>
         
        </div>
      </div>
      </div>
      </Style>
    {signupLevel==0 ?
    <LawyerProfileForm handleOnChange={handleOnChange} userData={userData} 
    setSignupLevel={setSignupLevel} state={getState?.state} FindStateSelected={FindStateSelected} showError={showError} setShowError={setShowError}/>:
    signupLevel==1 ?
    <LawyerEducationalForm otherdegree={otherdegree} 
    setOthersDegree={setOthersDegree} professionalBody={professionalBody} 
    setProfessionalBody={setProfessionalBody} handleOnChange={handleOnChange}
     userData={userData} bar_cert={bar_cert} getCountriesData={getCountriesData} 
     getLawyerSpecialization={getLawyerSpecialization} 
     setSignupLevel={setSignupLevel} Years={Years} HowManyYears={HowManyYears} 
     campuses={state?.campuses} setPraticeCountries={setPraticeCountries}
     FindStateSelected={FindStateSelected}
      practiceCountries={practiceCountries} showError={showError} setShowError={setShowError}/>:
    signupLevel==2 ? <AdditionalInformation 
    Years={Years} 
    areaofSpec={areaofSpec} 
    FindStateSelected={FindStateSelected}
    setAreaOfSpec={setAreaOfSpec}
     handleOnChange={handleOnChange} 
     userData={userData}
      state={getState?.state}
       getLawyerSpecialization={getLawyerSpecialization} 
       cv={cv} setCv={setCv} bar_cert={bar_cert} setBar_cert={setBar_cert} setSignupLevel={setSignupLevel} showError={showError} setShowError={setShowError}/> :
        <LawyerCreatePassword setSignupLevel={setSignupLevel} handelSubmit={handelSubmit}  handleOnChange={handleOnChange} loading={state?.isLoading}  userData={userData}/>}  

    </LayoutNoAuth>
  )
}
